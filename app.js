/**
* Copyright (c) 2017, 
* @package  MSB
* @author   HoffenSoft
* @date     2018-02-23
* @usetool  Product of money exchange reporting tool 
*/

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var multer = require('multer'); 
var common_functions = require('./middlewares/common_functions');
const flash = require('express-flash-notification');


var index = require('./controllers/index');
var buysell = require('./controllers/buysell');
var transactionrangereport = require('./controllers/transactionrangereport');
var outlet_highriskreport = require('./controllers/outlet_highriskreport');
var highrisk_customerlist_report = require('./controllers/highrisk_customerlist_report');
var customerrisk_summary_report = require('./controllers/customerrisk_summary_report');
var sanitized_import = require('./controllers/sanitized_import');
var frequent_transactions_report = require('./controllers/frequent_transactions_report');
var monthly_frequent_transactions_report = require('./controllers/monthly_frequent_transactions_report');
var high_network_customer_report = require('./controllers/high_network_customer_report');
var network_outlet_highriskreport =require('./controllers/network_outlet_highriskreport');
var monthly_highrisk_customerlist_report = require('./controllers/monthly_highrisk_customerlist_report');
var reporting_tree = require('./controllers/reporting_tree');

var admin_users = require('./controllers/admin/users');
var admin_object_permissions = require('./controllers/admin/object_permissions');
var admin_permissions = require('./controllers/admin/permissions');
var admin_module = require('./controllers/admin/module');
var admin_roles = require('./controllers/admin/roles');
var admin_organization = require('./controllers/organization');
var branch = require('./controllers/branch');
var admin_masterrisk = require('./controllers/admin/master_risk');

var dbUtil = require('./config/dbUtil');
var config = require('./config/database');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({limit: '16mb'}));

var path = require('path');

var storage = multer.diskStorage({
	destination: function(req, file, callback) {

    let original_url = (req.originalUrl != "") ? req.originalUrl : ""; 
    let original_url_spliter = original_url.split('/');
    let uploaded_filepath = "";
    if(original_url_spliter.length > 0) {
      if(original_url_spliter[1]=="organization"){
        uploaded_filepath = __dirname+'/public/img/company_logo';
      } else {
        uploaded_filepath = __dirname+'/public/img/upload';
      }
    } else {
      uploaded_filepath = __dirname+'/public/img/upload';
    }
		callback(null, uploaded_filepath);
	},
	filename: function(req, file, callback) {
    console.log(file)
    if(path.extname(file.originalname) == ".csv")
      var filename_without_extension = path.basename(file.originalname, '.csv');
    else
      var filename_without_extension = file.fieldname;

    callback(null, filename_without_extension + '-' + Date.now() + path.extname(file.originalname))
	}
});

// to get post data and upload
// app.use(multer({dest:__dirname+'/public/img/upload'}).any());

app.use(multer({storage:storage}).any());

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// session setup
app.use(session({
  cookieName: 'session',
  secret: 'eg[isfd-8yF9-7w2315df{}+Ijsli;;to8',
  duration: 30 * 60 * 1000,
  activeDuration: 5 * 60 * 1000,
  httpOnly: true,
  secure: false,
  ephemeral: true
}));

// setting flash message 
app.use(flash(app, common_functions.go_flash_notification_options));

app.use('/', index);
app.use('/buysell', buysell);
app.use('/outlet_highriskreport', outlet_highriskreport);
app.use('/transactionrangereport', transactionrangereport);
app.use('/highrisk_customerlist_report', highrisk_customerlist_report);
app.use('/customerrisk_summary_report', customerrisk_summary_report);
app.use('/sanitized_import', sanitized_import);
app.use('/frequent_transactions_report', frequent_transactions_report);
app.use('/monthly_frequent_transactions_report', monthly_frequent_transactions_report);
app.use('/high_network_customer_report', high_network_customer_report);
app.use('/network_outlet_highriskreport', network_outlet_highriskreport);
app.use('/monthly_highrisk_customerlist_report', monthly_highrisk_customerlist_report);
app.use('/reportingTree', reporting_tree);
app.use('/branch', branch);


app.use('/users', admin_users);
app.use('/objectPermissions',admin_object_permissions);
app.use('/permissions',admin_permissions);
app.use('/module', admin_module);
app.use('/roles', admin_roles);
app.use('/organization', admin_organization);

app.use('/masterrisk', admin_masterrisk);


// Access allow control setup 
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//Create DB object once and reuse everywhere
dbUtil.connect(config.remoteUrl, function(err) {
  if (err) {
    console.log('Unable to connect to Mongo.')
    process.exit(1)     
  } else {
    console.log('connect to Mongo.')
  }
});

// error handler
app.use(function(err, req, res, next) {

  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


app.use(ignoreFavicon);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

function ignoreFavicon(req, res, next) {
  if (req.originalUrl === '/favicon.ico') {
    res.status(204).json({nope: true});
  } else {
    next();
  }
}

module.exports = app;
