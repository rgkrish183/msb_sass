/**
 * USECASE : COMMON PROJECT CONFIG DETAILS LIKE DB, TABLE, SALTKEYS, ETC,.
 * NOTE : DO NOT CHANGE BELOW DETAILS, BASED ON THIS FILE WAS WHOLE PRODUCAT WORKINGS
 */

var go_common_config = new Object();

// go_common_config["remoteUrl"] = 'mongodb://admin:admin%40123@192.168.1.227:27017/MSB_Report_Dev';

// go_common_config["domain"] = 'http://192.168.1.227:3999/';

go_common_config["remoteUrl"] = 'mongodb://admin:admin%40123@192.168.1.227:27017/MSB_Report_Dev';

go_common_config["domain"] = 'http://192.168.1.67:3999/';

go_common_config["dbName"] = 'MSB_Report_Dev';

go_common_config["action_taken"] = [
                                    {action_value:1, action_name:"Document in Hand(Addtl. docs)"}, 
                                    {action_value:2, action_name:"Suspicious?"},
                                    {action_value:3, action_name:"BGC"},
                                    {action_value:4, action_name:"Q & A"},
                                    {action_value:5, action_name:"Requested document"},
                                    {action_value:6, action_name:"Keep on Monitoring"}
                                ];
go_common_config["login_salt"] = "Hoffen";


// BELOW IS DATABASE TABLE CONFIG DETAILS
go_common_config["tbl_datasource_master"] = 'MSB_Datasource_master';

go_common_config["tbl_sanitized_data"] = 'MSB_Sanitized_Data';


/**
 * MASTER RISK TABLE
 */
go_common_config["tbl_customertype"] = 'MSB_Customer_Type';

go_common_config["tbl_modeofdelivery"] = 'MSB_Mode_of_Delivery';

go_common_config["tbl_modeofpayment"] = 'MSB_Mode_of_Payment';

go_common_config["tbl_nationality"] = 'MSB_Nationality';

go_common_config["tbl_occupation"] = 'MSB_Occupation';

go_common_config["tbl_purpose"] = 'MSB_Purpose';

go_common_config["tbl_source"] = 'MSB_Source';

go_common_config["tbl_value"] = 'MSB_Value';


go_common_config["tbl_master_riskscore"] = 'tbl_master_riskscore';

go_common_config["tbl_object_permissions"] = 'tbl_object_permissions';

go_common_config["tbl_module"] = 'tbl_module';

go_common_config["tbl_permissions"] = 'tbl_permissions';

go_common_config["tbl_roles"] = 'tbl_roles';

go_common_config["tbl_users"] = 'tbl_users';

go_common_config["tbl_organization"] = 'tbl_organization';

go_common_config["tbl_branch"] = 'tbl_branch';


// BELOW TYPES OF URL ONLY WE ARE USING IN OUR APPLICATION, 
// THERE HAVING DEPEDENCY IN BACKEND SIDE.
let usertype_access = new Object();

usertype_access["superadmin"] = 1;      //HOFFENSOFT

usertype_access["admin"] = 2;           //ORGANIZATION MANAGER

usertype_access["system_admin"] = 3;    //SYSTEM ADMIN

usertype_access["users"] = 4;           //BRANCH MANAGER

usertype_access["supervisors"] = 5;     //SUPERVIOUSER

usertype_access["tellers"] = 6;         //TELLERS

go_common_config["usertype_access"] = usertype_access;


// BELOW URLS ARE ONLY ALLOWED FOR SUPERADMIN-HOFFENSOFT
let superadmin_urls = new Array();

    // superadmin_urls.push("object_permissions");

    superadmin_urls.push("permissions");

    superadmin_urls.push("module");

    superadmin_urls.push("roles");

go_common_config["superadmin_urls"] = superadmin_urls;


go_common_config["SA_LOGIN_SUSINDEX"] = "/users";

go_common_config["CMN_LOGIN_SUSINDEX"] = "/dashboard";


let risktypes = new Array();

    risktypes.push(["customertype", "Customer Type"]);          //0
    risktypes.push(["modeofdelivery", "Mode of Delivery"]);     //1
    risktypes.push(["modeofpayment", "Mode of Payment"]);       //2
    risktypes.push(["nationality", "Nationality"]);             //3
    risktypes.push(["occupation", "Occupation"]);               //4
    risktypes.push(["purpose", "Purpose"]);                     //5
    risktypes.push(["source", "Source"]);                       //6
    risktypes.push(["value", "Value"]);                         //7

go_common_config["master_risktypes"] = risktypes;



module.exports = go_common_config;