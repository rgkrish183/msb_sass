/**
 * USECASE : COMMON MODULE CONFIG DETAILS
 * NOTE : DO NOT CHANGE BELOW DETAILS, BASED ON THIS FILE WAS WHOLE PRODUCAT RBAC PERMISSIONS
 */


var go_module_config = new Object();

/**
 * TABLE : TABLE PERMISSION CONFIG DETAILS
 */
var go_tbl_permissions = new Object();

go_tbl_permissions["PER_LISTVIEW"] = "listview";
go_tbl_permissions["PER_DVIEW"] = "download";
go_tbl_permissions["PER_DPRINT"] = "print";
go_tbl_permissions["PER_ADD"] = "add";
go_tbl_permissions["PER_EDIT"] = "edit";
go_tbl_permissions["PER_DELETE"] = "delete";
go_tbl_permissions["PER_URL"] = "url";

go_module_config["PRMS"] = go_tbl_permissions;

/**
 * TABLE : TABLE PERMISSION CONFIG DETAILS
 * GENERATE MODURL NAME HINT : MODULE MAIN ROOT URL 
 * GENERATE MDOULE NAME HINT : [MODULE TITLE+WIDGET TITLE]
 */

var go_tbl_module = new Object();

//OVERVIEW
go_tbl_module["MODURL_DASHBOARD"] = "dashboard";
go_tbl_module["MOD_OVERVIEWBUYSELL"] = "overviewBuySell";
go_tbl_module["MOD_OVERVIEWRMRANGE"] = "overviewRmRange";
go_tbl_module["MOD_OVERVIEWCUSTOMERRISK"] = "overviewCustomerRisk";
go_tbl_module["MOD_OVERVIEWHIGHVALUETRANSACTIONSBYRMRANGE"] = "overviewHighvalueTransactionsByRmrange";

//RISK BASED ANALYSIS
go_tbl_module["MODURL_CUSTOMERRISK"] = "customerRisk";
go_tbl_module["MOD_RBANATIONALITY"] = "rbaNationality";
go_tbl_module["MOD_RBAVALUERISK"] = "rbaValuerisk";
go_tbl_module["MOD_RBACUSTOMERTYPE"] = "rbaCustomertype";
go_tbl_module["MOD_RBAPURPOSE"] = "rbaPurpose";
go_tbl_module["MOD_RBASOURCE"] = "rbaSource";
go_tbl_module["MOD_RBAOCCUPATION"] = "rbaOccupation";
go_tbl_module["MOD_RBAPAYMENT"] = "rbaPayment";
go_tbl_module["MOD_RBADELIVERY"] = "rbaDelivery";

//HIGH RISK CUSTOMERS
go_tbl_module["MODURL_HIGHRISKCUSTOMERLISTREPORT"] = "highriskCustomerlistReport";
go_tbl_module["MOD_HRCOVERALLOUTLETHIGHRISK"] = "hrcOverallOutletHighrisk";
go_tbl_module["MOD_HRCLOCALINDIVIDUALCUSTOMER"] = "hrcLocalIndividualCustomer";
go_tbl_module["MOD_HRCFOREIGNINDIVIDUALSCUSTOMER"] = "hrcForeignIndividualsCustomer";
go_tbl_module["MOD_HRCLOCALBUSINESSESCUSTOMER"] = "hrcLocalBusinessesCustomer";

//HIGH NETWORTH CUSTOMERS
go_tbl_module["MODURL_HIGHNETWORKCUSTOMERREPORT"] = "highNetworkCustomerReport";
go_tbl_module["MOD_HNCOVERALLOUTLETHIGHRISKRMRANGE"] = "hncOverallOutletHighriskRmrange";
go_tbl_module["MOD_HNCLOCALINDIVIDUALCUSTOMERRMRANGE"] = "hncLocalIndividualCustomerRmrange";
go_tbl_module["MOD_HNCFOREIGNINDIVIDUALSCUSTOMERRMRANGE"] = "hncForeignIndividualsCustomerRmrange";
go_tbl_module["MOD_HNCLOCALBUSINESSESCUSTOMERRMRANGE"] = "hncLocalBusinessesCustomerRmrange";

//OVERALL CUSTOMER RISK
go_tbl_module["MODURL_OVERALLCUSTOMERRISK"] = "overallCustomerRisk";
go_tbl_module["MOD_OCROVERALLRISKBASEDANALYSIS"] = "ocrOverallRiskbasedAnalysis";

//TRANSACTIONS RISK ANALYSIS
go_tbl_module["MODURL_FREQUENTTRANSACTIONSREPORT"] = "frequentTransactionsReport";
go_tbl_module["MOD_TRAFREQUENTTRANSACTION"] = "traFrequentTransaction";
go_tbl_module["MOD_TRAMULTIPLECURRENCYTRANSACTION"] = "traMultipleCurrencyTransaction";
go_tbl_module["MOD_TRAMBFREQUENTTRANSACTION"] = "mbtraFrequentTransaction";
go_tbl_module["MOD_TRAMBMULTIPLECURRENCYTRANSACTION"] = "mbtraMultipleCurrencyTransaction";


//MONTH BASED TRANSACTIONS RISK ANALYSIS # BELOW MODULE REMOVED
// go_tbl_module["MODURL_MONTHLYFREQUENTTRANSACTIONSREPORT"] = "monthlyFrequentTransactionsReport";
// go_tbl_module["MOD_MBTRAFREQUENTTRANSACTION"] = "mbtraFrequentTransaction";
// go_tbl_module["MOD_MBTRAMULTIPLECURRENCYTRANSACTION"] = "mbtraMultipleCurrencyTransaction";

//MONTH BASED HIGH RISK TRANSACTIONS ANALYSIS
go_tbl_module["MODURL_MONTHLYHIGHRISKCUSTOMERLISTREPORT"] = "monthlyHighriskCustomerlistReport";
go_tbl_module["MOD_MBHRTAOVERALLOUTLETHIGHRISK"] = "mbhrtaOverallOutletHighrisk";
go_tbl_module["MOD_MBHRTALOCALINDIVIDUALCUSTOMER"] = "mbhrtaLocalIndividualCustomer";
go_tbl_module["MOD_MBHRTAFOREIGNINDIVIDUALSCUSTOMER"] = "mbhrtaForeignIndividualsCustomer";
go_tbl_module["MOD_MBHRTALOCALBUSINESSESCUSTOMER"] = "mbhrtaLocalBusinessesCustomer";

//MASTER RISK MANAGEMENT
go_tbl_module["MODURL_MASTERRISK"] = "masterrisk";

//USERS
go_tbl_module["MODURL_USERS"] = "users";

//SANITIZED UPLOAD TRANSACTIONS
go_tbl_module["MODURL_DATASOURCEUPLOAD"] = "xlupload";

//MODULE PERMISSIONS
go_tbl_module["MODURL_MODULEPERMISSIONS"] = "objectPermissions";

go_module_config["MDUL"] = go_tbl_module;

module.exports = go_module_config;