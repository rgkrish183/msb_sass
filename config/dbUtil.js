var MongoClient = require('mongodb').MongoClient

var state = {
  db: null,
}

exports.connect = function(url, done) {
  if (state.db) return done()


  var dbOptions = {
    db: { native_parser: true },
    server: {
      auto_reconnect: true,
      socketOptions: {
        poolSize: 1000,
        keepAlive: 300000, 
        connectTimeoutMS: 300000,
        socketTimeoutMS: 300000
      }
    }
  };


  MongoClient.connect(url, dbOptions, function(err, db) {
    if (err) return done(err)
    state.db = db
    done()
  })
}

exports.get = function() {
  return state.db
}

exports.close = function(done) {
  if (state.db) {
    state.db.close(function(err, result) {
      state.db = null
      state.mode = null
      done(err)
    })
  }
}