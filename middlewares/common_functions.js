var config = require('../config/database');
var dbUtil = require('../config/dbUtil')
var each = require('async-each');
var RBAC = require('rbac').default;
var secure = require('rbac/controllers/express');
var mongo = require('mongodb');

/**
 * USE :  To settin user permission as in session value
 * @param {to passing object with roles id} params 
 */
exports.go_modules_sessionsetup = function(params) {

  return new Promise(async(resolve, reject) => {

    try {
      var rbac_obj = new RBAC();

      let user_roleid = new mongo.ObjectID(params.user_roleid);
      let user_organizationid = new mongo.ObjectID(params.user_organizationid);

      let matcher_permission = new Object();

      if(params.user_usertype==config.usertype_access.admin) {
        matcher_permission["role"] = user_roleid;
      } else {
        matcher_permission["role"] = user_roleid;
        matcher_permission["organizationid"] = user_organizationid;
      }

      let permissions_results = await dbUtil.get().collection(config.tbl_object_permissions).aggregate([
        { $match : matcher_permission },
        { $lookup: { from: config.tbl_module, localField: 'parent_object_id', foreignField: '_id', as: 'parent_modules' } },
        { $lookup: { from: config.tbl_roles, localField: 'role', foreignField: '_id', as: 'role' } },
        { $lookup: { from: config.tbl_module, localField: 'object', foreignField: '_id', as: 'object' } },
        { $lookup: { from: config.tbl_permissions, localField: 'permissions', foreignField: '_id', as: 'permissions' } }
      ]).toArray();
  
      let rtn_permissions_data = new Object();
  
      if(permissions_results.length > 0){

        var role = permissions_results[0].role[0].field;
        var objects = new Object();
        var grants = new Object();
        let grantPermissions = new Array();
        let last_urlgrants = new Array();
  
        each(permissions_results, function (item, next_mover) {

          let permissions = [];

          if(item.permissions.length > 0) {

            each(item.permissions, function(per, next_inner){
              permissions.push(per.field)
              grantPermissions.push(per.field+ "_" + item.object[0].field);
              
              next_inner();
            },function(){
  
              objects[item.object[0].field] = permissions;
              grants[role] = grantPermissions;
  
              let grant_object_urls = new Array();
              grant_object_urls.push(item.parent_modules[0].field);
              grant_object_urls.push(item.parent_modules[0].label);
              last_urlgrants.push(grant_object_urls);
  
              next_mover();
            });
              
          } else  {
              next_mover();
          }

        },function(){

          var nav_rooturl = new Array();

          each(last_urlgrants, function (url_items, url_next_mover) {

            if(objects[url_items[0]]!==undefined)
              objects[url_items[0]].push("url");
            else 
              objects[url_items[0]] = ["url"];
            grantPermissions.push("url_" + url_items[0]);
            grants[role] = grantPermissions;

            url_next_mover();
          },function(){

            rbac_obj.create([role], objects, grants, function(err, data) {
              if(err) {
                  throw err;
              } else {
                
                remove_duplicates_from_arr(last_urlgrants, function(rooturls_datalist){
                  rtn_permissions_data["status"] = true;
                  rtn_permissions_data["role_data"] = role;
                  rtn_permissions_data["root_urls_arr"] = rooturls_datalist;
                  rtn_permissions_data["msg"] = "For given user roles";
                  rtn_permissions_data["rbac_obj"] = rbac_obj;
        
                  resolve(rtn_permissions_data);
                });
              }
            });
          });

        }); 

      } else {
        rtn_permissions_data["status"] = false;
        rtn_permissions_data["role_data"] = new Array();
        rtn_permissions_data["root_urls_arr"] = new Array();
        rtn_permissions_data["msg"] = "There is dont have permission for given users roles";
        rtn_permissions_data["rbac_obj"] = rbac_obj;

        resolve(rtn_permissions_data);
      } 
    } catch(e) {
      console.log("ERR : When checking rbac setup");
      console.log(e);
      return reject(e);
    }
  });
}

/**
 * USE : GIVEN URL RBAC VALIDATION 
 * @param {params object roles, actions, url} params 
 * @param {validation function returns } callback 
 */
exports.go_modules_rbacvalidations = function(params, callback) {
  
  let req_usertype = params.usertype;
  let req_roles = params.roles;
  let req_actions = params.actions;
  let req_url = params.url;
  let rbac_obj = params.rbac_obj;

  return new Promise(async(resolve, reject) => {
    try {
      if(req_usertype == config.usertype_access.superadmin) {
        resolve(true);
      } else {
        rbac_obj.can(req_roles, req_actions, req_url, (err, can_status) => {
          if(err) {
            console.log("ERR : When checking rbac validations 1");
            console.log(err);
            resolve(false);
          } else {
            resolve(can_status);
          }
        });
      }
    } catch(e) {
      console.log("ERR : When checking rbac validations 2");
      console.log(e);
      resolve(false);
    }    
  });

}

/**
 * USE : FLASH NOTIFICATION PUSH MESSAGE SETUPS
 */
exports.go_flash_notification_options = {
  beforeSingleRender: function(item, callback) {
    if (item.type) {
      switch(item.type) {
        case 'GOOD':
          item.type = 'Success';
          item.alertClass = 'alert-success';
          break;
        case 'OK':
          item.type = 'Info';
          item.alertClass = 'alert-info';
          break;
        case 'BAD':
          item.type = 'Error';
          item.alertClass = 'alert-danger';
          break;
      }
    }
    callback(null, item);
  }
};


function remove_duplicates_from_arr(A, callback){

  var hash = {};
  var out = [];
  for (var i = 0, l = A.length; i < l; i++) {
    var key = A[i].join('|');
    if (!hash[key]) {
      out.push(A[i]);
      hash[key] = 'found';
    }
  }
  return callback(out);
}