var session = require('express-session');
var common_functions = require('./common_functions');
var config = require('../config/database');
var _ = require('lodash');
var dbUtil = require('../config/dbUtil');
var mongo = require('mongodb');

module.exports = {

  chk_loginaccess: async (req, res, next) => {
    if (!req.session.user) {
      res.redirect('/login');
    } else {

      if(req.session.user.ses_usertype == config.usertype_access.superadmin) {
        next();
      } else {

        var url_datasourceid_validator = new Promise(async(resolve, reject) => {
          resolve(true);
        });

        url_datasourceid_validator.then(async (validator) => {
          if(validator) {

            var baseurl_split_arr;
            if(req.baseUrl != ""){
              var url = req.baseUrl;
              baseurl_split_arr = url.split("/");
            } else if(req.originalUrl != "") {
              var url = req.originalUrl;
              baseurl_split_arr = url.split("/");
            }
            var pages_couter = await _.findIndex(config.superadmin_urls, function(o) { return o == baseurl_split_arr[1]; });
  
            if(pages_couter >= 0) {
              res.redirect('/dashboard');
            } else {
              let roles_params = new Object();
  
              roles_params["user_usertype"] =  req.session.user.ses_usertype;
              roles_params["user_roleid"] =  req.session.user.ses_role;
              roles_params["user_organizationid"] =  req.session.user.ses_organization;
  
              let modules = await common_functions.go_modules_sessionsetup(roles_params);
              if(modules) {
                req.session.user.root_urls = modules.root_urls_arr;
                res.locals.rbac_obj = modules.rbac_obj;
                next();
              }
            }
          } else {
            res.redirect('/malformaction');
          }          
        });
      }
    }
  }
};


