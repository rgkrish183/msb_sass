var config = require('../config/database');
var dbUtil = require('../config/dbUtil')
var each = require('async-each');
var moment = require("moment");
var mongo = require('mongodb');

exports.highrisk_customer_listreport = function(final_frequenttrans_arr, params, callback){

  var collection = dbUtil.get().collection(config.tbl_sanitized_data);
  var nationality = (params.nationality == "MALAYSIA") ? { "$eq": "MALAYSIA" } : { "$ne": "MALAYSIA" };
  var datasource_subqry = params.datasource_subqry; 
  var trans_count = params.trans_count;

  var finder = new Object();

  finder["Customer Type"] =  params.type;
  finder["Nationality"] =  nationality;
  finder["Transaction Date"] =  {"$gt":params.fmdt_cnter, "$lt":params.todt_cnter};
  finder["Total Risk Score"] =  {"$gt": 15};
  finder["data_proprietor"] =  datasource_subqry["data_proprietor"];

  var matcher = new Object();
  matcher["total"] = {'$gte': parseInt(trans_count)};

  var transcounter_qry = collection.aggregate([ { 
      $match : { $and :[finder]}
    }, { 
      $group : { 
        _id : {
          "Month":{$month:"$Transaction Date"}, 
          "Year":{$year:"$Transaction Date"},
          "Name":"$Name"
        }, 
        "total" : { $sum : 1 } ,
        "Customers":{$push:'$$ROOT.Name'}
      }
    }, {
        $match : matcher
    }, {
      $project :{
        "month":"$_id.Month","year":"$_id.Year","name":"$_id.Name","total":"$total"
      }
    }
  ]);

  transcounter_qry.toArray(function(err, transcounter_result) {

    if(transcounter_result.length > 0) {
      each(transcounter_result, function(result, next){

        var data_obj = new Array();
        var month = result.month;
        month = moment(month, 'MM').format('MMM');  
        var year = result.year;
        var monthyear = month+"-"+year;

        data_obj.push(monthyear);
        data_obj.push(result.name);
        data_obj.push(result.total);
        
        final_frequenttrans_arr.push(data_obj);
        next();
      }, function(){
        return callback(final_frequenttrans_arr)
      });
    } else {
      return callback(final_frequenttrans_arr)
    }
  });
}


exports.monthly_highriskcustomer_outlet_listreport = function(params, callback){

  var tbl_sd_collection = dbUtil.get().collection(config.tbl_sanitized_data);
  let datasource_subqry = params.datasource_subqry; 
  let prms_trans_count = params.trans_count;
  let prms_fromdate = params.fmdt_cnter;
  let prms_todate = params.todt_cnter;
 
  var finder = new Object();
  finder["Transaction Date"] =  {"$gte":prms_fromdate, "$lte":prms_todate};
  finder["Total Risk Score"] =  {"$gt": 15};
  finder["data_proprietor"] =  datasource_subqry["data_proprietor"];

  var matcher = new Object();
  matcher["Transctions_Count"] = {'$gte': parseInt(prms_trans_count)};

  tbl_sd_collection.aggregate([
    { 
      $match : { $and :[finder] }
    }, { 
      $group : { 
        _id : {
          "Outlet":"$Outlet",
        }, 
        "Transctions_Count" : { $sum : 1 } ,
        "Transctions_Date" : {$push:{$substr: ['$Transaction Date', 0, 7]}},
      }
    }, {
      $match : matcher
    }, {
      $project : { "Outlet" : "$_id.Outlet", "Transctions_Count" : "$Transctions_Count", "Transctions_Date" : "$Transctions_Date" }
    }
  ]).toArray(function(err, result) {

    if(err) {
      console.log(err);
    } else {
      return callback(result);
    }

  });
}


exports.monthly_hrc_basedonoutlet_lister = function(params, callback){

  var tbl_sd_collection = dbUtil.get().collection(config.tbl_sanitized_data);

  let prms_fromdate = params.fmdt_cnter;
  let prms_todate = params.todt_cnter;
  let prms_outlet = params.outlet_name;
 
  let prms_Name = params.Name;
  let prms_trans_count = params.trans_count;

  let prms_Fre_Name = params.fre_cust_name || "";
  let prms_Fre_trans_count = params.fre_trans_count;

  var finder = new Object();
  finder["data_proprietor"] =  params.datasource_subqry["data_proprietor"];

  if(prms_Fre_Name !== "" && prms_Fre_Name !== undefined) {
    
    finder["Transaction Date"] = { "$gt":prms_fromdate, "$lt":prms_todate};
    finder["Name"] = prms_Fre_Name;

    var matcher = new Object();
    matcher["count"] = {'$gt': parseInt(5)};
    
    tbl_sd_collection.aggregate( [ 
      { 
        $match: { $and: [finder] } 
      }, {
        '$group': { _id: null, 'count': {'$sum': 1}, 'data': {$push: '$$ROOT'} }
      }, {
        '$match': matcher
      }
    ]).toArray(function(err, customers_result){
      if(err) {
        console.log(err);
      } else {
        return callback(customers_result);
      }
    });

  } else if(prms_outlet === undefined || prms_outlet === " " || prms_outlet ==='') {

    finder["Transaction Date"] =  {"$gte":prms_fromdate, "$lte":prms_todate};
    finder["Total Risk Score"] =  {"$gt": 15};
    finder["Name"] = prms_Name;

    tbl_sd_collection.aggregate([
      {  $match : { $and :[finder] } }
    ]).toArray(function(err, customers_result){
      if(err) {
        console.log(err);
      } else {
        return callback(customers_result);
      }
    });

  } else {

    finder["Transaction Date"] =  {"$gte":prms_fromdate, "$lte":prms_todate};
    finder["Total Risk Score"] =  {"$gt": 15};
    finder["Outlet"] = prms_outlet;

    tbl_sd_collection.aggregate([
      { $match : { $and :[finder] } }
    ]).toArray(function(err, customers_result){
      if(err) {
        console.log(err);
      } else {
        return callback(customers_result);
      }
    });
  }
 }