var config = require('../config/database');
var dbUtil = require('../config/dbUtil')

exports.high_network_customer_report = function(params, callback){
  
  var collection = dbUtil.get().collection(config.tbl_sanitized_data);
  var nationality = (params.nationality == "MALAYSIA") ? { "$eq": "MALAYSIA" } : { "$ne": "MALAYSIA" };
  var datasource_subqry = params.datasource_subqry;
  var action = parseInt(params.action);

  let finder =  new Object();;

  finder["Customer Type"] =  params.type;
  finder["Nationality"] =  nationality;
  finder["Transaction Date"] =  {"$gt":params.fmdt_cnter, "$lt":params.todt_cnter};
  finder["Total Risk Score"] =  {"$gt": 15};
  finder["Exchange Amount"] = {"$gt": action};
  finder["data_proprietor"] =  datasource_subqry['data_proprietor'];

  let getter = new Object();
  getter["Transaction Date"] =  1;
  getter["Receipt No"] =  1;
  getter["Name"] =  1;
  getter["Nationality"] =  1;
  getter["Exchange Amount"] =  1;
  getter["Outlet"] =  1;
  getter["Customer Type"] =  1;
  getter["Total Risk Score"] =  1;
  getter["Action Taken"] =  1;
  getter["Remarks"] =  1;
  
  collection.find(finder, getter, 2000)
  .toArray(function(err, result) {
    callback(err,result)
  });


}