var config = require('../config/database');
var dbUtil = require('../config/dbUtil')

exports.highRiskCustomer = function(params, callback){
 
  var collection = dbUtil.get().collection(config.tbl_sanitized_data);
 
  collection.aggregate([
  { 
    $match: { $and: [ 
            {"Total Risk Score": { $gt: 15 } }, 
            {"Transaction Date":{ $gt:params.fmdt_cnter, $lt:params.todt_cnter} }, params.datasource_subqry 
          ] } 
  },
  { $group: { "_id": {$toLower : "$Outlet"}, "HRC": { $addToSet: "$Name" },"Total":{$push:"$Name"} } },
  {$project: {"HRC": { $size: "$HRC" },"Transactions":{$size:"$Total"}}}
 ]).toArray(function(err, result) {
  // console.log(result);
  callback(err,result)
 });
}

exports.highriskcustomerbasedonoutlet_list = function(params, callback){
  
  var collection = dbUtil.get().collection(config.tbl_sanitized_data);
  var outlet = new RegExp(params.outlet_name,"i");
  
  collection.aggregate([
  { 
    $match: { $and: [ 
      {"Total Risk Score": { $gt: 15 } }, 
      {"Transaction Date":{ $gt:params.fmdt_cnter, $lt:params.todt_cnter} },
      {"Outlet":outlet}, params.datasource_subqry 
    ]} 
  }
  ]).toArray(function(err, result) {
    // console.log(result);
    callback(err,result)
  });

}


