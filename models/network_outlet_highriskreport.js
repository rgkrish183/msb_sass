var dbUtil = require('../config/dbUtil')
var config = require('../config/database');

exports.networkhighRiskCustomer = function(params, callback){
  var action = parseInt(params.action);

  var collection = dbUtil.get().collection(config.tbl_sanitized_data);
 
  collection.aggregate([
  { 
    $match: { 
      $and: [ 
        {"Total Risk Score": { $gt: 15 } }, 
        {"Transaction Date":{ $gt:params.fmdt_cnter, $lt:params.todt_cnter} }, params.datasource_subqry,
        {"Exchange Amount": { $gt: action } }
      ] } 
  },
  { $group: { 
      "_id": { $toLower : "$Outlet" }, 
      "HRC": { $addToSet: "$Name" },
      "transArr":{ 
        $addToSet:{
          "month":{$month:"$Transaction Date"},
          "year":{$year:"$Transaction Date"},
          "Name":"$Name"
        }
      } 
    } 
  },
  {$project: {"HRC": { $size: "$HRC" },"transArrSize":{$size: "$transArr"}}}
 ]).toArray(function(err, result) {
  callback(err,result)
 });

}

exports.networkhighriskcustomerbasedonoutlet_list = function(params, callback){
  
  var collection = dbUtil.get().collection(config.tbl_sanitized_data);
  var action = parseInt(params.action);
  var outlet = new RegExp(params.outlet_name,"i");

  collection.aggregate([
  { 
    $match: { $and: [ 
      {"Total Risk Score": { $gt: 15 } }, 
      {"Transaction Date":{ $gt:params.fmdt_cnter, $lt:params.todt_cnter} },
      {"Outlet":outlet}, 
      params.datasource_subqry,
      {"Exchange Amount": { $gt: action } } 
    ]} 
  }
  ]).toArray(function(err, result) {
    callback(err,result)
  });

}


