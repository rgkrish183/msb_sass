var config = require('../../config/database');
var dbUtil = require('../../config/dbUtil');
var async = require('async');
var mongo = require('mongodb');

exports.insertdata = function(parms,callback ){
 
     var module_add_collection = dbUtil.get().collection(config.tbl_roles);
   
    

    module_add_collection.insertOne(parms,function(err, res){
        if (err){
            throw err;
        } else{
            return callback(res);
        }
    })
    // console.log(isAlreadyExists);
  }


  exports.updatedata = function(id, params, callback){

    var module_update_collection = dbUtil.get().collection(config.tbl_roles);
    var pk_module_id = new mongo.ObjectID(id);
    
    let matcher = new Object();
    matcher["_id"] = pk_module_id;

    let module_update_values = new Object();
    module_update_values["$set"] = params;

    module_update_collection.updateOne(matcher, module_update_values,function(err, res){
        if (err){
            throw err;
        } else {
            return callback(res);
        }
    })
  }

  exports.deletedata = function(params,id,callback){
 
    var collection = dbUtil.get().collection(config.tbl_roles);
    var o_id = new mongo.ObjectID(id);

    var myquery = {_id: o_id };

    collection.deleteOne(myquery,function(err, res){
        if (err){
            throw err;
        } else{
            return callback(res);
        }
    })
  }



  exports.finddata = function(params,id ,callback){
 
    var collection = dbUtil.get().collection(config.tbl_roles);
   var o_id = new mongo.ObjectID(id);
    var query = { _id: o_id };

      collection.find(query).toArray(function(err, result) {
        if (err) {
            throw err;
        }else{
            console.log(result);
            return callback(result)
        }
      });
  }


  
exports.getalldata = function(params, callback){

    var collection = dbUtil.get().collection(config.tbl_roles);
    var data = new Array();

    collection.find({},{ _id: 1, label: 1, field: 1, parent_role_position: 1 }).toArray(function(err, result) { 
        if (err) {
            throw err;
        }  else {
            return callback(result);
        }
    });
}
