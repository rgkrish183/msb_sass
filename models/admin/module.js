var config = require('../../config/database');
var dbUtil = require('../../config/dbUtil');
var async = require('async');
var mongo = require('mongodb');

exports.insertdata = function(parms,callback ){
 
     var module_add_collection = dbUtil.get().collection(config.tbl_module);
   
    

    module_add_collection.insertOne(parms,function(err, res){
        if (err){
            throw err;
        } else{
            return callback(res);
        }
    })
    // console.log(isAlreadyExists);
  }


  exports.updatedata = function(id, params, callback){

    var module_update_collection = dbUtil.get().collection(config.tbl_module);
    var pk_module_id = new mongo.ObjectID(id);
    
    let matcher = new Object();
    matcher["_id"] = pk_module_id;

    let module_update_values = new Object();
    module_update_values["$set"] = params;

    module_update_collection.updateOne(matcher, module_update_values,function(err, res){
        if (err){
            throw err;
        } else {
            return callback(res);
        }
    })
  }

  exports.deletedata = function(params,id,callback){
 
    var collection = dbUtil.get().collection(config.tbl_module);
    var o_id = new mongo.ObjectID(id);

    var myquery = {_id: o_id };

    collection.deleteOne(myquery,function(err, res){
        if (err){
            throw err;
        } else{
            return callback(res);
        }
    })
  }



  exports.finddata = function(params,id ,callback){
 
    var collection = dbUtil.get().collection(config.tbl_module);
   var o_id = new mongo.ObjectID(id);
    var query = { _id: o_id };

      collection.find(query).toArray(function(err, result) {
        if (err) {
            throw err;
        }else{
            console.log(result);
            return callback(result)
        }
      });
  }


  
exports.getalldata = function(params, callback){

    var collection = dbUtil.get().collection(config.tbl_module);
    var data = new Array();

    collection.aggregate([ 
        { $lookup: { from: config.tbl_module, localField: 'root_parent_id', foreignField: '_id', as: 'root_parent' } } 
    ]).toArray(function(err, result) { 
        if (err) {
            throw err;
        }  else {
            return callback(result);
        }
    });
}


exports.getallorganizationdata = function(params, callback){

    var collection = dbUtil.get().collection(config.tbl_organization);
    var data = new Array();

    collection.find({},{ _id: 1, u_orgname: 1, u_firstname: 1,u_lastname:1,u_email:1,u_mobile:1 }).toArray(function(err, result) { 
        if (err) {
            throw err;
        }  else {
            return callback(result);
        }
    });
}


exports.findorganizationdata = function(params,id ,callback){
 
    var collection = dbUtil.get().collection(config.tbl_organization);
   var o_id = new mongo.ObjectID(id);
    var query = { _id: o_id };

      collection.find(query).toArray(function(err, result) {
        if (err) {
            throw err;
        }else{
            console.log(result);
            return callback(result)
        }
      });
  }

  exports.updateorganizationdata = function(id, params, callback){

    var module_update_collection = dbUtil.get().collection(config.tbl_organization);
    var pk_module_id = new mongo.ObjectID(id);
    
    let matcher = new Object();
    matcher["_id"] = pk_module_id;

    let module_update_values = new Object();
    module_update_values["$set"] = params;

    module_update_collection.updateOne(matcher, module_update_values,function(err, res){
        if (err){
            throw err;
        } else {
            return callback(res);
        }
    })
  }



  exports.getallbranchdata = function(params, callback){

    var collection = dbUtil.get().collection(config.tbl_branch);
    var data = new Array();

    collection.find({},{ _id: 1, b_branchname: 1, b_branchaddress: 1, b_firstname: 1,b_lastname:1,b_email:1,b_mobile:1 }).toArray(function(err, result) { 
        if (err) {
            throw err;
        }  else {
            return callback(result);
        }
    });
}


exports.insertbranchdata = function(parms,callback ){
 
    var module_add_collection = dbUtil.get().collection(config.tbl_branch);
  
   

   module_add_collection.insertOne(parms,function(err, res){
       if (err){
           throw err;
       } else{
           return callback(res);
       }
   })
   // console.log(isAlreadyExists);
 }

