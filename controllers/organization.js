var config = require('../config/database');
var dbUtil = require('../config/dbUtil');
var express = require('express');
var router = express.Router();
var each = require('async-each');
var auth = require('../middlewares/auth'), chk_loginaccess = auth.chk_loginaccess;
var common_functions = require('../middlewares/common_functions');
var mongo = require('mongodb');
var module_report = require('../models/admin/module');

/**
 * MODULE : ORGANIZATION 
 * USE : TO LIST OVERALL ORGANIZATION IN SUPER ADMIN LOGIN 
 */
router.get('/', chk_loginaccess, async (req, res) => {

    if(req.session.user.ses_usertype == config.usertype_access.superadmin) {

        const result = new Promise(async(resolve, reject) => {

            try {

                let module_params = new Object();
                module_report.getallorganizationdata(module_params, function(module_results){

                    let rtn_users_data = new Array();
            
                    if(module_results.length > 0) {
                
                        each(module_results, function (module_items, next_mover) {

                            let temp_obj = new Object();
                            temp_obj["id"] = module_items._id;
                            temp_obj["org_orgname"] = module_items.u_orgname;
                            temp_obj["org_firstname"] = module_items.u_firstname;
                            temp_obj["org_lastname"] = module_items.u_lastname;
                            temp_obj["org_email"] = module_items.u_email;
                            temp_obj["org_mobile"] = module_items.u_mobile;

                            rtn_users_data.push(temp_obj);
                            next_mover();
                        }, function() {
                            resolve(rtn_users_data);                         
                        });
                    } else {
                        resolve(rtn_users_data);                         
                    }
                });

            } catch (e) {
                console.log(e);
                reject(e);
            }
        });

        var urlquery_request = new Object();
        urlquery_request["fromdate"] = req.query.fromdate;
        urlquery_request["todate"] = req.query.todate;

        // RENDER PAGE PARAMS 
        let render_params_obj = new Object();
        render_params_obj["pagename"] = "module";
        render_params_obj["domain_url"] = config.domain;
        render_params_obj["ses_user"] = req.session.user;
        render_params_obj["urlquery_request"] = urlquery_request;

        result.then(function(rtn_users_data){
            render_params_obj["data"] = rtn_users_data;
            res.render('organization_list', render_params_obj);
        });

    } else {
        res.render('errors/errors',{"domain_url":config.domain, "error_code":401, "error_mainmsg":"Module Permission Denied !"});
    }
});

/**
 * MODULE : ORGANIZATION 
 * USE : TO EDIT ORGANIZATION IN ADMIN LOGIN 
 */
router.get('/edit', chk_loginaccess, async (req, res) => {

    if(req.session.user.ses_usertype == config.usertype_access.admin) {

        let user_id = req.session.user.ses_organization;
        var rtn_users_data = new Array();

        const result = new Promise(async(resolve, reject) => {

            try {
                let rtn_module_data = new Array();
                console.log(user_id === undefined || user_id == "");

                if(user_id === undefined || user_id == "") {
                    resolve(rtn_module_data);
                } else {
                    let module_params = new Object();

                    module_report.findorganizationdata(module_params,user_id,function(module_results){
                        if(module_results.length > 0) {
            
                            each(module_results, function (module_item, next_mover) {

                                let temp_obj = new Object();
                                temp_obj["id"] = module_item._id;
                                temp_obj["org_orgname"] = module_item.u_orgname;
                                temp_obj["org_firstname"] = module_item.u_firstname;
                                temp_obj["org_lastname"] = module_item.u_lastname;
                                temp_obj["org_email"] = module_item.u_email;
                                temp_obj["org_mobile"] = module_item.u_mobile;
                                temp_obj["org_orgaddress"]=module_item.u_organizationaddress;
                                
                                rtn_module_data.push(temp_obj);
                                next_mover();
                            }, function() {
                                resolve(rtn_module_data);                         
                            });
                        } else {
                            resolve(rtn_module_data);                         
                        }   
                    });
                }
            } catch (e) {
                console.log(e);
                reject(e);
            }
        });

        result.then(function(rtn_module_data){

            var urlquery_request = new Object();
            urlquery_request["fromdate"] = req.query.fromdate;
            urlquery_request["todate"] = req.query.todate;
        
            // RENDER PAGE PARAMS 
            let render_params_obj = new Object();
            render_params_obj["pagename"] = "module";
            render_params_obj["domain_url"] = config.domain;
            render_params_obj["ses_user"] = req.session.user;
            render_params_obj["urlquery_request"] = urlquery_request;
            render_params_obj["data"] = rtn_module_data;

            if(rtn_module_data.length > 0)
                res.render('organization_edit', render_params_obj);
            else 
                res.render('errors/errors',{"domain_url":config.domain, "error_code":400, "error_mainmsg":"Something Went Wrong. Action id is required !", });
        });
    } else {
        res.render('errors/errors',{"domain_url":config.domain, "error_code":401, "error_mainmsg":"Module Permission Denied !"});
    }
});

/**
 * MODULE : ORGANIZATION 
 * USE : TO UPDATE ORGANIZATION IN ADMIN LOGIN 
 */
router.post('/update', chk_loginaccess, async (req, res) => {

    if(req.session.user.ses_usertype == config.usertype_access.admin) {
    
        let user_id = req.session.user.ses_organization;
        var rtn_users_data = new Array();
        var roles_results;
        var users_reporting_results;

        const result = new Promise(async(resolve, reject) => {

            try {
                if(user_id === undefined || user_id == "") {
                    resolve(false);
                } else {
                    
                    var files = req.files[0];
                    if(files) {
                        var upload_filename = files.filename;
                        req.session.user.ses_organization_logo=upload_filename;
                    } else {
                        req.session.user.ses_organization_logo=req.session.user.ses_organization_logo;
                    }

                    let org_update_obj = new Object();

                    org_update_obj["u_orgname"] = req.body.o_orgname;
                    org_update_obj["u_firstname"] = req.body.o_firstname;
                    org_update_obj["u_lastname"] =req.body.o_lastname;
                    org_update_obj["u_email"] =req.body.o_email;
                    org_update_obj["u_mobile"] =req.body.o_mobile;
                    if(files) {
                        org_update_obj["u_organization_logo"] =upload_filename;
                    }
                    org_update_obj["u_organizationaddress"]=req.body.o_orgaddress;

                    module_report.updateorganizationdata(user_id, org_update_obj, function(userupdate_collection){
                        if(userupdate_collection){
                            req.session.save(function() { resolve(true); });
                        } else {
                            resolve(false);
                        }
                    });
                }
            } catch(e) {
                console.log(e);
                reject(e);
            }
        });

        result.then(function(rtn_usersupdate_status){

            if(rtn_usersupdate_status) {
                req.flash('GOOD', 'Organization Updated Successfully!', false);
                res.status(200).send({status:true, msg:"Module Updated Successfully!"});
            } else {
                req.flash('BAD', 'Somthing went wrong, try again!', false);
                res.status(204).send({status:false, msg:"Somthing went wrong!"});
            }
        });

    } else {
        res.render('errors/errors',{"domain_url":config.domain, "error_code":401, "error_mainmsg":"Module Permission Denied !"});
    }    
});


/**
 * MODULE : ORGANIZATION 
 * USE : FOR DETAILS VIEW IN SUPERADMIN LOGIN 
 */
router.get('/getdata', chk_loginaccess, async (req, res) => {
    
    if(req.session.user.ses_usertype == config.usertype_access.superadmin) {

        let user_id = req.query.id;
        
        var rtn_users_data = new Array();

        const result = new Promise(async(resolve, reject) => {

            try {
                let rtn_module_data = new Array();
                console.log(user_id === undefined || user_id == "");

                if(user_id === undefined || user_id == "") {
                    resolve(rtn_module_data);
                } else {
                    let module_params = new Object();

                    module_report.findorganizationdata(module_params,user_id,function(module_results){
                        if(module_results.length > 0) {
            
                            each(module_results, function (module_item, next_mover) {

                                let temp_obj = new Object();
                                temp_obj["id"] = module_item._id;
                                temp_obj["org_orgname"] = module_item.u_orgname;
                                temp_obj["org_firstname"] = module_item.u_firstname;
                                temp_obj["org_lastname"] = module_item.u_lastname;
                                temp_obj["org_email"] = module_item.u_email;
                                temp_obj["org_mobile"] = module_item.u_mobile;
                                temp_obj["org_orgaddress"]=module_item.u_organizationaddress;
                                temp_obj["org_organization_logo"]=module_item.u_organization_logo
                                rtn_module_data.push(temp_obj);
                                next_mover();
                            }, function() {
                                resolve(rtn_module_data);                         
                            });
                        } else {
                            resolve(rtn_module_data);                         
                        }   
                    });
                }
            } catch (e) {
                console.log(e);
                reject(e);
            }
        });

        result.then(function(rtn_module_data){

            if(rtn_module_data.length > 0)
                res.json( rtn_module_data);
            else 
                res.send({"domain_url":config.domain, "error_code":400, "error_mainmsg":"Something Went Wrong. Action id is required !", });
        });

    } else {
        res.render('errors/errors',{"domain_url":config.domain, "error_code":401, "error_mainmsg":"Module Permission Denied !"});
    }

});






module.exports = router;