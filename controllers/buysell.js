var config = require('../config/database');
var dbUtil = require('../config/dbUtil');
var express = require('express');
var router = express.Router();
var each = require('async-each');
var auth = require('../middlewares/auth'), chk_loginaccess = auth.chk_loginaccess;
var mongo = require('mongodb');

/**
 * MAIN : DASHBOARD
 * MODULE : BUYSELL TRANSACTION
 */
router.get('/', chk_loginaccess, function(req, res) {

  var fromdate = req.query.fromdate || "";
  var todate = req.query.todate || "";
  var exchangeamount = req.query.exc_amount || 3000;

  var datasource_subqry = new Object();
  datasource_subqry["data_proprietor"] = new mongo.ObjectID(req.session.user.ses_userid);

  if(fromdate == '' || todate == ''){
    console.log({"status":"failed", "message":"fromdate and todate required!!!"});
    res.send([]);
  } else {
    var from_spliter = fromdate.split("-");
    var to_spliter = todate.split("-");
    
    var fmdt_cnter = new Date(from_spliter[2]+"-"+from_spliter[1]+"-"+from_spliter[0]);
    var todt_cnter = new Date(to_spliter[2]+"-"+to_spliter[1]+"-"+to_spliter[0]+"T23:59:59");

    var collection = dbUtil.get().collection(config.tbl_sanitized_data);
    
    collection.aggregate( [
      { 
        $match: { $and: [ 
              {"Exchange Amount": { $lte: parseFloat(exchangeamount) } }, 
              {"Transaction Date":{ $gte:fmdt_cnter, $lte:todt_cnter} } ,
              datasource_subqry
          ] } 
      },
      { $group: { _id: "$BuySell", "Exchange_Amount": { $sum: "$Exchange Amount" }, "Trans_Count":{ $sum: 1} } },
    ]).toArray(function(err, below_result) {
      
      if (err) {
        console.log(err);
      } else {
        var result_value = below_result;
        var great_buysell_arr = [];
        
        if(result_value.length > 0) {

          afterxxx(result_value, function(lessthan_data){

            collection.aggregate([
              { 
                $match: { 
                  $and: [ 
                    {"Exchange Amount": { "$gt": parseFloat(exchangeamount) } } ,
                    {"Transaction Date":{ "$gte": fmdt_cnter, "$lte": todt_cnter } },
                    datasource_subqry
                  ], 
                } 
              },
              { $group: { 
                _id: {"Customer Type":"$Customer Type","BuySell":"$BuySell"}, 
                "Exchange_Amount": { $sum : "$Exchange Amount" },
                "Trans_Count":{$sum:1}        
                }  
              },
              { $limit : 13 }  
          
            ]).toArray(function(err, above_result) {
              
              if(err) {
                console.log(err);
              } else {
                if(above_result.length > 0) {
                  afteryyy(lessthan_data, above_result, res);
                } else {
                  res.send({"status":"failed", "message":"there is don't have data for corresponding date range!!!"})
                }
              }
            });	
          });
        } else {
          collection.aggregate([
            { 
              $match: { 
              $and: [ 
                {"Exchange Amount": { "$gt": parseFloat(exchangeamount) } } ,
                {"Transaction Date":{ "$gte": fmdt_cnter, "$lte": todt_cnter } }, 
                datasource_subqry
              ], 
              } 
            },
            { $group: { 
              _id: {"Customer Type":"$Customer Type","BuySell":"$BuySell"}, 
              "Exchange_Amount": { $sum : "$Exchange Amount" },
              "Trans_Count":{$sum:1}        
              }  
            },
            { $limit : 13 }  
          ]).toArray(function(err, above_result) {
            
            if(err) {
              console.log(err);
            } else {
              if(above_result.length > 0) {
                afteryyy(result_value, above_result, res);
              } else {
                res.send({"status":"failed", "message":"there is don't have data for corresponding date range!!!"})
              }
        
            }
          });
        }
      }
    });
  }

});

function afterxxx(result_value, callback) {

  var main_runner_arr = {};
  var counter = 0;

  each(result_value, function(entry_arr, next){

    var below_thsd_arr = {};

    if(entry_arr["_id"]=="WeBuy") {

      if(main_runner_arr["Less Than RM3000 (W.I.C)"] === undefined) {

        below_thsd_arr["buy_type"] = entry_arr["_id"];
        below_thsd_arr["buy_excamt"] = entry_arr["Exchange_Amount"];
        below_thsd_arr["buy_transcnt"] = entry_arr["Trans_Count"];
        below_thsd_arr["buy_transcnt_percent"] = "";
        below_thsd_arr["buy_percentage"] = "";

        below_thsd_arr["sell_type"] = "";
        below_thsd_arr["sell_excamt"] = "";
        below_thsd_arr["sell_transcnt"] = "";
        below_thsd_arr["sell_transcnt_percent"] = "";
        below_thsd_arr["sell_percentage"] = "";

        main_runner_arr["Less Than RM3000 (W.I.C)"] = below_thsd_arr;
      } else {
        main_runner_arr["Less Than RM3000 (W.I.C)"].buy_type = entry_arr["_id"];
        main_runner_arr["Less Than RM3000 (W.I.C)"].buy_excamt = entry_arr["Exchange_Amount"];
        main_runner_arr["Less Than RM3000 (W.I.C)"].buy_transcnt = entry_arr["Trans_Count"];
        main_runner_arr["Less Than RM3000 (W.I.C)"].buy_transcnt_percent = "";
        main_runner_arr["Less Than RM3000 (W.I.C)"].buy_percentage = "";
      }
    } else if(entry_arr["_id"]=="WeSell") {

      if(main_runner_arr["Less Than RM3000 (W.I.C)"] === undefined) {

        below_thsd_arr["buy_type"] = "";
        below_thsd_arr["buy_excamt"] = "";
        below_thsd_arr["buy_transcnt"] = "";
        below_thsd_arr["buy_transcnt_percent"] = "";
        below_thsd_arr["buy_percentage"] = "";

        below_thsd_arr["sell_type"] = entry_arr["_id"];
        below_thsd_arr["sell_excamt"] = entry_arr["Exchange_Amount"];
        below_thsd_arr["sell_transcnt"] = entry_arr["Trans_Count"];
        below_thsd_arr["sell_transcnt_percent"] = "";
        below_thsd_arr["sell_percentage"] = "";

        main_runner_arr["Less Than RM3000 (W.I.C)"] = below_thsd_arr;
      } else {
        main_runner_arr["Less Than RM3000 (W.I.C)"].sell_type = entry_arr["_id"];
        main_runner_arr["Less Than RM3000 (W.I.C)"].sell_excamt = entry_arr["Exchange_Amount"];
        main_runner_arr["Less Than RM3000 (W.I.C)"].sell_transcnt = entry_arr["Trans_Count"];
        main_runner_arr["Less Than RM3000 (W.I.C)"].sell_transcnt_percent = "";
        main_runner_arr["Less Than RM3000 (W.I.C)"].sell_percentage = "";
      }
    }

    if(result_value.length == 0 || counter == (result_value.length-1)) {
      // console.log(main_runner_arr);
      return callback(main_runner_arr);
    }
    counter++;
  });

}

function afteryyy(main_runner_arr, above_result, res) {

  var count_above = 0;
  global.allgater_great_buy_arr = [];
  global.allgater_great_sell_arr = [];
  
  each(above_result, function(above_result_entry, next){

    var threethd_great_buy_arr = {};
    var threethd_great_sell_arr = {};

    var ab_id = above_result_entry["_id"];

    var ab_buysell = ab_id["BuySell"];
    var ab_customertype = ab_id["Customer Type"];
    var ab_excamount = above_result_entry["Exchange_Amount"];
    var ab_transcount = above_result_entry["Trans_Count"];
    
    if(ab_buysell == 'WeBuy') {

      threethd_great_buy_arr["buysellype"] = ab_buysell;
      threethd_great_buy_arr["customer_type"] = ab_customertype;
      threethd_great_buy_arr["exchange_amt"] = ab_excamount;
      threethd_great_buy_arr["trans_count"] = ab_transcount;

      allgater_great_buy_arr.push(threethd_great_buy_arr);

    } else if(ab_buysell == 'WeSell') {

      threethd_great_sell_arr["buysellype"] = ab_buysell;
      threethd_great_sell_arr["customer_type"] = ab_customertype;
      threethd_great_sell_arr["exchange_amt"] = ab_excamount;
      threethd_great_sell_arr["trans_count"] = ab_transcount;

      allgater_great_sell_arr.push(threethd_great_sell_arr);
    }

    if(count_above == (above_result.length-1)) {
     
      if(allgater_great_buy_arr.length > 0) {

        clbk_allgater_great_buy(main_runner_arr, allgater_great_buy_arr, function(less_main_runner_arr, great_buy_ac){
          
          clbk_allgater_great_sell(less_main_runner_arr, great_buy_ac, allgater_great_sell_arr, function(finaldata){

            var total_buy_transcount = 0;
            var total_buy_exchangeamount = 0;
            
            var total_sell_transcount = 0;
            var total_sell_exchangeamount = 0;

            var bf_cnt = 0;
            var fn_cnt = 0;

            var finaldata_length = Object.keys(finaldata).length;

            if(finaldata_length > 0) {

              for (x in finaldata) {

                if(finaldata[x] !== undefined) {

                  if(finaldata[x].buy_transcnt != "")
                    total_buy_transcount += finaldata[x].buy_transcnt;

                  if(finaldata[x].buy_excamt != "")
                    total_buy_exchangeamount += finaldata[x].buy_excamt;

                  if(finaldata[x].sell_transcnt != "")
                    total_sell_transcount += finaldata[x].sell_transcnt;

                  if(finaldata[x].sell_excamt != "")
                    total_sell_exchangeamount += finaldata[x].sell_excamt;
                }

                if(bf_cnt == (finaldata_length-1)) {

                  total_buy_exchangeamount = (parseFloat(total_buy_exchangeamount)).toFixed(2);
                  total_sell_exchangeamount = (parseFloat(total_sell_exchangeamount)).toFixed(2);
              
                  for (y in finaldata) {

                    if(finaldata[y] !== undefined) {

                      finaldata[y].buy_transcnt_percent = (parseFloat((finaldata[y].buy_transcnt/total_buy_transcount)*100)).toFixed(2);
                      finaldata[y].sell_transcnt_percent = (parseFloat((finaldata[y].sell_transcnt/total_sell_transcount)*100)).toFixed(2);

                      finaldata[y].buy_percentage = (parseFloat((finaldata[y].buy_excamt/total_buy_exchangeamount)*100)).toFixed(2);
                      finaldata[y].sell_percentage = (parseFloat((finaldata[y].sell_excamt/total_sell_exchangeamount)*100)).toFixed(2);

                      finaldata[y].buy_excamt = finaldata[y].buy_excamt;
                      finaldata[y].sell_excamt = finaldata[y].sell_excamt;

                    }

                    if(fn_cnt == (finaldata_length-1)) {

                      var arranger_value = [];

                      var cnt_arranger = 0;
                      var finaldata_length = Object.keys(finaldata).length;

                      var total_bs_trans_arr = [];
                      var total_bs_excamt_arr = [];
                      
                      for (z in finaldata) {

                        var x_value = {};

                        if(finaldata[z] !== undefined) {

                          x_value["customer"] = z;
                          x_value["buy_type"] = finaldata[z].buy_type;
                          x_value["buy_excamt"] = finaldata[z].buy_excamt;
                          x_value["buy_transcnt"] = finaldata[z].buy_transcnt;
                          x_value["buy_transcnt_percent"] = finaldata[z].buy_transcnt_percent;
                          x_value["buy_percentage"] = finaldata[z].buy_percentage;
                          x_value["sell_type"] = finaldata[z].sell_type;
                          x_value["sell_excamt"] = finaldata[z].sell_excamt;
                          x_value["sell_transcnt"] = finaldata[z].sell_transcnt;
                          x_value["sell_transcnt_percent"] = finaldata[z].sell_transcnt_percent;
                          x_value["sell_percentage"] = finaldata[z].sell_percentage;

                          var total_bs_trans = parseFloat((parseFloat(finaldata[z].buy_transcnt+finaldata[z].sell_transcnt)).toFixed(2));
                          var total_bs_excamt = parseFloat((parseFloat(finaldata[z].buy_excamt+finaldata[z].sell_excamt)).toFixed(2));
                          
                          x_value["buysell_trans"] =  total_bs_trans;
                          x_value["buysell_excamt"] = total_bs_excamt;

                          total_bs_trans_arr.push(total_bs_trans);
                          total_bs_excamt_arr.push(total_bs_excamt);
                          
                          x_value["buysell_transcnt_percent"] = "";
                          x_value["buysell_percentage"] = "";
                          
                          arranger_value.push(x_value);
                        }

                        if(cnt_arranger == (finaldata_length-1)) {

                          var final_xxx_valuer = [];

                          var cnt_arranger = 0;
                          var finaldata_length = Object.keys(finaldata).length;

                          function common_sum(numbers) {

                            return numbers.reduce(function(a,b) {
                              return (a + b)
                            });
                          }

                          var total_bs_trans_xxx = parseFloat(common_sum(total_bs_trans_arr));
                          var total_bs_excamt_xxx = parseFloat(common_sum(total_bs_excamt_arr));
                          
                          var cnt_final_bs = 0;

                          var danger_value = [];
                          
                          for (avl_cnt in arranger_value) {

                            var y_value = {};
                            y_value["customer"] = arranger_value[avl_cnt].customer;
                            y_value["buy_type"] = arranger_value[avl_cnt].buy_type;
                            y_value["buy_excamt"] = arranger_value[avl_cnt].buy_excamt;
                            y_value["buy_transcnt"] = (arranger_value[avl_cnt].buy_transcnt != "") ? arranger_value[avl_cnt].buy_transcnt : 0.00;
                            y_value["buy_transcnt_percent"] = (arranger_value[avl_cnt].buy_transcnt_percent != "") ? arranger_value[avl_cnt].buy_transcnt_percent : 0.00;
                            y_value["buy_percentage"] = (arranger_value[avl_cnt].buy_percentage != "") ? arranger_value[avl_cnt].buy_percentage : 0.00;
                            y_value["sell_type"] = (arranger_value[avl_cnt].sell_type != "") ? arranger_value[avl_cnt].sell_type : 0.00;
                            y_value["sell_excamt"] = (arranger_value[avl_cnt].sell_excamt != "") ? arranger_value[avl_cnt].sell_excamt : 0.00;
                            y_value["sell_transcnt"] = (arranger_value[avl_cnt].sell_transcnt != "") ? arranger_value[avl_cnt].sell_transcnt : 0.00;
                            y_value["sell_transcnt_percent"] = (arranger_value[avl_cnt].sell_transcnt_percent != "") ? arranger_value[avl_cnt].sell_transcnt_percent : 0.00;
                            y_value["sell_percentage"] = (arranger_value[avl_cnt].sell_percentage != "") ? arranger_value[avl_cnt].sell_percentage : 0.00;
                            y_value["buysell_trans"] = arranger_value[avl_cnt].buysell_trans;
                            y_value["buysell_excamt"] = arranger_value[avl_cnt].buysell_excamt;
                            
                            y_value["buysell_transcnt_percent"] = (parseFloat((arranger_value[avl_cnt].buysell_trans/total_bs_trans_xxx)*100)).toFixed(2);
                            y_value["buysell_percentage"] = (parseFloat((arranger_value[avl_cnt].buysell_excamt/total_bs_excamt_xxx)*100)).toFixed(2)
    
                            danger_value.push(y_value);

                            if(cnt_final_bs == (arranger_value.length-1))	{
                              res.send(danger_value);
                            }
                            cnt_final_bs++;
                          }
                        }
                        cnt_arranger++;
                      }
                    }
                    fn_cnt++;
                  }
                }
                bf_cnt++;
              }

            } else {
              res.send({"status":"failed", "message":"there is don't have final data for corresponding date range!!!"})
            }
          });
          
        });
      } else {
        var great_buy_ac = {};
        clbk_allgater_great_sell(less_main_runner_arr, great_buy_ac, allgater_great_sell_arr, function(finaldata){
          res.send(finaldata);
        });
      }
    }
    count_above++;

  });	

}

function clbk_allgater_great_buy(main_runner_arr, allgater_great_buy_arr, callback) {

  var ac = {};
  var cn = 0;
  
  each(allgater_great_buy_arr, function(rs, next){
    
    var cc = {};

    cc["buy_type"] = rs["buysellype"];
    cc["buy_excamt"] = rs["exchange_amt"];
    cc["buy_transcnt"] = rs["trans_count"];
    cc["buy_transcnt_percent"] = "";
    cc["buy_percentage"] = "" ;
    
    cc["sell_type"] = "";
    cc["sell_excamt"] ="";
    cc["sell_transcnt"] ="";
    cc["sell_transcnt_percent"] ="";
    cc["sell_percentage"] = "" ;

    ac[rs["customer_type"]] = cc;
    
    if(cn == (allgater_great_buy_arr.length-1)) {
      return callback(main_runner_arr, ac);
    }
    cn++;
  });

}

function clbk_allgater_great_sell(less_main_runner_arr, ac, allgater_great_sell_arr, callback) {

  // console.log(less_main_runner_arr);
  var nc = 0;
  
  each(allgater_great_sell_arr, function(ss, next){
    
    var dd = {};
    if(ac[ss["customer_type"]] === undefined) {
      dd["buy_type"] ="";
      dd["buy_excamt"] ="";
      dd["buy_transcnt"] ="";
      dd["buy_transcnt_percent"] ="";
      dd["buy_percentage"] = "" ;
      
      ac[ss["customer_type"]] = dd;
    }
    ac[ss["customer_type"]].sell_type = ss["buysellype"];
    ac[ss["customer_type"]].sell_excamt = ss["exchange_amt"];
    ac[ss["customer_type"]].sell_transcnt = ss["trans_count"];
    ac[ss["customer_type"]].sell_transcnt_percent = "";
    ac[ss["customer_type"]].sell_percentage = "";
    
    if(nc == (allgater_great_sell_arr.length-1)) {
      ac["Less Than RM3000 (W.I.C)"] = less_main_runner_arr["Less Than RM3000 (W.I.C)"];
      callback(ac);
    }
    nc++;
  });	
}


module.exports = router;
