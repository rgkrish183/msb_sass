var config = require('../config/database');
var dbUtil = require('../config/dbUtil');
var express = require('express');
var router = express.Router();
var auth = require('../middlewares/auth'), chk_loginaccess = auth.chk_loginaccess;
var report = require('../models/monthly_highrisk_customerlist_report');
var each = require('async-each');
var moment = require("moment");
var _ = require('lodash');
var common_functions = require('../middlewares/common_functions');
var MODCONF = require('../config/module_config');
const PRMS = MODCONF.PRMS;
const MDUL = MODCONF.MDUL;
var mongo = require('mongodb');

/**
 * USE : ROOT LIST LOADER AND POPUP EXPAND VIEW FOR 3 REPORTS OF LOCAL, INDIVIDUAL AND FOREIGN BUSINESS TRANSACTIONS 
 * MODULE : HIGH RISK CUSTOMERS
 * MAIN : http://192.168.1.187:3000/highrisk_customerlist_report/highrisk_customerlist
 */
router.get('/', chk_loginaccess, function(req, res, next) {

  var fromdate = req.query.fromdate || '';
  var todate = req.query.todate || '';
  var nationality = req.query.nationality || '';
  var customerType = req.query.type || '';  
  var trans_count = req.query.trans_count || "";

  var datasource_subqry = new Object();
  datasource_subqry["data_proprietor"] = new mongo.ObjectID(req.session.user.ses_userid);

  if(fromdate == '' || todate == '' || nationality=='' || customerType=='') {
    console.log(JSON.stringify({"status":"failed", "message":"fromdate, todate, nationality and type required!!!"}));
    res.send([]);
  } else {
    var from_spliter = fromdate.split("-");
    var to_spliter = todate.split("-");
    var fmdt_cnter = new Date(from_spliter[2]+"-"+from_spliter[1]+"-"+from_spliter[0]);
    var todt_cnter = new Date(to_spliter[2]+"-"+to_spliter[1]+"-"+to_spliter[0]); 
    
    var params = {
      fmdt_cnter:fmdt_cnter, 
      todt_cnter:todt_cnter, 
      type:decodeURI(customerType),
      nationality:nationality, 
      trans_count:trans_count, 
      datasource_subqry:datasource_subqry
    };

    var final_frequenttrans_arr = Array();

    report.highrisk_customer_listreport(final_frequenttrans_arr, params, function(data){

      if(final_frequenttrans_arr.length > 0) {
        res.send(final_frequenttrans_arr);
      } else {
        console.log("there is don't have data for corresponding date range!!!");
        res.send([]);
      }
    });
  }
});

/**
 * USE : HIGHRISK CUSTOMERS TRANSACTIONBASED ON MONTH AND OUTLET 
 * MODULE : MONTHLY HIGH RISK CUSTOMERS
 * MAIN : http://192.168.1.187:3000/monthly_highrisk_customerlist_report/monthly_highrisk_customerlist
 * INSIDE : http://192.168.1.187:3000/monthly_highrisk_customerlist_report/monthly_highriskcustomer_outlet
 */
router.get('/monthly_highriskcustomer_outlet', function(req, res, next) {

  var fromdate = req.query.fromdate || "";
  var todate = req.query.todate || "";
  var trans_count = req.query.trans_count || "";

  var datasource_subqry = new Object();
  datasource_subqry["data_proprietor"] = new mongo.ObjectID(req.session.user.ses_userid);
  
  let from_spliter = fromdate.split("-");
  let to_spliter = todate.split("-");

  var fmdt_cnter_string = from_spliter[2]+"-"+from_spliter[1]+"-"+from_spliter[0];
  var todt_cnter_string = to_spliter[2]+"-"+to_spliter[1]+"-"+to_spliter[0];

  let fmdt_cnter = new Date(fmdt_cnter_string);
  let todt_cnter = new Date(todt_cnter_string);
  
  let monthoutlet_params = {fmdt_cnter:fmdt_cnter, todt_cnter:todt_cnter, trans_count:trans_count, datasource_subqry:datasource_subqry};
  
  report.monthly_highriskcustomer_outlet_listreport(monthoutlet_params, function(result){

    var outlet_arr = new Array();
    each(result, function(result_value, next){

      var ok = new Array();
      ok.push(result_value.Outlet);
      var tsdate = result_value.Transctions_Date;

      var counts = new Object();
        each(tsdate, function(result_value_dt, next){
          counts[result_value_dt] = (counts[result_value_dt] || 0)+1; 
          next();
      }, function(){
        ok.push(counts);
        outlet_arr.push(ok);
      });    
      next();
    }, function(){
      var final_arr = new Array();
      each(outlet_arr, function(outlet_arr_value, outlet_arr_next){

        console.log("-------------------------------------------------------");
        console.log(outlet_arr_value);

        var ck = new Object();
        // ck.push(outlet_arr_value[0]);
        // ck["Name"] = outlet_arr_value[0];

        ck["Name"]  = outlet_arr_value[0];

        var dates_arr = outlet_arr_value[1];

        go_fnmonths_betweendates(fmdt_cnter_string, todt_cnter_string, function(months_arr){

          each(months_arr, function(months_arr_value, months_arr_next){

            console.log("888888888888888888888");
            console.log(dates_arr[months_arr_value]);
            console.log(trans_count);
            console.log((dates_arr[months_arr_value] !== undefined && dates_arr[months_arr_value] > trans_count));

            if(dates_arr[months_arr_value] !== undefined && dates_arr[months_arr_value] > trans_count)
              ck[months_arr_value]  = dates_arr[months_arr_value];
            else 
              ck[months_arr_value]  = 0;

            months_arr_next();
          },function(){
            // Don't remove for testing purpose 
            // ck.push(dates_arr);
            // final_arr.push(ck);
            // outlet_arr_next();

            const ck_promise = new Promise(async(resolve, reject) => {

              var ck_access = false;
              var counter = 1;
              var ck_length = Object.keys(ck).length;

              await _.map(ck, function(data, index){

                console.log(data);

                if(parseInt(data) > 0 ){
                  ck_access = true;
                }
                if(ck_length == counter){
                  resolve(ck_access);
                }
                counter++;
              });

            }).then(function(ck_promise_value){

              if(ck_promise_value) {

                console.log("---------------------------------------------------------------");
                console.log(ck);

                var ac_value = false;
                const result = new Promise(async(resolve, reject)=>{
                
                  var myobjecter_length = (Object.keys(ck).length-2);
                
                  Object.keys(ck).map(async(key, index) => {
                
                    if(typeof(ck[key])==="number"){
                      if(ck[key] > 0){
                        ac_value = true;
                      }
                      if(index == myobjecter_length)
                        resolve(ac_value);
                    }
                  });
                }).then(function(checker){
                
                  console.log("------------enter------------");
                  if(checker){
                    final_arr.push(ck);
                    console.log("ok->"+checker);
                    console.log(ck);
                    outlet_arr_next();
                  } else {
                    console.log("no->"+checker);
                    console.log(ck);
                    outlet_arr_next();
                  } 
                });
                // final_arr.push(ck);

              } else { 
                outlet_arr_next();
              }
            });

          });
        });

      },function(){
        res.status(200).send(final_arr);
      });
    });
  });
});

/**
 * USECASE : USE START AND END DATE TO GET BETWEEN MONTHS
 * @param {string date} startDate 
 * @param {string date} endDate 
 * @param {callback months array} callback 
 */
function go_fnmonths_betweendates(startDate, endDate, callback) {
  var start      = startDate.split('-');
  var end        = endDate.split('-');
  var startYear  = parseInt(start[0]);
  var endYear    = parseInt(end[0]);
  var dates      = [];

  for(var i = startYear; i <= endYear; i++) {
    var endMonth = i != endYear ? 11 : parseInt(end[1]) - 1;
    var startMon = i === startYear ? parseInt(start[1])-1 : 0;
    for(var j = startMon; j <= endMonth; j = j > 12 ? j % 12 || 11 : j+1) {
      var month = j+1;
      var displayMonth = month < 10 ? '0'+month : month;
      dates.push([i, displayMonth].join('-'));
    }
  }
  return callback(dates);
}

/**
 * USE : LIST LOADER POPUP
 * MODULE : MONTHLY OUTLETBASED HRC
 */
router.get('/customer_risklist_loader', chk_loginaccess, function(req, res) {

  var outlet_yearmonth = req.query.outlet_yearmonth || "";
  var outlet_name = req.query.outlet_name || "";

  let ejs_extra_params = new Object();

  ejs_extra_params["pagename"] = "customer_risk";
  ejs_extra_params["domain_url"] = config.domain;
  ejs_extra_params["fromdate"] = outlet_yearmonth;
  ejs_extra_params["todate"] = outlet_yearmonth;
  ejs_extra_params["risk_action"] = outlet_name;
  ejs_extra_params["customerview_action"] = "MONTHOUTLETHRC";

  // VARIABLE DEPENDENCY FOR ANOTHER CUSTMER DATA LIST PAGES
  ejs_extra_params["Fre_action"] = "";
  ejs_extra_params["Name"] = "";
  ejs_extra_params["trans_count"] = "";
  ejs_extra_params["fre_cust_name"] = "";
  ejs_extra_params["fre_trans_count"] = "";

  res.render("customerrisk_speclist_loader", ejs_extra_params);
});

/**
 * USE : ROOT LIST POPUP EXPAND VIEW FOR 3 REPORTS OF LOCAL, INDIVIDUAL AND FOREIGN BUSINESS TRANSACTIONS 
 * MODULE : HIGH RISK CUSTOMERS
 * MAIN : http://192.168.1.187:3000/highrisk_customerlist_report/highrisk_customerlist
 * INSIDE : http://192.168.1.187:3000/highrisk_customerlist_report/highrisk_fullview_lister
 */
router.get('/highrisk_fullview_lister', chk_loginaccess, function(req, res) {
  var fromdate = req.query.fromdate || "";
  var todate = req.query.todate || "";
  var nationality = req.query.nationality || "";
  var type = req.query.type || "";
  var datasource_objid = req.query.datasource_objid || "";
  var trans_count = req.query.trans_count || "";
  
  res.render("highrisk_fullview_lister", {"pagename":"highrisk_fullview_lister","domain_url":config.domain, "fromdate":fromdate, "todate":todate, "nationality":nationality, "type":type, "datasource_objid":datasource_objid, "trans_count":trans_count});
});

/**
 * USE : HIGHRISK CUSTOMERS TRANSACTIONs BASED ON MONTH AND OUTLET 
 * MODULE : MONTHLY HIGH RISK CUSTOMERS TRANSACTIONS
 * MAIN : http://192.168.1.187:3000/monthly_highrisk_customerlist_report/monthly_highrisk_customerlist
 * INSIDE : http://192.168.1.187:3000/monthly_highrisk_customerlist_report/monthly_hrc_basedonoutlet
 */
router.get('/monthly_hrc_basedon_outlet', function(req, res) {

  let fmdt_cnter;
  let todt_cnter;

  var datasource_subqry = new Object();
  datasource_subqry["data_proprietor"] = new mongo.ObjectID(req.session.user.ses_userid);
  
  if(req.query.outlet_yearmonth === "" || req.query.outlet_yearmonth === undefined){

    fmdt_cnter = req.query.from_date;
    todt_cnter = req.query.to_date;

    fmdt_cnter = moment(fmdt_cnter,'DDMMYYYY').format('YYYY-MM-DD');
    fmdt_cnter = new Date(fmdt_cnter+"T00:01:59");

    todt_cnter = moment(todt_cnter,'DDMMYYYY').format('YYYY-MM-DD');
    todt_cnter = new Date(todt_cnter+"T23:59:59");

  } else {
    
    let yearmonth = req.query.outlet_yearmonth || "";
    let fm_date_concater = yearmonth+"-01";
    fm_date_concater = moment(fm_date_concater).format("YYYY-MM-DD");
    let to_lastdateof = moment(fm_date_concater).daysInMonth();
    let to_date_concater = yearmonth+"-"+to_lastdateof;
    to_date_concater = moment(to_date_concater).format("YYYY-MM-DD");

    fmdt_cnter = new Date(fm_date_concater+"T00:01:59");
    todt_cnter = new Date(to_date_concater+"T23:59:59");
  }
  
  let outlet_name = req.query.outlet_name || "";

  var cust_name =  req.query.Name || "";
  var trans_count =  req.query.trans_count || "";

  var fre_cust_name =  req.query.fre_cust_name;
  var fre_trans_count =  req.query.fre_transactionCount;

  let monthoutlet_params = new Object();

  monthoutlet_params["fmdt_cnter"] = fmdt_cnter;
  monthoutlet_params["todt_cnter"] = todt_cnter;
  monthoutlet_params["datasource_subqry"] = datasource_subqry;

  monthoutlet_params["outlet_name"] = outlet_name;

  monthoutlet_params["Name"] = cust_name;
  monthoutlet_params["trans_count"] = trans_count;

  monthoutlet_params["fre_cust_name"] = fre_cust_name;
  monthoutlet_params["fre_trans_count"] = fre_trans_count;

  console.log(monthoutlet_params);

  report.monthly_hrc_basedonoutlet_lister(monthoutlet_params, function(result_value){

    var data = new Array();
    if(result_value.length > 0) {
      if(result_value[0].data !== undefined) {
        result_value = result_value[0].data;
      } else {
        result_value = result_value;
      }
      each(result_value, function(result, next){

        var data_obj = new Array();
        data_obj.push(result["_id"]);
        data_obj.push(result["Name"]);
        data_obj.push(result["Exchange Amount"]);
        data_obj.push(result["BuySell"]);
        data_obj.push(result["Transaction Date"]);
        data_obj.push(result["Source"]);
        data_obj.push(result["Purpose"]);
        
        data.push(data_obj);
        next();
      }, function(){
        res.status(200).send(data);
      });
    } else {
      res.status(200).send(data);
    }
    
  });

});



module.exports = router;