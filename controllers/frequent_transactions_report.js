var config = require('../config/database');
var dbUtil = require('../config/dbUtil');
var express = require('express');
var router = express.Router();
var each = require('async-each');
var auth = require('../middlewares/auth'), chk_loginaccess = auth.chk_loginaccess;
var common_functions = require('../middlewares/common_functions');
var MODCONF = require('../config/module_config');
const PRMS = MODCONF.PRMS;
const MDUL = MODCONF.MDUL;
var mongo = require('mongodb');

/**
 * USE : ROOT LOADER FOR FREQUENT TRANSACTION
 * MODULE : TRANSACTION RISK ANALYSIS
 */
router.get('/', function(req, res, next) {
  
  var fromdate = req.query.fromdate || "";
  var todate = req.query.todate || "";
  var trans_count = req.query.trans_count || "";
  var action = req.query.action || "";

  var datasource_subqry = new Object();
  datasource_subqry["data_proprietor"] = new mongo.ObjectID(req.session.user.ses_userid);

  if(fromdate == '' || todate == '') {
    console.log({"status":"failed", "message":"fromdate and todate required!!!"});
    res.send([]);
  } else {

    var from_spliter = fromdate.split("-");
    var to_spliter = todate.split("-");
    var fmdt_cnter = new Date(from_spliter[2]+"-"+from_spliter[1]+"-"+from_spliter[0]);
    var todt_cnter = new Date(to_spliter[2]+"-"+to_spliter[1]+"-"+to_spliter[0]);

    var final_frequenttrans_arr = Array();

    var params = {"fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "trans_count":trans_count, "action":"", "datasource_subqry":datasource_subqry};
          
    fn_frequent_transaction(final_frequenttrans_arr, params, function(data){

      if(final_frequenttrans_arr.length > 0) {
        res.send(final_frequenttrans_arr);
      } else {
        res.send({"status":"failed", "message":"there is don't have data for corresponding date range!!!"})
      }
    });    
  }
});  

/**
 * FUNCTION USE : FREQUENT TRANSACTION CUSTOMERS, TRANSACTION COUNT, CORRESPONDIND DATA WILL RETURN  
 * MODULE : TRANSACTION RISK ANALYSIS
 */
function fn_frequent_transaction(final_frequenttrans_arr, params, callback){
    
    var datasource_subqry = params.datasource_subqry;
    var fmdt_cnter = params.fmdt_cnter;
    var todt_cnter = params.todt_cnter;
    var trans_count = params.trans_count;
    var action = params.action;

    var collection = dbUtil.get().collection(config.tbl_sanitized_data);

    var finder = new Object();
    finder["Transaction Date"] = { $gt:fmdt_cnter, $lt:todt_cnter };

    var matcher = new Object();
    matcher["count"] = {'$gt': parseInt(trans_count)};

    var transcounter_qry = collection.aggregate( [ { 
        $match: { 
          $and: [finder, datasource_subqry ]
        } 
    }, {
        '$group': {
            '_id': {'Name': '$Name'}, 
            'count': {'$sum': 1} ,
            'data': {$push: '$$ROOT'}
        }
    }, {
        '$match': matcher
    } ]);

    transcounter_qry.toArray(function(err, transcounter_result) {
    
        if(transcounter_result.length > 0) {
          each(transcounter_result, function(result, next){
    
            var data_obj = new Array();
            data_obj.push(result["_id"].Name);
            data_obj.push(result["count"]);
            //data_obj.push(result["data"]);
            
            final_frequenttrans_arr.push(data_obj);
            next();
          }, function(){
            return callback(final_frequenttrans_arr);
          });
        } else {
          return callback(final_frequenttrans_arr);
        }
    });
}

/**
 * USE : ROOT LOADER FOR FREQUENT TRANSACTION
 * MODULE : TRANSACTION RISK ANALYSIS
 */
router.get('/multi_currency_transactions', function(req, res, next) {
  
  var fromdate = req.query.fromdate || "";
  var todate = req.query.todate || "";
  var trans_count = req.query.trans_count || "";
  var action = req.query.action || "";

  var datasource_subqry = new Object();
  datasource_subqry["data_proprietor"] = new mongo.ObjectID(req.session.user.ses_userid);

  if(fromdate == '' || todate == '') {
    console.log({"status":"failed", "message":"fromdate and todate required!!!"});
    res.send([]);
  } else {

    var from_spliter = fromdate.split("-");
    var to_spliter = todate.split("-");
    var fmdt_cnter = new Date(from_spliter[2]+"-"+from_spliter[1]+"-"+from_spliter[0]);
    var todt_cnter = new Date(to_spliter[2]+"-"+to_spliter[1]+"-"+to_spliter[0]);

    var final_frequenttrans_arr = Array();

    var params = {"fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "trans_count":trans_count, "action":"", "datasource_subqry":datasource_subqry};
          
    fn_multicurrency_transaction(final_frequenttrans_arr, params, function(data){

      if(final_frequenttrans_arr.length > 0) {
        res.send(final_frequenttrans_arr);
      } else {
        res.send({"status":"failed", "message":"there is don't have data for corresponding date range!!!"})
      }
    });    
  }
});  

/**
 * FUNCTION USE : MULTIPLE CURRENCEY FREQUENT TRANSACTION CUSTOMERS, TRANSACTION COUNT, CORRESPONDIND DATA WILL RETURN  
 * MODULE : TRANSACTION RISK ANALYSIS
 */
function fn_multicurrency_transaction(final_frequenttrans_arr, params, callback){
    
  var datasource_subqry = params.datasource_subqry;
  var fmdt_cnter = params.fmdt_cnter;
  var todt_cnter = params.todt_cnter;
  var trans_count = params.trans_count;
  var action = params.action;

  var collection = dbUtil.get().collection(config.tbl_sanitized_data);

  var finder = new Object();
  finder["Transaction Date"] = { $gt:fmdt_cnter, $lt:todt_cnter };

  var matcher = new Object();
  matcher["count"] = {'$gt': parseInt(trans_count)};

  var transcounter_qry = collection.aggregate( [ { 
      $match: { 
      $and: [finder, datasource_subqry ]
      } 
  }, {
      '$group': {
          '_id': {'Name': '$Name'}, 
          'count': {'$sum': 1} ,
          'data': {$push: '$$ROOT'}
      }
  } ]);

  transcounter_qry.toArray(function(err, transcounter_result) {
  
      if(transcounter_result.length > 0) {
        each(transcounter_result, function(result, next){
  
          var data_obj = new Array();
          data_obj.push(result["_id"].Name);
          data_obj.push(result["count"]);

          var root_data = result["data"];
          var currencey_counter = 0; 

          if(root_data.length > 0){

            var promise3 = new Promise(function(resolve, reject) {

              var currencey_arr = new Array();
              var transamount_arr = new Array();

              each(root_data, function(rootdata_result, next){

                currencey_arr.push(rootdata_result["Currency Code"]);
                transamount_arr.push(rootdata_result["Exchange Amount"])

                next();
              }, function(){
                
                function onlyUnique(value, index, self) { 
                  return self.indexOf(value) === index;
                }
                var unique_currencey_arr = currencey_arr.filter( onlyUnique );

                resolve(unique_currencey_arr.length);
              });

            });
            currencey_counter = 1; 

          } else {
            currencey_counter = 0; 
          }

          Promise.all([promise3]).then(function(values) {

            if(currencey_counter && values.length > 0)
              data_obj.push(values[0]);
            else
              data_obj.push(currencey_counter);
        
            if(values.length > 0 && values[0] > trans_count)
              final_frequenttrans_arr.push(data_obj);
  
            next();
          });

        }, function(){
          return callback(final_frequenttrans_arr);
        });
      } else {
        return callback(final_frequenttrans_arr);
      }
  });
}

/**
 * USE : LIST LOADER POPUP
 * MODULE : MONTHLY OUTLETBASED HRC
 */
router.get('/customer_risklist_loader', chk_loginaccess, function(req, res) {

  var outlet_yearmonth = req.query.outlet_yearmonth || "";
  var outlet_name = req.query.outlet_name || "";

  var cust_name =  req.query.Name;
  var trans_count =  req.query.transactionCount;

  var fre_cust_name =  req.query.Fre_Name;
  var fre_trans_count =  req.query.fre_transactionCount;

  let ejs_extra_params = new Object();

  ejs_extra_params["pagename"] = "customer_risk";
  ejs_extra_params["domain_url"] = config.domain;
  if(outlet_yearmonth === ""){
    ejs_extra_params["Fre_action"] = "frequency";
    ejs_extra_params["fromdate"] = req.query.from_date;
    ejs_extra_params["todate"] = req.query.to_date;
  }else{
    ejs_extra_params["Fre_action"] = "outlet_yearmonth";
    ejs_extra_params["fromdate"] = outlet_yearmonth;
    ejs_extra_params["todate"] = outlet_yearmonth;
  }
  
  ejs_extra_params["risk_action"] = outlet_name;
  ejs_extra_params["customerview_action"] = "FTR_MONTHOUTLETHRC";

  ejs_extra_params["Name"] = cust_name;
  ejs_extra_params["trans_count"] = trans_count;

  ejs_extra_params["fre_cust_name"] = fre_cust_name;
  ejs_extra_params["fre_trans_count"] = fre_trans_count;

  res.render("customerrisk_speclist_loader", ejs_extra_params);
});

module.exports = router;
