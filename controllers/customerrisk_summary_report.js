var config = require('../config/database');
var dbUtil = require('../config/dbUtil')
var express = require('express');
var router = express.Router();
var each = require('async-each');
var mongo = require('mongodb');
var auth = require('../middlewares/auth'), chk_loginaccess = auth.chk_loginaccess;
var common_functions = require('../middlewares/common_functions');
var MODCONF = require('../config/module_config');
const PRMS = MODCONF.PRMS;
const MDUL = MODCONF.MDUL;

/**
 * USE : LIST LOADER FOR ALL RISK BASED ANALYSIS AS HIGH, LOW, MEDIUM RISKS.
 * MODULE : RISKBASED ANALYSIS
 */
router.get('/', chk_loginaccess, function(req, res) {

  var fromdate = req.query.fromdate || "";
  var todate = req.query.todate || "";
  var risk_action = req.query.riskaction || "";

  var datasource_subqry = new Object();
  datasource_subqry["data_proprietor"] = new mongo.ObjectID(req.session.user.ses_userid);

  if(fromdate == '' || todate == '') {
    console.log({"status":"failed", "message":"fromdate and todate required!!!"});
    res.send([]);
  } else {
    var from_spliter = fromdate.split("-");
    var to_spliter = todate.split("-");
    
    var fmdt_cnter = new Date(from_spliter[2]+"-"+from_spliter[1]+"-"+from_spliter[0]);
    var todt_cnter = new Date(to_spliter[2]+"-"+to_spliter[1]+"-"+to_spliter[0]);
      
    var collection = dbUtil.get().collection(config.tbl_sanitized_data);

    if(risk_action == "RSK_NATIONALITY") {
      var low_params = {"collection":collection, "rsk_value":1, "ristktype":"low", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"risk_nationality", "datasource_subqry":datasource_subqry};
      var medium_params = {"collection":collection, "rsk_value":2, "ristktype":"medium", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"risk_nationality", "datasource_subqry":datasource_subqry};
      var high_params = {"collection":collection, "rsk_value":3, "ristktype":"high", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"risk_nationality", "datasource_subqry":datasource_subqry};
    
    } else if(risk_action == "RSK_VALUE") {
      var low_params = {"collection":collection, "rsk_value":1, "ristktype":"low", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"risk_valuerisk", "datasource_subqry":datasource_subqry};
      var medium_params = {"collection":collection, "rsk_value":2, "ristktype":"medium", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"risk_valuerisk", "datasource_subqry":datasource_subqry};
      var high_params = {"collection":collection, "rsk_value":3, "ristktype":"high", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"risk_valuerisk", "datasource_subqry":datasource_subqry};
    
    } else if(risk_action == "RSK_CUSTOMERTYPE") {
      var low_params = {"collection":collection, "rsk_value":1, "ristktype":"low", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"risk_customertype", "datasource_subqry":datasource_subqry};
      var medium_params = {"collection":collection, "rsk_value":2, "ristktype":"medium", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"risk_customertype", "datasource_subqry":datasource_subqry};
      var high_params = {"collection":collection, "rsk_value":3, "ristktype":"high", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"risk_customertype", "datasource_subqry":datasource_subqry};
    
    } else if(risk_action == "RSK_PURPOSE") {
      var low_params = {"collection":collection, "rsk_value":1, "ristktype":"low", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"risk_purpose", "datasource_subqry":datasource_subqry};
      var medium_params = {"collection":collection, "rsk_value":2, "ristktype":"medium", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"risk_purpose", "datasource_subqry":datasource_subqry};
      var high_params = {"collection":collection, "rsk_value":3, "ristktype":"high", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"risk_purpose", "datasource_subqry":datasource_subqry};
    
    } else if(risk_action == "RSK_SOURCE") {
      var low_params = {"collection":collection, "rsk_value":1, "ristktype":"low", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"risk_source", "datasource_subqry":datasource_subqry};
      var medium_params = {"collection":collection, "rsk_value":2, "ristktype":"medium", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"risk_source", "datasource_subqry":datasource_subqry};
      var high_params = {"collection":collection, "rsk_value":3, "ristktype":"high", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"risk_source", "datasource_subqry":datasource_subqry};
    
    } else if(risk_action == "RSK_OCCUPATION") {
      var low_params = {"collection":collection, "rsk_value":1, "ristktype":"low", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"risk_occupation", "datasource_subqry":datasource_subqry};
      var medium_params = {"collection":collection, "rsk_value":2, "ristktype":"medium", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"risk_occupation", "datasource_subqry":datasource_subqry};
      var high_params = {"collection":collection, "rsk_value":3, "ristktype":"high", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"risk_occupation", "datasource_subqry":datasource_subqry};
    
    } else if(risk_action == "RSK_MODEOFPAYMENT") {
      var low_params = {"collection":collection, "rsk_value":1, "ristktype":"low", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"risk_modeofpayment", "datasource_subqry":datasource_subqry};
      var medium_params = {"collection":collection, "rsk_value":2, "ristktype":"medium", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"risk_modeofpayment", "datasource_subqry":datasource_subqry};
      var high_params = {"collection":collection, "rsk_value":3, "ristktype":"high", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"risk_modeofpayment", "datasource_subqry":datasource_subqry};
    
    } else if(risk_action == "RSK_MODEOFDELIVERY") {
      var low_params = {"collection":collection, "rsk_value":1, "ristktype":"low", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"risk_modeofdelivery", "datasource_subqry":datasource_subqry};
      var medium_params = {"collection":collection, "rsk_value":2, "ristktype":"medium", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"risk_modeofdelivery", "datasource_subqry":datasource_subqry};
      var high_params = {"collection":collection, "rsk_value":3, "ristktype":"high", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"risk_modeofdelivery", "datasource_subqry":datasource_subqry};
    
    } else {
      var low_params = {"collection":collection, "gte":1, "lte":9, "ristktype":"low", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"", "datasource_subqry":datasource_subqry};
      var medium_params = {"collection":collection, "gte":10, "lte":15, "ristktype":"medium", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"", "datasource_subqry":datasource_subqry};
      var high_params = {"collection":collection, "gte":16, "lte":25, "ristktype":"high", "fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "risk_action":"", "datasource_subqry":datasource_subqry};
    }
    
    var risk_data = [];

    risk(low_params, function(lowrisk_data){

      if(Object.keys(lowrisk_data).length > 0)
        risk_data.push(lowrisk_data);

      risk(medium_params, function(medium_data){

        if(Object.keys(medium_data).length > 0)
          risk_data.push(medium_data);
        
        risk(high_params, function(high_data){
  
          if(Object.keys(high_data).length > 0)
            risk_data.push(high_data);
          
          if(risk_data.length > 0) {
            var total_customer_cnt = 0;
            var total_trans_cnt = 0;

            each(risk_data, function(risk_result, next){

              if(risk_result["customer"] != "")
                total_customer_cnt += risk_result["customer"];

              if(risk_result.trans_count != "")
                total_trans_cnt += risk_result["trans_count"];

              next();
          
            },function(){
                
                for (xxx in risk_data) {

                  var customer_percentage = (parseFloat((risk_data[xxx].customer/total_customer_cnt)*100)).toFixed(2);
                  var trans_percentage = (parseFloat((risk_data[xxx].trans_count/total_trans_cnt)*100)).toFixed(2);

                  risk_data[xxx].customer_percentage = (customer_percentage > 0) ? customer_percentage : 0.00;
                  risk_data[xxx].trans_percentage = (trans_percentage > 0) ? trans_percentage : 0.00;

                  if(xxx == (risk_data.length-1)) {
                    res.send(risk_data);
                  }
                }
            });
          } else {
            res.send({"status":"failed", "message":"there is don't have data for corresponding date range!!!"})
          }
        });
      });
    });
  }

});

/**
 * FUNCTION USE : LIST LOADER FOR ALL RISK BASED ANALYSIS AS HIGH, LOW, MEDIUM RISKS.
 * MODULE : RISKBASED ANALYSIS
 */
function risk(params, callback) {
  var datasource_subqry = params.datasource_subqry;

  if(params.risk_action != "" && params.risk_action == "risk_nationality") {
    var agent_data = params.collection.aggregate( [
      { $match: { 
          $and: [ 
            {"Nationality risk Score": params.rsk_value },
            {"Transaction Date":{ "$gt": params.fmdt_cnter, "$lt": params.todt_cnter } }, datasource_subqry  
          ]
      } },
      { $group: { _id: "$Name", "Trans_Count":{ $sum: 1} } },
    ]);
  } else if(params.risk_action != "" && params.risk_action == "risk_valuerisk") {
    var agent_data = params.collection.aggregate( [
      { $match: { 
          $and: [ 
            {"Value Risk Score": params.rsk_value },
            {"Transaction Date":{ "$gt": params.fmdt_cnter, "$lt": params.todt_cnter } }, datasource_subqry 
          ]
      } },
      { $group: { _id: "$Name", "Trans_Count":{ $sum: 1} } },
    ]);
  } else if(params.risk_action != "" && params.risk_action == "risk_customertype") {
    var agent_data = params.collection.aggregate( [
      { $match: { 
          $and: [ 
            {"Customer Type Risk Score": params.rsk_value },
            {"Transaction Date":{ "$gt": params.fmdt_cnter, "$lt": params.todt_cnter } }, datasource_subqry 
          ]
      } },
      { $group: { _id: "$Name", "Trans_Count":{ $sum: 1} } },
    ]);
  } else if(params.risk_action != "" && params.risk_action == "risk_purpose") {
    var agent_data = params.collection.aggregate( [
      { $match: { 
          $and: [ 
            {"Purpose Risk Score": params.rsk_value },
            {"Transaction Date":{ "$gt": params.fmdt_cnter, "$lt": params.todt_cnter } }, datasource_subqry 
          ]
      } },
      { $group: { _id: "$Name", "Trans_Count":{ $sum: 1} } },
    ]);
  } else if(params.risk_action != "" && params.risk_action == "risk_source") {
    var agent_data = params.collection.aggregate( [
      { $match: { 
          $and: [ 
            {"Source Risk score": params.rsk_value },
            {"Transaction Date":{ "$gt": params.fmdt_cnter, "$lt": params.todt_cnter } }, datasource_subqry 
          ]
      } },
      { $group: { _id: "$Name", "Trans_Count":{ $sum: 1} } },
    ]);
  } else if(params.risk_action != "" && params.risk_action == "risk_occupation") {
    var agent_data = params.collection.aggregate( [
      { $match: { 
          $and: [ 
            {"Occupation Risk Score": params.rsk_value },
            {"Transaction Date":{ "$gt": params.fmdt_cnter, "$lt": params.todt_cnter } }, datasource_subqry 
          ]
      } },
      { $group: { _id: "$Name", "Trans_Count":{ $sum: 1} } },
    ]);
  } else if(params.risk_action != "" && params.risk_action == "risk_modeofpayment") {
    var agent_data = params.collection.aggregate( [
      { $match: { 
          $and: [ 
            {"Mode of Payment Risk Score": params.rsk_value },
            {"Transaction Date":{ "$gt": params.fmdt_cnter, "$lt": params.todt_cnter } }, datasource_subqry 
          ]
      } },
      { $group: { _id: "$Name", "Trans_Count":{ $sum: 1} } },
    ]);
  } else if(params.risk_action != "" && params.risk_action == "risk_modeofdelivery") {
    var agent_data = params.collection.aggregate( [
      { $match: { 
          $and: [ 
            {"Mode of Delivery Risk Score": params.rsk_value },
            {"Transaction Date":{ "$gt": params.fmdt_cnter, "$lt": params.todt_cnter } }, datasource_subqry 
          ]
      } },
      { $group: { _id: "$Name", "Trans_Count":{ $sum: 1} } },
    ]);
  } else {
    var agent_data = params.collection.aggregate( [
      { $match: { 
          $and: [ 
            {"Total Risk Score": { $gte: params.gte, $lte: params.lte } },
            {"Transaction Date":{ "$gt": params.fmdt_cnter, "$lt": params.todt_cnter } }, datasource_subqry 
          ]
      } },
      { $group: { _id: "$Name", "Trans_Count":{ $sum: 1} } },
    ]);
  }
  
  agent_data.toArray(function(err, lowrisk_result) {
    
    if (err) {
      console.log(err);
    } else {
      var result_value = lowrisk_result;
      var lowrisk_data = {};
      var customer = [];
      var trans_count = [];
    
      if(result_value.length > 0) {
        each(result_value, function(lowdata, next){
          customer.push(lowdata["_id"]);
          trans_count.push(lowdata["Trans_Count"]);
          next();
        },function(){
          
            function common_sum(numbers) {
              return numbers.reduce(function(a,b) {
                return (a + b)
              });
            }
            if(params.ristktype == "low") {
              riskname = "Low Risk";
            } else if(params.ristktype == "medium") {
              riskname = "Medium Risk";
            } else if(params.ristktype == "high") {
              riskname = "High Risk";
            }
            if(customer.length > 0 && trans_count.length > 0) {
              lowrisk_data = {  
                  "risk":riskname,
                  "customer":customer.length,
                  "customer_percentage":"", 
                  "trans_count":common_sum(trans_count),
                  "trans_percentage":"",
                  "customer_arr":customer
              };
            }
            return callback(lowrisk_data);
        });
      } else {
        if(params.ristktype == "low") {
          riskname = "Low Risk";
        } else if(params.ristktype == "medium") {
          riskname = "Medium Risk";
        } else if(params.ristktype == "high") {
          riskname = "High Risk";
        }
        lowrisk_data = {  
          "risk":riskname,
          "customer":0.00,
          "customer_percentage":0.00, 
          "trans_count":0.00,
          "trans_percentage":0.00,
        };

        return callback(lowrisk_data);
      }
    }
  });
}

/**
 * USE : LIST LOADER POPUP
 * MODULE : RISKBASED ANALYSIS
 */
router.get('/customer_risklist_loader', chk_loginaccess, function(req, res) {

  var fromdate = req.query.fromdate || "";
  var todate = req.query.todate || "";
  var risk_action = req.query.riskaction || "";

  let ejs_extra_params = new Object();
  ejs_extra_params["pagename"] = "customer_risk";
  ejs_extra_params["domain_url"] = config.domain;
  ejs_extra_params["fromdate"] = fromdate;
  ejs_extra_params["todate"] = todate;
  ejs_extra_params["risk_action"] = risk_action;
  ejs_extra_params["customerview_action"] = "";

  // VARIABLE DEPENDENCY FOR ANOTHER CUSTMER DATA LIST PAGES
  ejs_extra_params["Fre_action"] = "";
  ejs_extra_params["Name"] = "";
  ejs_extra_params["trans_count"] = "";
  ejs_extra_params["fre_cust_name"] = "";
  ejs_extra_params["fre_trans_count"] = "";
    
  res.render("customerrisk_speclist_loader", ejs_extra_params);
});

/**
 * USE : LIST LOADER POPUP, WHEN CLICKING CUSTOMER COUNT GETTING RISK CUSTOMER TRANSACTION MORE DETAILS
 * MODULE : OVERALL CUSTOMER RISK
 * MAIN : http://192.168.1.187:3000/customerrisk_summary_report/overall_customer_risk
 * INSIDE : http://192.168.1.187:3000/customerrisk_summary_report/risk_customerdetails
 */
router.get('/risk_customerdetails', chk_loginaccess, function(req, res) {

  var fromdate = req.query.fromdate || "";
  var todate = req.query.todate || "";
  var risk_action = req.query.riskaction || "";

  var datasource_subqry = new Object();
  datasource_subqry["data_proprietor"] = new mongo.ObjectID(req.session.user.ses_userid);

  if(fromdate == '' || todate == '') {
    console.log({"status":"failed", "message":"fromdate and todate required!!!"})
    res.send([]);
  } else {
    var from_spliter = fromdate.split("-");
    var to_spliter = todate.split("-");
    var fmdt_cnter = new Date(from_spliter[2]+"-"+from_spliter[1]+"-"+from_spliter[0]);
    var todt_cnter = new Date(to_spliter[2]+"-"+to_spliter[1]+"-"+to_spliter[0]);
    var collection = dbUtil.get().collection(config.tbl_sanitized_data);

    if(risk_action == "RSK_NATIONALITY") {
      var high_params = { 
        "collection":collection, 
        "fieldname":"Nationality risk Score", 
        "rsk_value":3, 
        "ristktype":"high", 
        "fmdt_cnter":fmdt_cnter, 
        "todt_cnter":todt_cnter, 
        "risk_action":"risk_nationality", 
        "datasource_subqry":datasource_subqry
      };
    } else if(risk_action == "RSK_VALUE") {
      var high_params = {
        "collection":collection, 
        "fieldname":"Value Risk Score", 
        "rsk_value":3, 
        "ristktype":"high", 
        "fmdt_cnter":fmdt_cnter, 
        "todt_cnter":todt_cnter, 
        "risk_action":"risk_valuerisk", 
        "datasource_subqry":datasource_subqry
      };
    } else if(risk_action == "RSK_CUSTOMERTYPE") {
      var high_params = {
        "collection":collection, 
        "fieldname":"Customer Type Risk Score", 
        "rsk_value":3, 
        "ristktype":"high", 
        "fmdt_cnter":fmdt_cnter, 
        "todt_cnter":todt_cnter, 
        "risk_action":"risk_customertype", 
        "datasource_subqry":datasource_subqry
      };
    } else if(risk_action == "RSK_PURPOSE") {
      var high_params = {
        "collection":collection, "fieldname":"Purpose Risk Score", 
        "rsk_value":3, 
        "ristktype":"high", 
        "fmdt_cnter":fmdt_cnter, 
        "todt_cnter":todt_cnter, 
        "risk_action":"risk_purpose", 
        "datasource_subqry":datasource_subqry
      };
    } else if(risk_action == "RSK_SOURCE") {
      var high_params = {
        "collection":collection, 
        "fieldname":"Source Risk score", 
        "rsk_value":3, 
        "ristktype":"high", 
        "fmdt_cnter":fmdt_cnter, 
        "todt_cnter":todt_cnter, 
        "risk_action":"risk_source", 
        "datasource_subqry":datasource_subqry
      };
    } else if(risk_action == "RSK_OCCUPATION") {
      var high_params = {
        "collection":collection, 
        "fieldname":"Occupation Risk Score", 
        "rsk_value":3, 
        "ristktype":"high", 
        "fmdt_cnter":fmdt_cnter, 
        "todt_cnter":todt_cnter, 
        "risk_action":"risk_occupation", 
        "datasource_subqry":datasource_subqry
      };
    } else if(risk_action == "RSK_MODEOFPAYMENT") {
      var high_params = {
        "collection":collection, 
        "fieldname":"Mode of Payment Risk Score", 
        "rsk_value":3, 
        "ristktype":"high", 
        "fmdt_cnter":fmdt_cnter, 
        "todt_cnter":todt_cnter, 
        "risk_action":"risk_modeofpayment", 
        "datasource_subqry":datasource_subqry
      };
    } else if(risk_action == "RSK_MODEOFDELIVERY") {
      var high_params = {
        "collection":collection, 
        "fieldname":"Mode of Delivery Risk Score", 
        "rsk_value":3, 
        "ristktype":"high", 
        "fmdt_cnter":fmdt_cnter, 
        "todt_cnter":todt_cnter, 
        "risk_action":"risk_modeofdelivery", 
        "datasource_subqry":datasource_subqry
      };
    } else {
      var high_params = {
        "collection":collection, 
        "fieldname":"Total Risk Score", 
        "rsk_value":16, 
        "ristktype":"high", 
        "fmdt_cnter":fmdt_cnter, 
        "todt_cnter":todt_cnter, 
        "risk_action":"", 
        "datasource_subqry":datasource_subqry
      };
    }
    
    fn_risk_customerdetails(high_params, function(risk_data){
      if(risk_data.length > 0) {
        res.send(risk_data);
      } else {
        res.send({"status":"failed", "message":"there is don't have data for corresponding date range!!!"})
      }
    });
  }
});

/**
 * FUNCTION USE : LIST LOADER POPUP, WHEN CLICKING CUSTOMER COUNT GETTING RISK CUSTOMER TRANSACTION MORE DETAILS
 * MODULE : OVERALL CUSTOMER RISK
 * MAIN : http://192.168.1.187:3000/customerrisk_summary_report/overall_customer_risk
 * INSIDE : http://192.168.1.187:3000/customerrisk_summary_report/risk_customerdetails
 */
function fn_risk_customerdetails(params, callback){

  var fieldname = params.fieldname;
  var risk_action = params.risk_action;
  var datasource_subqry = params.datasource_subqry; 
  
  var matcher = {};

  if(risk_action == "")
    matcher[fieldname] = { $gte: params.rsk_value, $lte: 25 };
  else
    matcher[fieldname] = params.rsk_value;

  matcher["data_proprietor"] = datasource_subqry.data_proprietor;
  matcher["Transaction Date"] = { "$gt": params.fmdt_cnter, "$lt": params.todt_cnter };

  var agent_data = params.collection.aggregate( [
    { $match: { 
        $and: [ matcher ]
      } 
    }
  ]);

  agent_data.toArray(function(err, customerrisk_result) {
    if (err) {
      console.log(err);
    } else {
      var result_value = customerrisk_result;
      var customer_arr = [];
    
      if(result_value.length > 0) {
        each(result_value, function(result, next){
          var data_obj = new Array();
  
          data_obj.push(result["No"]);
          data_obj.push(result["Name"]);
          data_obj.push(result["Receipt No"]);
          data_obj.push(result["Nationality"]);
          data_obj.push(result["Occupation"]);
          data_obj.push(result["Source"]);
          data_obj.push(result["Purpose"]);
          data_obj.push(result["Mode of Payment"]);
          data_obj.push(result["Mode of Delivery"]);
          data_obj.push(result["Total Risk Score"]);
          data_obj.push(result["Outlet"]);
          data_obj.push(result["Exchange Amount"]);
          data_obj.push(result["BuySell"]);
          data_obj.push(result["Transaction Date"]);

          customer_arr.push(data_obj);
          next();
        },function(){
            return callback(customer_arr);
        });
      } else {
        return callback(customer_arr);
      }
    }
  });
}


/**
 * USE : LIST LOADER POPUP FOR ALL RISK TYPES WHEN CLICKING INFO ICON
 * MODULE : OVERALL CUSTOMER RISK
 * MAIN : http://192.168.1.187:3000/customerrisk_summary_report/overall_customer_risk
 * INSIDE : http://192.168.1.187:3000/customerrisk_summary_report/customer_riskbased_loader
 */
router.get('/customer_riskbased_loader', chk_loginaccess, function(req, res) {

  var fromdate = req.query.fromdate || "";
  var todate = req.query.todate || "";
  var risk_action = req.query.riskaction || "";

  res.render("overall_riskbased_view", {"pagename":"customer_risk","domain_url":config.domain, "fromdate":fromdate, "todate":todate, "risk_action":risk_action});
});


module.exports = router;
