var config = require('../config/database');
var dbUtil = require('../config/dbUtil');
var express = require('express');
var router = express.Router();
var each = require('async-each');
var auth = require('../middlewares/auth'), chk_loginaccess = auth.chk_loginaccess;
var moment = require("moment");
var common_functions = require('../middlewares/common_functions');
var MODCONF = require('../config/module_config');
const PRMS = MODCONF.PRMS;
const MDUL = MODCONF.MDUL;
var mongo = require('mongodb');

/**
 * USE : ROOT LOADER FOR FREQUENT TRANSACTION
 * MODULE : TRANSACTION RISK ANALYSIS
 */
router.get('/', function(req, res, next) {
  
  var fromdate = req.query.fromdate || "";
  var todate = req.query.todate || "";
  var trans_count = req.query.trans_count || "";
  var action = req.query.action || "";

  var datasource_subqry = new Object();
  datasource_subqry["data_proprietor"] = new mongo.ObjectID(req.session.user.ses_userid);

  if(fromdate == '' || todate == '') {
    console.log({"status":"failed", "message":"fromdate and todate required!!!"});
    res.send([]);
  } else {

    var from_spliter = fromdate.split("-");
    var to_spliter = todate.split("-");
    var fmdt_cnter = new Date(from_spliter[2]+"-"+from_spliter[1]+"-"+from_spliter[0]);
    var todt_cnter = new Date(to_spliter[2]+"-"+to_spliter[1]+"-"+to_spliter[0]);

    var final_frequenttrans_arr = Array();

    var params = {"fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "trans_count":trans_count, "action":"", "datasource_subqry":datasource_subqry};
          
    fn_frequent_transaction(final_frequenttrans_arr, params, function(data){

      if(final_frequenttrans_arr.length > 0) {
        res.send(final_frequenttrans_arr);
      } else {
        res.send({"status":"failed", "message":"there is don't have data for corresponding date range!!!"})
      }
    });    
  }
});  

/**
 * FUNCTION USE : FREQUENT TRANSACTION CUSTOMERS, TRANSACTION COUNT, CORRESPONDIND DATA WILL RETURN  
 * MODULE : TRANSACTION RISK ANALYSIS
 */
function fn_frequent_transaction(final_frequenttrans_arr, params, callback){
    
    var datasource_subqry = params.datasource_subqry;
    var fmdt_cnter = params.fmdt_cnter;
    var todt_cnter = params.todt_cnter;
    var trans_count = params.trans_count;
    var action = params.action;

    var collection = dbUtil.get().collection(config.tbl_sanitized_data);

    var finder = {};
    finder["Transaction Date"] = { $gt:fmdt_cnter, $lt:todt_cnter };

    var matcher = {};
    matcher["total"] = {'$gt': parseInt(trans_count)};
    var transcounter_qry = collection.aggregate([
        {
          $match:{
            $and: [ { "Transaction Date": { $gt: fmdt_cnter, $lt: todt_cnter } }, datasource_subqry ]
          }
        }, {
          $group:{
            _id:{
              "name":'$Name',
              "month": {$month: "$Transaction Date"},
              "year": {$year: "$Transaction Date"}
            }, 
            total:{$sum:1},
            Transaction_Date:{$push:'$Transaction Date'}
          }
        }, {
           $match : matcher  
        }, {
           $project : {"name" : "$_id.name", "month" : "$_id.month", "year" : "$_id.year", "total" : "$total"}
        }]);

    transcounter_qry.toArray(function(err, transcounter_result) {

        if(transcounter_result.length > 0) {
          each(transcounter_result, function(result, next){

            var data_obj = new Array();
            var month = result.month;
            month = moment(month, 'MM').format('MMM');  
            var year = result.year;
            var monthyear = month+"-"+year;

            data_obj.push(monthyear);
            data_obj.push(result.name);
            data_obj.push(result.total);
            
            final_frequenttrans_arr.push(data_obj);
            next();
          }, function(){
            return callback(final_frequenttrans_arr);
          });
        } else {
          return callback(final_frequenttrans_arr);
        }
    });
}

/**
 * USE : ROOT LOADER FOR FREQUENT TRANSACTION
 * MODULE : TRANSACTION RISK ANALYSIS
 */
router.get('/monthly_multi_currency_transactions', function(req, res, next) {
  
  var fromdate = req.query.fromdate || "";
  var todate = req.query.todate || "";
  var trans_count = req.query.trans_count || "";
  var action = req.query.action || "";

  var datasource_subqry = new Object();
  datasource_subqry["data_proprietor"] = new mongo.ObjectID(req.session.user.ses_userid);
  
  if(fromdate == '' || todate == '') {
    console.log({"status":"failed", "message":"fromdate and todate required!!!"});
    res.send([]);
  } else {

    var from_spliter = fromdate.split("-");
    var to_spliter = todate.split("-");
    var fmdt_cnter = new Date(from_spliter[2]+"-"+from_spliter[1]+"-"+from_spliter[0]);
    var todt_cnter = new Date(to_spliter[2]+"-"+to_spliter[1]+"-"+to_spliter[0]);

    var final_frequenttrans_arr = Array();

    var params = {"fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "trans_count":trans_count, "action":"", "datasource_subqry":datasource_subqry};
          
    fn_multicurrency_transaction(final_frequenttrans_arr, params, function(data){

      if(final_frequenttrans_arr.length > 0) {
        res.send(final_frequenttrans_arr);
      } else {
        res.send({"status":"failed", "message":"there is don't have data for corresponding date range!!!"})
      }
    });    
  }
});  

/**
 * FUNCTION USE : MULTIPLE CURRENCEY FREQUENT TRANSACTION CUSTOMERS, TRANSACTION COUNT, CORRESPONDIND DATA WILL RETURN  
 * MODULE : TRANSACTION RISK ANALYSIS
 */
function fn_multicurrency_transaction(final_frequenttrans_arr, params, callback){
    
  var datasource_subqry = params.datasource_subqry;
  var fmdt_cnter = params.fmdt_cnter;
  var todt_cnter = params.todt_cnter;
  var trans_count = params.trans_count;
  var action = params.action;

  var collection = dbUtil.get().collection(config.tbl_sanitized_data);

  var finder = new Object();
  finder["Transaction Date"] = { $gt:fmdt_cnter, $lt:todt_cnter };

  var matcher = new Object();
  matcher["total"] = {'$gt': parseInt(trans_count)};

  var transcounter_qry = collection.aggregate([
    {
      $match:{
        $and: [ finder, datasource_subqry ]
      }
    }, {
        $group:{
          _id:{
            "name":'$Name',
            "month": {$month: "$Transaction Date"},
            "year": {$year: "$Transaction Date"}
          }, 
          total:{$sum:1},
          Currency_code:{$addToSet:'$Currency Code'}
        }
    }, {
        $match : matcher  
    }, {
        $project : {"name" : "$_id.name", "month" : "$_id.month", "year" : "$_id.year", "total" : "$total", "Currency_code":"$Currency_code"}
  }]);

  transcounter_qry.toArray(function(err, transcounter_result) {

    if(transcounter_result.length > 0) {
      each(transcounter_result, function(result, next){
        

        var data_obj = new Array();
        var month = result.month;
        month = moment(month, 'MM').format('MMM');  
        var year = result.year;
        var monthyear = month+"-"+year;

        data_obj.push(monthyear);
        data_obj.push(result.name);
        data_obj.push(result.total);
        data_obj.push(result.Currency_code.length);
          
        final_frequenttrans_arr.push(data_obj);
        next();
      }, function(){
        console.log("under sucess callback")
        return callback(final_frequenttrans_arr);
      });
    } else {
      console.log("empty call back");
      return callback(final_frequenttrans_arr);
    }
  });
}

module.exports = router;
