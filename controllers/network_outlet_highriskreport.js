var config = require('../config/database');
var express = require('express');
var router = express.Router();
var each = require('async-each');
var report = require('../models/network_outlet_highriskreport')
var auth = require('../middlewares/auth'), chk_loginaccess = auth.chk_loginaccess;
var mongo = require('mongodb');

/**
 * USE : ROOT LIST LOADER FOR HIGH RISK CUSTOMERS COUNT BASED ON OUTLET
 * MODULE : HIGH RISK OUTLET
 * MAIN : http://192.168.1.187:3000/outlet_highriskreport/outlet_highrisk
 */
router.get('/', chk_loginaccess, function(req, res, next) {
  
  var fromdate = req.query.fromdate || "";
  var todate = req.query.todate || "";
  var action = req.query.action;
  
  var datasource_subqry = new Object();
  datasource_subqry["data_proprietor"] = new mongo.ObjectID(req.session.user.ses_userid);
  
  if(fromdate == '' || todate == '') {
    console.log({"status":"failed", "message":"fromdate and todate required!!!"});
    res.send([]);
  } else {
    var from_spliter = fromdate.split("-");
    var to_spliter = todate.split("-");
    
    var fmdt_cnter = new Date(from_spliter[2]+"-"+from_spliter[1]+"-"+from_spliter[0]);
    var todt_cnter = new Date(to_spliter[2]+"-"+to_spliter[1]+"-"+to_spliter[0]);

    var params = {};

    if(action == "rmk_ten") {
      params = {
        fmdt_cnter:fmdt_cnter, 
        todt_cnter:todt_cnter, 
        datasource_subqry:datasource_subqry,
        action:"10000"
      };
    } else if(action == "rmk_twenty") {
      params = {
        fmdt_cnter:fmdt_cnter, 
        todt_cnter:todt_cnter, 
        datasource_subqry:datasource_subqry,
        action:"20000"
      };
    } else if(action == "rmk_thirty") {
      params = {
        fmdt_cnter:fmdt_cnter, 
        todt_cnter:todt_cnter, 
        datasource_subqry:datasource_subqry,
        action:"30000"
      };
    } else if(action == "rmk_fourty") {
      params = {
        fmdt_cnter:fmdt_cnter, 
        todt_cnter:todt_cnter, 
        datasource_subqry:datasource_subqry,
        action:"40000"
      };
    } else if(action == "rmk_fifty") {
      params = {
        fmdt_cnter:fmdt_cnter, 
        todt_cnter:todt_cnter, 
        datasource_subqry:datasource_subqry,
        action:"50000"
      };
    } else if(action == "rmk_fiftyabove") {
      params = {
        fmdt_cnter:fmdt_cnter, 
        todt_cnter:todt_cnter, 
        datasource_subqry:datasource_subqry,
        action:"60000"
      };
    } 

    report.networkhighRiskCustomer(params, function(err, result){
      if(err){
        console.log(err);
        res.send(err);
      }else{
        
        var data_obj = {};
        var ak = 0; 
        if(result.length > 0){

          var final_data = new Array();
          each(result, function(result_value, next){

              var temp_arr = new Array();
              temp_arr.push(result_value["_id"]);
              temp_arr.push(result_value["HRC"]);
              temp_arr.push(result_value["transArrSize"]);
              final_data.push(temp_arr);
              next();
          }, function() {
              res.send(final_data);
          });
        } else {
          res.send([]);
        }
      }
    });
  }
});


/**
 * USE : ROOT LOADER POPUP VIEW FOR HIGH RISK OUTLET BASED CUSTOMERS TRANSACTION DETAILS
 * MODULE : HIGH RISK OUTLET
 */
router.get('/customer_risklist_loader', chk_loginaccess, function(req, res) {
  
  var fromdate = req.query.fromdate || "";
  var todate = req.query.todate || "";
  var outlet_name = req.query.outlet_name || "";
  var action = req.query.action;

  let render_params = new Object();
  render_params["pagename"] = "customer_risk";
  render_params["domain_url"] = config.domain;
  render_params["fromdate"] = fromdate;
  render_params["todate"] = todate;
  render_params["risk_action"] = outlet_name;
  render_params["customerview_action"] = "HR_Outlet_Customers";
  render_params["action"] = action;

  res.render("network_customerrisk_speclist_loader", render_params);
});

/**
 * USE : POPUP LIST LOADER FOR HIGH RISK OUTLET BASED CUSTOMERS TRANSACTION DETAILS
 * MODULE : HIGH RISK OUTLET
 */
router.get('/getcustomersbasedonoutlets', chk_loginaccess, function(req, res, next) {

  var fromdate = req.query.fromdate || "";
  var todate = req.query.todate || "";
  var outlet_name = req.query.outlet_name || "";
  var action=req.query.action;

  var datasource_subqry = new Object();
  datasource_subqry["data_proprietor"] = new mongo.ObjectID(req.session.user.ses_userid);
    
  if(fromdate == '' || todate == '' || outlet_name == "") {
    res.send({"status":"failed", "message":"fromdate, todate and outlet name required!!!"});
  } else {
    var from_spliter = fromdate.split("-");
    var to_spliter = todate.split("-");
    
    var fmdt_cnter = new Date(from_spliter[2]+"-"+from_spliter[1]+"-"+from_spliter[0]);
    var todt_cnter = new Date(to_spliter[2]+"-"+to_spliter[1]+"-"+to_spliter[0]);

    var params;

    if(action == "rmk_ten") {
      params = {
        fmdt_cnter:fmdt_cnter, 
        todt_cnter:todt_cnter, 
        outlet_name:outlet_name, 
        datasource_subqry:datasource_subqry,
        action:"10000"
      };
    } else if(action == "rmk_twenty") {
      params = {
        fmdt_cnter:fmdt_cnter, 
        todt_cnter:todt_cnter, 
        outlet_name:outlet_name, 
        datasource_subqry:datasource_subqry,
        action:"20000"
      };
    } else if(action == "rmk_thirty") {
      params = {
        fmdt_cnter:fmdt_cnter, 
        todt_cnter:todt_cnter, 
        outlet_name:outlet_name, 
        datasource_subqry:datasource_subqry,
        action:"30000"
      };
    } else if(action == "rmk_fourty") {
      params = {
        fmdt_cnter:fmdt_cnter, 
        todt_cnter:todt_cnter, 
        outlet_name:outlet_name, 
        datasource_subqry:datasource_subqry,
        action:"40000"
      };
    } else if(action == "rmk_fifty") {
      params = {
        fmdt_cnter:fmdt_cnter, 
        todt_cnter:todt_cnter, 
        outlet_name:outlet_name, 
        datasource_subqry:datasource_subqry,
        action:"50000"
      };
    } else if(action == "rmk_fiftyabove") {
      params = {
        fmdt_cnter:fmdt_cnter, 
        todt_cnter:todt_cnter, 
        outlet_name:outlet_name, 
        datasource_subqry:datasource_subqry,
        action:"60000"
      };
    } 
    
    report.networkhighriskcustomerbasedonoutlet_list(params, function(err, result){
      if(err){
        console.log(err)
        res.send(err);
      }else{

        var final_data = [];
        var cnter = 1;

        each(result, function(result_value, next){

          var data_obj = [];
          data_obj.push(result_value["Name"]);
          data_obj.push(result_value["Exchange Amount"]);
          data_obj.push(result_value["BuySell"]);
          data_obj.push(result_value["Transaction Date"]);
          data_obj.push(result_value["Source"]);
          data_obj.push(result_value["Purpose"]);

          final_data.push(data_obj);
          cnter++;

          next();

        }, function(){

          res.send(final_data);

        });

      }
    });
  }

});


module.exports = router;