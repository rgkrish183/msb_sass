var config = require('../config/database');
var express = require('express');
var router = express.Router();
var each = require('async-each');
var report = require('../models/outlet_highriskreport')
var auth = require('../middlewares/auth'), chk_loginaccess = auth.chk_loginaccess;
var mongo = require('mongodb');

/**
 * USE : ROOT LIST LOADER FOR HIGH RISK CUSTOMERS COUNT BASED ON OUTLET
 * MODULE : HIGH RISK OUTLET
 * MAIN : http://192.168.1.187:3000/outlet_highriskreport/outlet_highrisk
 */
router.get('/', chk_loginaccess, function(req, res, next) {
  
  var fromdate = req.query.fromdate || "";
  var todate = req.query.todate || "";

  var datasource_subqry = new Object();
  datasource_subqry["data_proprietor"] = new mongo.ObjectID(req.session.user.ses_userid);
  
  if(fromdate == '' || todate == '') {
    console.log({"status":"failed", "message":"fromdate and todate required!!!"});
    res.send([]);
  } else {
    var from_spliter = fromdate.split("-");
    var to_spliter = todate.split("-");
    
    var fmdt_cnter = new Date(from_spliter[2]+"-"+from_spliter[1]+"-"+from_spliter[0]);
    var todt_cnter = new Date(to_spliter[2]+"-"+to_spliter[1]+"-"+to_spliter[0]);

    var params = {fmdt_cnter:fmdt_cnter, todt_cnter:todt_cnter, datasource_subqry:datasource_subqry};

    report.highRiskCustomer(params, function(err, result){
      if(err){
        console.log(err);
        res.send(err);
      }else{
        
        if(result.length > 0){

          var final_data = new Array();
          each(result, function(result_value, next){

              console.log(result_value._id);
              var temp_arr = new Array();

              temp_arr.push(result_value["_id"]);
              temp_arr.push(result_value["HRC"]);
              temp_arr.push(result_value["Transactions"]);

              final_data.push(temp_arr);
              next();
          }, function() {
              console.log(final_data);
              res.send(final_data);
          });
        } else {
          res.send([]);
        }
      }
    });
  }

});


/**
 * USE : ROOT LOADER POPUP VIEW FOR HIGH RISK OUTLET BASED CUSTOMERS TRANSACTION DETAILS
 * MODULE : HIGH RISK OUTLET
 */
router.get('/customer_risklist_loader', chk_loginaccess, function(req, res) {
  
  var fromdate = req.query.fromdate || "";
  var todate = req.query.todate || "";
  var outlet_name = req.query.outlet_name || "";

  let ejs_extra_params = new Object();
  ejs_extra_params["pagename"] = "customer_risk";
  ejs_extra_params["domain_url"] = config.domain;
  ejs_extra_params["fromdate"] = fromdate;
  ejs_extra_params["todate"] = todate;
  ejs_extra_params["risk_action"] = outlet_name;
  ejs_extra_params["customerview_action"] = "HR_Outlet_Customers";
  ejs_extra_params["Fre_action"] = "";

  // VARIABLE DEPENDENCY FOR ANOTHER CUSTMER DATA LIST PAGES
  ejs_extra_params["Fre_action"] = "";
  ejs_extra_params["Name"] = "";
  ejs_extra_params["trans_count"] = "";
  ejs_extra_params["fre_cust_name"] = "";
  ejs_extra_params["fre_trans_count"] = "";

  res.render("customerrisk_speclist_loader", ejs_extra_params);
});

/**
 * USE : POPUP LIST LOADER FOR HIGH RISK OUTLET BASED CUSTOMERS TRANSACTION DETAILS
 * MODULE : HIGH RISK OUTLET
 */
router.get('/getcustomersbasedonoutlets', chk_loginaccess, function(req, res, next) {

  var fromdate = req.query.fromdate || "";
  var todate = req.query.todate || "";
  var outlet_name = req.query.outlet_name || "";

  var datasource_subqry = new Object();
  datasource_subqry["data_proprietor"] = new mongo.ObjectID(req.session.user.ses_userid);
    
  if(fromdate == '' || todate == '' || outlet_name == "") {
    res.send({"status":"failed", "message":"fromdate, todate and outlet name required!!!"});
  } else {

    var from_spliter = fromdate.split("-");
    var to_spliter = todate.split("-");
    
    var fmdt_cnter = new Date(from_spliter[2]+"-"+from_spliter[1]+"-"+from_spliter[0]);
    var todt_cnter = new Date(to_spliter[2]+"-"+to_spliter[1]+"-"+to_spliter[0]);

    var params = {fmdt_cnter:fmdt_cnter, todt_cnter:todt_cnter, outlet_name:outlet_name, datasource_subqry:datasource_subqry};
    
    report.highriskcustomerbasedonoutlet_list(params, function(err, result){
      if(err){
        console.log(err)
        res.send(err);
      }else{

        var final_data = [];
        var cnter = 1;

        each(result, function(result_value, next){

          var data_obj = [];
          data_obj.push(result_value["Name"]);
          data_obj.push(result_value["Exchange Amount"]);
          data_obj.push(result_value["BuySell"]);
          data_obj.push(result_value["Transaction Date"]);
          data_obj.push(result_value["Source"]);
          data_obj.push(result_value["Purpose"]);

          final_data.push(data_obj);
          cnter++;
          next();
          
        }, function(){
          res.send(final_data);
        });

      }
    });
  }

});


module.exports = router;