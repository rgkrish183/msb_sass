var express = require('express');
var router = express.Router();
var config = require('../config/database');
var dbUtil = require('../config/dbUtil');
var auth = require('../middlewares/auth'), chk_loginaccess = auth.chk_loginaccess;
var session = require('express-session');
var each = require('async-each');
var mongo = require('mongodb');
var moment = require("moment");
var common_functions = require('../middlewares/common_functions');
var _ = require('lodash');

/**
 * USE : ROOT LOADER
 * MODULE : REPORTING TREE
 */
router.get('/', chk_loginaccess, async (req, res, next) => {

    // URL PARAMS
    var urlquery_request = new Object();
    urlquery_request["fromdate"] = req.query.fromdate;
    urlquery_request["todate"] = req.query.todate;

    // RENDER PAGE PARAMS 
    let render_params_obj = new Object();
    render_params_obj["pagename"] = "reportingTree";
    render_params_obj["domain_url"] = config.domain;
    render_params_obj["ses_user"] = req.session.user;
    render_params_obj["urlquery_request"] = urlquery_request;
    
    if(req.session.user.ses_usertype == config.usertype_access.admin || req.session.user.ses_usertype == config.usertype_access.system_admin || req.session.user.ses_usertype == config.usertype_access.users)
        res.render('reporting_tree', render_params_obj);
    else 
        res.render('errors/errors',{"domain_url":config.domain, "error_code":401, "error_mainmsg":"Module Permission Denied !"});
});


/**
 * USE : ROOT LOADER
 * MODULE : REPORTING TREE
 */
router.get('/tree_reporting', chk_loginaccess, async (req, res, next) => {

    if(req.session.user.ses_usertype == config.usertype_access.admin || req.session.user.ses_usertype == config.usertype_access.system_admin || req.session.user.ses_usertype == config.usertype_access.users){

        let tree_data = await dbUtil.get().collection(config.tbl_users).aggregate( [ { 
                $match: { $or: [{ "u_reporting_hierarchy" : new mongo.ObjectID(req.session.user.ses_userid) }, { "_id" : new mongo.ObjectID(req.session.user.ses_userid) }] }
            }, {
                $graphLookup: { from: config.tbl_users, startWith: "$_id", connectFromField: "u_reporting_to", connectToField: "u_reporting_to", as: "reportingHierarchy" }
            }, {
                $sort : {"u_usertype": 1} 
            }
        ] ).toArray();


        if(tree_data.length > 0) {

            let reporting_tree_data_arr = new Array();

            each(tree_data, async (users_item, next_mover) => {

                let userid = (users_item._id).toString();
                var loginer_name = users_item.u_firstname+" "+users_item.u_lastname;

                if(userid == req.session.user.ses_userid) {
                    
                    let current_temp_obj_main = new Array();

                    var loginer_roles = await dbUtil.get().collection(config.tbl_roles).findOne({ _id: new mongo.ObjectID(users_item.u_role)});

                    current_temp_obj_main.push({v:loginer_name, f:loginer_name+'<div style="color:blue;font-size: 12px;">'+loginer_roles.label+'</div>'});
                    current_temp_obj_main.push("");
                    current_temp_obj_main.push("");

                    reporting_tree_data_arr.push(current_temp_obj_main);
                }
                
                if((users_item.reportingHierarchy).length > 0) {

                    each(users_item.reportingHierarchy, async (childnode_users_item, childnode_next_mover) => {

                        let router_temp_obj = new Array();

                        let router_name = childnode_users_item.u_firstname+" "+childnode_users_item.u_lastname;

                        var router_roles = await dbUtil.get().collection(config.tbl_roles).findOne({ _id: new mongo.ObjectID(childnode_users_item.u_role)});

                        router_temp_obj.push({v:router_name, f:router_name+'<div style="color:blue;font-size: 12px;">'+router_roles.label+'</div>'});
                        router_temp_obj.push(loginer_name);
                        router_temp_obj.push("");
                                
                        reporting_tree_data_arr.push(router_temp_obj);

                        childnode_next_mover();

                    }, function() {
                        next_mover();  
                    });
                } else {
                    next_mover();  
                }
            }, function() {
                res.status(200).send(reporting_tree_data_arr);        
            });
        } else {
            res.status(304).send(reporting_tree_data_arr);        
        }

    } else {
        res.render('errors/errors',{"domain_url":config.domain, "error_code":401, "error_mainmsg":"Module Permission Denied !"});
    }
});

module.exports = router;
