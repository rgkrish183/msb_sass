var config = require('../config/database');
var dbUtil = require('../config/dbUtil');
var express = require('express');
var router = express.Router();
var each = require('async-each');
var auth = require('../middlewares/auth'), chk_loginaccess = auth.chk_loginaccess;
var mongo = require('mongodb');


/**
 * USE : ROOT LOADER DATA API
 * MODULE : OVERVIEW DASHBOARD
 * SUB-MODULE : RM RANGE 
 */
router.get('/', chk_loginaccess, function(req, res, next) {
  
  var fromdate = req.query.fromdate || "";
  var todate = req.query.todate || "";

  var datasource_subqry = new Object();
  datasource_subqry["data_proprietor"] = new mongo.ObjectID(req.session.user.ses_userid);
  
  if(fromdate == '' || todate == '') {
    console.log({"status":"failed", "message":"fromdate and todate required!!!"})
    res.send([]);
  } else {
    var from_spliter = fromdate.split("-");
    var to_spliter = todate.split("-");
    var fmdt_cnter = new Date(from_spliter[2]+"-"+from_spliter[1]+"-"+from_spliter[0]);
    var todt_cnter = new Date(to_spliter[2]+"-"+to_spliter[1]+"-"+to_spliter[0]);
    var below3_params = {"fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "action":"below3", "datasource_subqry":datasource_subqry};
    var final_sanitized = Array();

    gt_msbsanitized(final_sanitized, below3_params, function(below3_data){
      var above3to10_params = {"fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "action":"above3to10", "datasource_subqry":datasource_subqry};
      
      gt_msbsanitized(final_sanitized, above3to10_params, function(above3to10_data){
        var above10to50_params = {"fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "action":"above10to50", "datasource_subqry":datasource_subqry};
        
        gt_msbsanitized(final_sanitized, above10to50_params, function(above10to50_data){
          var above50_params = {"fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "action":"above50", "datasource_subqry":datasource_subqry};
          
          gt_msbsanitized(final_sanitized, above50_params, function(above50_data){
  
            if(final_sanitized.length > 0) {
              var transcnt_arr = Array();
              var exc_amt_arr = Array();
              
              each(final_sanitized, function(result, next){

                transcnt_arr.push(result.trans_count);
                exc_amt_arr.push(result.exchange_amt);
                
                next();
              }, function(){

                function common_sum(numbers) {
                  return numbers.reduce(function(a,b) {
                    return (a + b)
                  });
                }
                var total_trans = parseFloat(common_sum(transcnt_arr));
                var total_excamt = parseFloat(common_sum(exc_amt_arr));

                each(final_sanitized, function(result, next){
                  
                  result.trans_count_percent = parseFloat((result.trans_count/total_trans)*100).toFixed(2);
                  result.exchange_amt_percent = parseFloat((result.exchange_amt/total_excamt)*100).toFixed(2);
                  
                  next();
                }, function(){
                  res.send(final_sanitized);
                });
                  
              });  
            } else {
              res.send({"status":"failed", "message":"there is don't have data for corresponding date range!!!"});
            }
          });
        });
      });
    });
  }
});  

/**
 * USE : ROOT LOADER DATA API
 * MODULE : OVERVIEW DASHBOARD
 * SUB-MODULE : RM RANGE MORE THAN 30K(DEFAULT VALUE)- WHICH GIVEN USER SELECT AMOUNT RANGE 
 */
router.get('/rmrange_morethan30k', chk_loginaccess, function(req, res, next) {
  
  var fromdate = req.query.fromdate || "";
  var todate = req.query.todate || "";
  var action = req.query.action || "";

  var datasource_subqry = new Object();
  datasource_subqry["data_proprietor"] = new mongo.ObjectID(req.session.user.ses_userid);  
  
  if(fromdate == '' || todate == '') {
    console.log({"status":"failed", "message":"fromdate and todate required!!!"});
    res.send([]);
  } else {
    var from_spliter = fromdate.split("-");
    var to_spliter = todate.split("-");
    
    var fmdt_cnter = new Date(from_spliter[2]+"-"+from_spliter[1]+"-"+from_spliter[0]);
    var todt_cnter = new Date(to_spliter[2]+"-"+to_spliter[1]+"-"+to_spliter[0]);

    var final_sanitized = Array();

    if(action == "rmk_ten"){
      var params = {"fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "action":"10K", "datasource_subqry":datasource_subqry};
    } else if(action == "rmk_twenty"){
      var params = {"fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "action":"20K", "datasource_subqry":datasource_subqry};
    } else if(action == "rmk_thirty"){
      var params = {"fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "action":"30K", "datasource_subqry":datasource_subqry};
    } else if(action == "rmk_fourty"){
      var params = {"fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "action":"40K", "datasource_subqry":datasource_subqry};
    } else if(action == "rmk_fifty"){
      var params = {"fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "action":"50K", "datasource_subqry":datasource_subqry};
    } else if(action == "rmk_fiftyabove"){
      var params = {"fmdt_cnter":fmdt_cnter, "todt_cnter":todt_cnter, "action":"60tomore", "datasource_subqry":datasource_subqry};
    }
          
    custom_rmrages(final_sanitized, params, function(above50_data){

      if(final_sanitized.length > 0) {
        res.send(final_sanitized);
      } else {
        res.send({"status":"failed", "message":"there is don't have data for corresponding date range!!!"})
      }

    });
    
  }
  
});  


/**
 * USE : Update Action Taken
 * MODULE : OVERVIEW DASHBOARD
 * SUB-MODULE : RM RANGE MORE THAN 30K(DEFAULT VALUE)- WHICH GIVEN USER SELECT AMOUNT RANGE 
 */
router.put('/updateActionTaken', chk_loginaccess, function(req, res, next) {
  console.log("Printing the value of action id:"+req.query.actionId+" "+req.query.actionValue);
  var actionId = req.query.actionId;
  var actionValue = req.query.actionValue;

  var collection = dbUtil.get().collection(config.tbl_sanitized_data);
  var mongo = require('mongodb');
  var sanitized_datasource_id = new mongo.ObjectID(actionId);
    
    collection.findOneAndUpdate( 
      { "_id": sanitized_datasource_id }, 
      { $set:  { "Action Taken": actionValue } },  
      { upsert: true }, 
      (err, result) => {
        if (err) return res.send(err)
        res.send(result)
    })
});


/**
 * USE : Update Remarks
 * MODULE : OVERVIEW DASHBOARD
 * SUB-MODULE : RM RANGE MORE THAN 30K(DEFAULT VALUE)- WHICH GIVEN USER SELECT AMOUNT RANGE 
 */
router.put('/updateRemarks', chk_loginaccess, function(req, res, next) {
  console.log("Printing the value of remarks:"+req.query.actionId+" "+req.query.actionValue);
  var remarkId = req.query.remarkId;
  var remarkValue = req.query.remarkValue;

  var collection = dbUtil.get().collection(config.tbl_sanitized_data);
  var mongo = require('mongodb');
  var sanitized_datasource_id = new mongo.ObjectID(remarkId);
    
    collection.findOneAndUpdate(
      { "_id": sanitized_datasource_id }, 
      { $set:  { "Remarks": remarkValue } }, 
      { upsert: true }, 
      (err, result) => {
        if (err) return res.send(err)
        res.send(result)
    })
});


 /**
 * FUNCTION USE : ROOT LOADER FOR RM RANGE
 * MODULE : OVERVIEW DASHBOARD 
 * SUB-MODULE : RM RANGE
 */
function gt_msbsanitized(data, params, callback) {
  var exc_amt;
  var trans_range;
  var datasource_subqry = params.datasource_subqry;
  
  if(params.action == 'below3') {
    exc_amt = {$lt: 3000};
    trans_range = "Less than RM 3,000";
  } else if(params.action == 'above3to10') {
    exc_amt = {$gte: 3000, $lt:10000};
    trans_range = "RM 3K Up to < RM 10K";
  } else if(params.action == 'above10to50') {
    exc_amt = {$gte: 10000, $lt:50000};
    trans_range = "RM 10K Up to < RM 50K";
  } else if(params.action == 'above50') {
    exc_amt = {$gte: 50000};
    trans_range = "RM 50K and Above";
  }

  var collection = dbUtil.get().collection(config.tbl_sanitized_data);
    
  collection.aggregate([
    { 
      $match: { $and: [ 
              {"Exchange Amount": exc_amt }, 
              {"Transaction Date":{ $gt:params.fmdt_cnter, $lt:params.todt_cnter} },datasource_subqry              
        ] } 
    },
    { $group: { _id: "Exchange Amount", "Exchange_Amount": { $sum: "$Exchange Amount" }, "Trans_Count":{ $sum: 1} } },

  ]).toArray(function(err, result_value) {
    
    if(result_value.length > 0) {
      each(result_value, function(result, next){

        var data_obj = {};
        data_obj["trans_range"] = trans_range;
        data_obj["trans_count"] = result["Trans_Count"];
        data_obj["trans_count_percent"] = "";
        data_obj["exchange_amt"] = result["Exchange_Amount"];
        data_obj["exchange_amt_percent"] = "";
        
        data.push(data_obj);

        next();
      }, function(){
        return callback(data);
      });
    } else {
      return callback(data);
    }
  });
}

/**
 * FUNCTION USE : ROOT LOADER DATA API
 * MODULE : OVERVIEW DASHBOARD
 * SUB-MODULE : RM RANGE MORE THAN 30K(DEFAULT VALUE)- WHICH GIVEN USER SELECT AMOUNT RANGE
 */
function custom_rmrages(data, params, callback) {
  var exc_amt;
  var datasource_subqry = params.datasource_subqry;
  
  if(params.action == '10K') {
    exc_amt = {$gt: 10000};
  } else if(params.action == '20K') {
    exc_amt = {$gt: 20000};
  } else if(params.action == '30K') {
    exc_amt = {$gt: 30000};
  } else if(params.action == '40K') {
    exc_amt = {$gt: 40000};
  } else if(params.action == '50K') {
    exc_amt = {$gt: 50000};
  } else if(params.action == '60tomore') {
    exc_amt = {$gt: 60000};
  }

  var collection = dbUtil.get().collection(config.tbl_sanitized_data);
    
  collection.aggregate([
    { 
      $match: { $and: [ 
              {"Exchange Amount": exc_amt }, 
              {"Transaction Date":{ $gt:params.fmdt_cnter, $lt:params.todt_cnter} },datasource_subqry              
        ] } 
    }
  ]).toArray(function(err, result_value) {
    
    if(result_value.length > 0) {
      each(result_value, function(result, next){

        var data_obj = new Array();
        data_obj.push(result["_id"]);
        data_obj.push(result["Name"]);
        data_obj.push(result["Exchange Amount"]);
        data_obj.push(result["BuySell"]);
        data_obj.push(result["Transaction Date"]);
        data_obj.push(result["Source"]);
        data_obj.push(result["Purpose"]);
        data_obj.push(result["Action Taken"]);
        data_obj.push(result["Remarks"]);
        
        data.push(data_obj);
        next();
      }, function(){
        return callback(data);
      });
    } else {
      return callback(data);
    }
  });
}


module.exports = router;
