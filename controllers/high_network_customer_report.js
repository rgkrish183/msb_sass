var config = require('../config/database');
var dbUtil = require('../config/dbUtil');
var express = require('express');
var router = express.Router();
var auth = require('../middlewares/auth'), chk_loginaccess = auth.chk_loginaccess;
var report = require('../models/high_network_customer_report');
var common_functions = require('../middlewares/common_functions');
var MODCONF = require('../config/module_config');
const PRMS = MODCONF.PRMS;
const MDUL = MODCONF.MDUL;
var mongo = require('mongodb');

/**
 * USE : ROOT LIST LOADER AND POPUP EXPAND VIEW FOR 3 REPORTS OF LOCAL, INDIVIDUAL AND FOREIGN BUSINESS TRANSACTIONS 
 * MODULE : HIGH RISK CUSTOMERS
 * MAIN : http://192.168.1.187:3000/highrisk_customerlist_report/highrisk_customerlist
 */
router.get('/', chk_loginaccess, function(req, res, next) {

  var fromdate = req.query.fromdate || '';
  var todate = req.query.todate || '';
  var nationality = req.query.nationality || '';
  var customerType = req.query.type || '';
  var action = req.query.action;

  var datasource_subqry = new Object();
  datasource_subqry["data_proprietor"] = new mongo.ObjectID(req.session.user.ses_userid);
  
  if(fromdate == '' || todate == '' || nationality=='' || customerType=='') {
    console.log({"status":"failed", "message":"fromdate, todate, nationality and type required!!!"});
    res.send([]);
  } else {
    var from_spliter = fromdate.split("-");
    var to_spliter = todate.split("-");
    var fmdt_cnter = new Date(from_spliter[2]+"-"+from_spliter[1]+"-"+from_spliter[0]);
    var todt_cnter = new Date(to_spliter[2]+"-"+to_spliter[1]+"-"+to_spliter[0]);
    var params ={};

    if(action == "rmk_ten") {
      params = {
        fmdt_cnter:fmdt_cnter, 
        todt_cnter:todt_cnter, 
        type:decodeURI(customerType),
        nationality:nationality, 
        datasource_subqry:datasource_subqry,
        action:"10000"
      };
    } else if(action == "rmk_twenty") {
      params = {
        fmdt_cnter:fmdt_cnter, 
        todt_cnter:todt_cnter, 
        type:decodeURI(customerType),
        nationality:nationality, 
        datasource_subqry:datasource_subqry,
        action:"20000"
      };
    } else if(action == "rmk_thirty") {
      params = {
        fmdt_cnter:fmdt_cnter, 
        todt_cnter:todt_cnter, 
        type:decodeURI(customerType),
        nationality:nationality, 
        datasource_subqry:datasource_subqry,
        action:"30000"
      };
    } else if(action == "rmk_fourty") {
      params = {
        fmdt_cnter:fmdt_cnter, 
        todt_cnter:todt_cnter, 
        type:decodeURI(customerType),
        nationality:nationality, 
        datasource_subqry:datasource_subqry,
        action:"40000"
      };
    } else if(action == "rmk_fifty") {
      params = {
        fmdt_cnter:fmdt_cnter, 
        todt_cnter:todt_cnter, 
        type:decodeURI(customerType),
        nationality:nationality, 
        datasource_subqry:datasource_subqry,
        action:"50000"
      };
    } else if(action == "rmk_fiftyabove") {
      params = {
        fmdt_cnter:fmdt_cnter, 
        todt_cnter:todt_cnter, 
        type:decodeURI(customerType),
        nationality:nationality, 
        datasource_subqry:datasource_subqry,
        action:"60000"
      };
    } 

    report.high_network_customer_report(params, function(err, result){
      if(err) {
        console.log(err);
        res.send(err)
      } else {
        res.json(result);
      }
    });
  }
});

module.exports = router;