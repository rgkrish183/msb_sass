var express = require('express');
var router = express.Router();
var dbUtil = require('../../config/dbUtil');
var async=require('async');
var mongo = require('mongodb');
var config = require('../../config/database');
var auth = require('../../middlewares/auth'), chk_loginaccess = auth.chk_loginaccess;
var common_functions = require('../../middlewares/common_functions');

/**
 * MODULE : PERMISSIONS
 * USE : PERMISSION LIST VIEW
 * VIEW : ADMIN/PERMISSIONS_LIST
 */
router.get('/', chk_loginaccess, async(req,res)=>{

    try {
        const permissions=dbUtil.get().collection(config.tbl_permissions);
        let permission;
       
        if(req.param("id")) {
            let datasource_id = new mongo.ObjectID(req.param("id"));
            permission=await permissions.findOne({_id:datasource_id});
            res.render("./admin/permissions_list",{users:[permission],"domain_url":config.domain, "pagename":"permissions", "ses_user" : req.session.user}) 
        } else {
            permission=await permissions.find({}).toArray();
            res.render("./admin/permissions_list",{users:permission,"domain_url":config.domain, "pagename":"permissions", "ses_user" : req.session.user }) 
        }
    } catch(error) {
        res.json({"success":"false","data":[]})
    }
});

/**
 * MODULE : PERMISSIONS
 * USE : PERMISSION INSERT ACTION
 */
router.post('/create',async(req,res)=>{

    try {
        if(req.body.label == null || req.body.field == null || req.body.label == "" || req.body.field == "") {
            req.flash('BAD', 'Required fields are missing, try again', false);
            res.redirect("/permissions");
        } else {
            const permissions=dbUtil.get().collection(config.tbl_permissions);

            let permission;
            const label=req.body.label;
            const field=req.body.field;
            permission=await permissions.insert({"label":label,"field":field});

            if(permission) {
                req.flash('GOOD', 'Permissions added Successfully!', false);
                res.status(200).send({status:true, msg:"Permissions Added Successfully!"});
            } else {
                req.flash('BAD', 'Somthing went wrong, try again!', false);
                res.status(204).send({status:false, msg:"Somthing went wrong!"});
            }
        }
    } catch(error) {
        res.json({"success":"false","data":[],"error":error.message})
    }
});

/**
 * MODULE : PERMISSIONS
 * USER : PERMISSIONS UPDATE ACTIONS
 */
router.post('/update',async(req,res)=>{

    try {

        if(req.body.label == null || req.body.field == null){
            req.flash('BAD', 'Required fields are missing, try again', false);
            res.redirect("/permissions");
        } else {
            const permissions=dbUtil.get().collection(config.tbl_permissions);

            let permission;
            const label=req.body.label;
            const field=req.body.field;
            
            let id = new mongo.ObjectID(req.param("id"));
            permission=await permissions.update({_id:id},{$set:{"label":label,"field":field}});

            if(permission) {
                req.flash('GOOD', 'Permissions Updated Successfully!', false);
                res.status(200).send({status:true, msg:"Permissions Updated Successfully!"});
            } else {
                req.flash('BAD', 'Somthing went wrong, try again!', false);
                res.status(204).send({status:false, msg:"Somthing went wrong!"});
            }
        }
    } catch(error) {
        res.json({"success":"false","data":[],"error":error.message})
    }
});

/**
 * MODULE : PERMISSIONS
 * USER : PERMISSIONS DELETE ACTIONS
 */
router.post('/delete/:id',async(req,res)=>{

    try {
        if(req.params.id == null){
            req.flash('BAD', 'Required fields are missing, try again', false);
            res.redirect("/permissions");
        } else {
            const permissions=dbUtil.get().collection(config.tbl_permissions);
 
            let id= new mongo.ObjectID(req.params.id);
            permission=await permissions.remove({_id:id});
            res.redirect('/permissions');

            if(permission) {
                req.flash('GOOD', 'Permissions Deleted Successfully!', false);
                res.status(200).send({status:true, msg:"Permissions Deleted Successfully!"});
            } else {
                req.flash('BAD', 'Somthing went wrong, try again!', false);
                res.status(204).send({status:false, msg:"Somthing went wrong!"});
            }
        }
    } catch(error) {
        res.json({"success":"false","data":[],"error":error.message});
    }

});

/**
 * MODULE : PERMISSIONS
 * USE : TO PERMISSION ADD VIEW 
 * VIEW : ADMIN/PERMISSIONS_CREATE
 */
router.get('/create/get',async(req,res)=>{
    try {
        res.render("./admin/permissions_create",{data:{},"domain_url":config.domain, "pagename":"permissions", "ses_user" : req.session.user})
    } catch(e) {
        res.send(e.message);
    }
 });

/**
 * MODULE : PERMISSIONS
 * USE : TO PERMISSION ADD VIEW 
 * VIEW : ADMIN/PERMISSIONS_CREATE
 */
 router.get('/update/get/:id',async(req,res)=>{

   try {
        const permissions=dbUtil.get().collection('tbl_permissions');
        let datasource_id = new mongo.ObjectID(req.params.id);
        let  permission=await permissions.findOne({_id:datasource_id});
        res.render("./admin/permissions_update",{data:permission,"domain_url":config.domain, "pagename":"permissions", "ses_user" : req.session.user})
    } catch(e) {
        res.send(e.message);
    }
});

/**
 * MODULE : PERMISSIONS
 * USE : TO CHECK THE LABEL SHOULD BE ALREADY EXISTS OR NOT
 */
router.get('/chk_label_exists', chk_loginaccess, async (req, res) => {

    let u_userid = req.query.id;
    let u_username = decodeURI(req.query.label);
    let u_username_lower = u_username.toLowerCase();

    let matcher = new Object();

    if(u_userid !== undefined && u_userid != "") {
        let pk_user_id = new mongo.ObjectID(u_userid);
        matcher["_id"] = {"$ne" : pk_user_id};
    }
    matcher["label"] = new RegExp(["^", u_username_lower.trim(), "$"].join(""), "i");;

    console.log(matcher);
    dbUtil.get().collection(config.tbl_permissions).find(matcher).toArray(function(err, users_data) {

        if(users_data.length > 0){
            return res.status(200).send(false);
        } else {
            return res.status(200).send(true);
        }
    });    
});

/**
 * MODULE : PERMISSIONS
 * USE : TO CHECK THE FIELD SHOULD BE ALREADY EXISTS OR NOT
 */
router.get('/chk_field_exists', chk_loginaccess, async (req, res) => {

    let u_userid = req.query.id;
    let u_username = decodeURI(req.query.field);
    let u_username_lower = u_username.toLowerCase();

    let matcher = new Object();

    if(u_userid !== undefined && u_userid != "") {
        let pk_user_id = new mongo.ObjectID(u_userid);
        matcher["_id"] = {"$ne" : pk_user_id};
    }
    matcher["field"] = new RegExp(["^", u_username_lower.trim(), "$"].join(""), "i");;

    dbUtil.get().collection(config.tbl_permissions).find(matcher).toArray(function(err, users_data) {

        if(users_data.length > 0){
            return res.status(200).send(false);
        } else {
            return res.status(200).send(true);
        }
    });
});

module.exports=router;