var config = require('../../config/database');
var dbUtil = require('../../config/dbUtil');
var express = require('express');
var router = express.Router();
var each = require('async-each');
var auth = require('../../middlewares/auth'), chk_loginaccess = auth.chk_loginaccess;
var common_functions = require('../../middlewares/common_functions');
var mongo = require('mongodb');
var bcrypt = require('bcrypt');
var MODCONF = require('../../config/module_config');
const PRMS = MODCONF.PRMS;
const MDUL = MODCONF.MDUL;

/**
 * MODULE : USERS 
 * USE : USERS GET LIST
 * VIEW : ADMIN/USERS_LIST
 */
router.get('/', chk_loginaccess, async (req, res) => {

    let rbac_obj = res.locals.rbac_obj;

    // URL VALIDATIONS
    let rbac_urls = new Object();
    rbac_urls["rbac_obj"] = rbac_obj;
    rbac_urls["roles"] = req.session.user.ses_rolename;
    rbac_urls["actions"] = PRMS["PER_URL"];
    rbac_urls["url"] = MDUL["MODURL_USERS"];
    rbac_urls["usertype"] = req.session.user.ses_usertype;

    var url_validation = await common_functions.go_modules_rbacvalidations(rbac_urls);

    if(url_validation) {

        // //OVERVIEW BUYSELL
        let rbac1 = new Object();
        rbac1["rbac_obj"] = rbac_obj;
        rbac1["roles"] = req.session.user.ses_rolename;
        rbac1["actions"] = PRMS["PER_LISTVIEW"];
        rbac1["url"] = MDUL["MODURL_USERS"];
        rbac1["usertype"] = req.session.user.ses_usertype;
        let report_list = await common_functions.go_modules_rbacvalidations(rbac1);

        let rbac2 = new Object();
        rbac2["rbac_obj"] = rbac_obj;
        rbac2["roles"] = req.session.user.ses_rolename;
        rbac2["actions"] = PRMS["PER_ADD"];
        rbac2["url"] = MDUL["MODURL_USERS"];
        rbac2["usertype"] = req.session.user.ses_usertype;
        let report_add = await common_functions.go_modules_rbacvalidations(rbac2);

        let rbac3 = new Object();
        rbac3["rbac_obj"] = rbac_obj;
        rbac3["roles"] = req.session.user.ses_rolename;
        rbac3["actions"] = PRMS["PER_EDIT"];
        rbac3["url"] = MDUL["MODURL_USERS"];
        rbac3["usertype"] = req.session.user.ses_usertype;
        let report_edit = await common_functions.go_modules_rbacvalidations(rbac3);

        let rbac4 = new Object();
        rbac4["rbac_obj"] = rbac_obj;
        rbac4["roles"] = req.session.user.ses_rolename;
        rbac4["actions"] = PRMS["PER_DELETE"];
        rbac4["url"] = MDUL["MODURL_USERS"];
        rbac4["usertype"] = req.session.user.ses_usertype;
        let report_delete = await common_functions.go_modules_rbacvalidations(rbac4);
        
        const result = new Promise(async(resolve, reject) => {

            try {
                let ses_userid = new mongo.ObjectID(req.session.user.ses_userid);
                let matcher = new Object();
                let usertype= null;

                if(req.session.user.ses_usertype == config.usertype_access.superadmin){
                    usertype=config.usertype_access.superadmin;
                    matcher["u_usertype"]=config.usertype_access.admin;
                }else if(req.session.user.ses_usertype == config.usertype_access.admin){
                    usertype=config.usertype_access.admin;
                    matcher["u_organizationid"]=new mongo.ObjectID(req.session.user.ses_organization);
                }else if(req.session.user.ses_usertype == config.usertype_access.system_admin){
                    usertype=config.usertype_access.system_admin;
                    matcher["u_organizationid"]=new mongo.ObjectID(req.session.user.ses_organization);
                }else if(req.session.user.ses_usertype == config.usertype_access.users){
                    usertype=config.usertype_access.users;
                    matcher["u_organizationid"]=new mongo.ObjectID(req.session.user.ses_organization);
                }

                let users_results = await dbUtil.get().collection(config.tbl_users).aggregate([
                    { $match: matcher },
                    { $match: { u_usertype: { $gt: usertype }  } },
                    { $lookup: { from: config.tbl_roles, localField: 'u_role', foreignField: '_id', as: 'role' } },
                    { $sort : {"_id" : -1} }
                ]).toArray();

                let rtn_users_data = new Array();
            
                if(users_results.length > 0) {
            
                    each(users_results, async (users_item, next_mover) => {

                        let temp_obj = new Object();
                        temp_obj["id"] = users_item._id;
                        temp_obj["u_name"] = users_item.u_firstname+" "+users_item.u_lastname;
                        temp_obj["u_email"] = users_item.u_email;
                        temp_obj["u_mobile"] = users_item.u_mobile;
                        temp_obj["u_username"] = users_item.u_username;
                        temp_obj["u_rolename"] = users_item.role[0].label;

                        let org_id = new mongo.ObjectID(users_item.u_organizationid);
                        let org_finder = { "_id" : org_id };
                        var org_collection_data = await dbUtil.get().collection(config.tbl_organization).findOne(org_finder);
                        temp_obj["u_orgname"] = org_collection_data.u_orgname;
                        
                        rtn_users_data.push(temp_obj);
                        next_mover();
                    }, function() {
                        resolve(rtn_users_data);                         
                    });
                } else {
                    resolve(rtn_users_data);                         
                }
            } catch (e) {
                console.log(e);
                reject(e);
            }
        });

        // URL PARAMS
        var urlquery_request = new Object();
        urlquery_request["fromdate"] = req.query.fromdate;
        urlquery_request["todate"] = req.query.todate;

        // RENDER PAGE PARAMS 
        let render_params_obj = new Object();
        render_params_obj["pagename"] = "users";
        render_params_obj["domain_url"] = config.domain;
        render_params_obj["ses_user"] = req.session.user;
        render_params_obj["urlquery_request"] = urlquery_request;
        render_params_obj["pos_users"] = {"report_list":report_list, "report_add":report_add, "report_edit":report_edit, "report_delete":report_delete};

        result.then(function(rtn_users_data){
            render_params_obj["data"] = rtn_users_data;
            res.render('admin/users_list', render_params_obj);
        });    

    } else {
        res.render('errors/errors',{"domain_url":config.domain, "error_code":401, "error_mainmsg":"Module Permission Denied !"});
    }
});

/**
 * MODULE : USERS
 * USE : FOR SHOWING USERS EDIT SCREEN
 * VIEW : ADMIN/USERS_EDIT
 */
router.get('/edit/:id', chk_loginaccess, async (req, res) => {

    let rbac_obj = res.locals.rbac_obj;

    let rbac2 = new Object();
    rbac2["rbac_obj"] = rbac_obj;
    rbac2["roles"] = req.session.user.ses_rolename;
    rbac2["actions"] = PRMS["PER_EDIT"];
    rbac2["url"] = MDUL["MODURL_USERS"];
    rbac2["usertype"] = req.session.user.ses_usertype;
    let report_update = await common_functions.go_modules_rbacvalidations(rbac2);

    if(report_update) {

        let user_id = req.params.id;
        var rtn_users_data = new Array();
        var roles_results;
        var users_reporting_results;

        const result = new Promise(async(resolve, reject) => {

            try {
                let rtn_users_data = new Array();
                console.log(user_id === undefined || user_id == "");

                if(user_id === undefined || user_id == "") {
                    resolve(rtn_users_data);
                } else {
                    console.log(user_id);

                    let pk_userid = new mongo.ObjectID(user_id);
                    let ses_userid = new mongo.ObjectID(req.session.user.ses_userid);
                    let ses_roleid = new mongo.ObjectID(req.session.user.ses_role);
                    
                    let matcher = new Object();
                    matcher["_id"] = pk_userid;

                    if(req.session.user.ses_usertype == config.usertype_access.admin) { 
                        matcher["u_reporting_hierarchy"] = ses_userid;
                    }

                    let users_results = await dbUtil.get().collection(config.tbl_users).aggregate([
                        { $match: matcher },
                        { $lookup: { from: config.tbl_roles, localField: 'u_role', foreignField: '_id', as: 'role' } }
                    ]).toArray();

                    // roles_results = await dbUtil.get().collection(config.tbl_roles).find({}).toArray();
               
                    var parent_position =await dbUtil.get().collection(config.tbl_roles).findOne({"_id":ses_roleid});
    
                    roles_results = await dbUtil.get().collection(config.tbl_roles).find({ parent_role_position: { $gt:  parent_position.parent_role_position } }).toArray();

                    // users_reporting_results = await dbUtil.get().collection(config.tbl_users).aggregate([
                    //     { $match: { "u_reporting_hierarchy": ses_userid, "_id" : { $ne : pk_userid } } },
                    //     { $lookup: { from: "tbl_roles", localField: 'u_role', foreignField: '_id', as: 'role' } },
                    //     { $project : { "u_name" : {$concat: ["$u_firstname"," ","$u_lastname"]}} }
                    // ]).toArray();

                    users_reporting_results = await dbUtil.get().collection(config.tbl_users).aggregate([
                        { $match: { $or :  [{ "u_reporting_hierarchy": ses_userid }, { "_id": ses_userid } ] } },
                        { $lookup: { from: "tbl_roles", localField: 'u_role', foreignField: '_id', as: 'role' } },
                        { $project : { "u_name" : {$concat: ["$u_firstname"," ","$u_lastname"]}} }
                    ]).toArray();                    

                    if(users_results.length > 0) {
            
                        each(users_results, function (users_item, next_mover) {

                            let temp_obj = new Object();
                            temp_obj["id"] = users_item._id;
                            temp_obj["u_firstname"] = users_item.u_firstname;
                            temp_obj["u_lastname"] = users_item.u_lastname;
                            temp_obj["u_email"] = users_item.u_email;
                            temp_obj["u_mobile"] = users_item.u_mobile;
                            temp_obj["u_username"] = users_item.u_username;                                
                            temp_obj["u_password"] = users_item.u_password;                                
                            temp_obj["u_roleid"] = (users_item.role[0]._id).valueOf();
                            temp_obj["u_reporting_to"] = (users_item.u_reporting_to).valueOf();
                            
                            rtn_users_data.push(temp_obj);
                            next_mover();
                        }, function() {
                            resolve(rtn_users_data);                         
                        });
                    } else {
                        resolve(rtn_users_data);                         
                    }                        
                }
            } catch (e) {
                console.log(e);
                reject(e);
            }
        });

        result.then(function(rtn_users_data){

            // URL PARAMS
            var urlquery_request = new Object();
            urlquery_request["fromdate"] = req.query.fromdate;
            urlquery_request["todate"] = req.query.todate;

            // RENDER PAGE PARAMS 
            let render_params_obj = new Object();
            render_params_obj["pagename"] = "users";
            render_params_obj["domain_url"] = config.domain;
            render_params_obj["ses_user"] = req.session.user;
            render_params_obj["urlquery_request"] = urlquery_request;
            render_params_obj["data"] = rtn_users_data;
            render_params_obj["data_roles"] = roles_results;
            render_params_obj["data_reporting"] = users_reporting_results;

            if(rtn_users_data.length > 0)
                res.render('admin/users_edit', render_params_obj);
            else 
                res.render('errors/errors',{"domain_url":config.domain, "error_code":400, "error_mainmsg":"Something Went Wrong. Action id is required !", });
        });
    }
});

/**
 * MODULE : USERS
 * USE : TO UPDATE THE USERS DETAILS IN DB
 */
router.post('/update/:id', chk_loginaccess, async (req, res) => {

    let rbac_obj = res.locals.rbac_obj;
    
    let rbac2 = new Object();
    rbac2["rbac_obj"] = rbac_obj;
    rbac2["roles"] = req.session.user.ses_rolename;
    rbac2["actions"] = PRMS["PER_EDIT"];
    rbac2["url"] = MDUL["MODURL_USERS"];
    rbac2["usertype"] = req.session.user.ses_usertype;
    let report_update = await common_functions.go_modules_rbacvalidations(rbac2);

    if(report_update) {

        let user_id = req.params.id;
        var rtn_users_data = new Array();
        var roles_results;
        var users_reporting_results;

        const result = new Promise(async(resolve, reject) => {

            try {
                if(user_id === undefined || user_id == "") {
                    resolve(false);
                } else {

                    var pk_user_id = new mongo.ObjectID(user_id);

                    let user_update_obj = new Object();

                    user_update_obj["u_firstname"] = req.body.u_firstname;
                    user_update_obj["u_lastname"] = req.body.u_lastname;
                    user_update_obj["u_email"] = req.body.u_email;
                    user_update_obj["u_mobile"] = req.body.u_mobile;
                    user_update_obj["u_username"] = req.body.u_username;
                    user_update_obj["u_password"] = req.body.u_password;
                    user_update_obj["u_password_salter"] = bcrypt.hashSync((req.body.u_password)+config.login_salt, 16);
                    
                    if(req.session.user.ses_usertype==config.usertype_access.superadmin){

                        let super_roles_data =await dbUtil.get().collection(config.tbl_roles).findOne({"parent_role_position": config.usertype_access.admin});
    
                        user_update_obj["u_role"] = new mongo.ObjectID(super_roles_data._id);
                        user_update_obj["u_usertype"] = super_roles_data.parent_role_position;
                    }else{
    
                        let parent_position =await dbUtil.get().collection(config.tbl_roles).findOne({"_id":new mongo.ObjectID(req.body.u_role)});
    
                        user_update_obj["u_role"] = new mongo.ObjectID(parent_position._id);
                        user_update_obj["u_usertype"] = parent_position.parent_role_position;
                    }

                    let u_reporting_to = (req.body.u_reporting_to !== undefined && req.body.u_reporting_to != "") ? new mongo.ObjectID(req.body.u_reporting_to) : "";
                    user_update_obj["u_reporting_to"] = u_reporting_to;

                    if(req.session.user.ses_usertype == config.usertype_access.superadmin) { 
                        user_update_obj["u_reporting_hierarchy"] = new Array();
                    } else {
                        let u_reporting_hierarchy_results  = await dbUtil.get().collection(config.tbl_users).aggregate([
                            { $match: { "_id": u_reporting_to } },
                        ]).toArray();
                        let reproting_hr = u_reporting_hierarchy_results[0].u_reporting_hierarchy;
                        reproting_hr.push(u_reporting_to);
                        user_update_obj["u_reporting_hierarchy"] = reproting_hr;
                    }
                    
                    var userupdate_collection = await dbUtil.get().collection(config.tbl_users).update({ "_id" : pk_user_id }, { $set: user_update_obj });

                    if(userupdate_collection){
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                }
            } catch (e) {
                console.log(e);
                reject(e);
            }
        });

        result.then(function(rtn_usersupdate_status){

            if(rtn_usersupdate_status) {
                req.flash('GOOD', 'Users updated Successfully!', false);
                res.status(200).send({status:true, msg:"Users updated Successfully!"});
            } else {
                req.flash('BAD', 'Somthing went wrong, try again!', false);
                res.status(204).send({status:false, msg:"Somthing went wrong!"});
            }
        });
    }
});

/**
 * MODULE : USERS
 * USE : FOR SHOWING USERS ADD SCREEN
 * VIEW : ADMIN/USERS_ADD
 */
router.get('/add', chk_loginaccess, async (req, res) => {

    let rbac_obj = res.locals.rbac_obj;

    let rbac2 = new Object();
    rbac2["rbac_obj"] = rbac_obj;
    rbac2["roles"] = req.session.user.ses_rolename;
    rbac2["actions"] = PRMS["PER_ADD"];
    rbac2["url"] = MDUL["MODURL_USERS"];
    rbac2["usertype"] = req.session.user.ses_usertype;
    let report_add = await common_functions.go_modules_rbacvalidations(rbac2);

    if(report_add) {

        var roles_results;
        var parent_position;
        var users_reporting_results;
        const result = new Promise(async(resolve, reject) => {

            try {
                let ses_userid = new mongo.ObjectID(req.session.user.ses_userid);
                let ses_roleid = new mongo.ObjectID(req.session.user.ses_role);
               
                parent_position =await dbUtil.get().collection(config.tbl_roles).findOne({"_id":ses_roleid});

                roles_results = await dbUtil.get().collection(config.tbl_roles).find({ parent_role_position: { $gt:  parent_position.parent_role_position } }).toArray();

                users_reporting_results = await dbUtil.get().collection(config.tbl_users).aggregate([
                    { $match: { $or :  [{ "u_reporting_hierarchy": ses_userid }, { "_id": ses_userid } ] } },
                    { $lookup: { from: "tbl_roles", localField: 'u_role', foreignField: '_id', as: 'role' } },
                    { $project : { "u_name" : {$concat: ["$u_firstname"," ","$u_lastname"]}} }
                ]).toArray();

                resolve(users_reporting_results);
            } catch (e) {
                console.log(e);
                reject(e);
            }
        });
        
        // URL PARAMS
        var urlquery_request = new Object();
        urlquery_request["fromdate"] = "";
        urlquery_request["todate"] = "";

        // RENDER PAGE PARAMS 
        let render_params_obj = new Object();
        render_params_obj["pagename"] = "dashboard";
        render_params_obj["domain_url"] = config.domain;
        render_params_obj["ses_user"] = req.session.user;
        render_params_obj["urlquery_request"] = urlquery_request;
        
        result.then(function(rtn_users_data){
            render_params_obj["data_roles"] = roles_results;
            render_params_obj["data_reporting"] = users_reporting_results;
            res.render('admin/users_add', render_params_obj);
        });    
    } else {
        res.render('errors/errors',{"domain_url":config.domain, "error_code":401, "error_mainmsg":"Module Permission Denied !"});
    }
});

/**
 * MODULE : USERS
 * USE : TO INSERT THE USERS DETAILS IN DB
 */
router.post('/create', chk_loginaccess, async (req, res) => {

    let rbac_obj = res.locals.rbac_obj;
    
    let rbac2 = new Object();
    rbac2["rbac_obj"] = rbac_obj;
    rbac2["roles"] = req.session.user.ses_rolename;
    rbac2["actions"] = PRMS["PER_ADD"];
    rbac2["url"] = MDUL["MODURL_USERS"];
    rbac2["usertype"] = req.session.user.ses_usertype;
    let report_add = await common_functions.go_modules_rbacvalidations(rbac2);
    
    var files = req.files[0];
    var upload_newpath="";
    var upload_filename = "";

    if(files){
        files = files.path;
        upload_filename=files.originalname;
    }
    

    if(report_add) {

        const result = new Promise(async(resolve, reject) => {

            try {
                let user_add_obj = new Object();

                let req_u_username = req.body.u_username;
                let u_username_lower = req_u_username.toLowerCase();
                
                user_add_obj["u_firstname"] = req.body.u_firstname;
                user_add_obj["u_middlename"] = req.body.u_middlename;
                user_add_obj["u_lastname"] = req.body.u_lastname;
                user_add_obj["u_email"] = req.body.u_email;
                user_add_obj["u_mobile"] = req.body.u_mobile;
                user_add_obj["u_phone"] = req.body.u_phone;
                user_add_obj["u_username"] = u_username_lower;
                user_add_obj["u_password"] = req.body.u_password;
                user_add_obj["u_password_salter"] = bcrypt.hashSync((req.body.u_password)+config.login_salt, 16);

                var u_reporting_hierarchy_arr = new Array();
                var u_systadmin_results = new Array();
                
                if(req.session.user.ses_usertype==config.usertype_access.superadmin){

                    let super_roles_data =await dbUtil.get().collection(config.tbl_roles).findOne({"parent_role_position": config.usertype_access.admin});

                    user_add_obj["u_role"] = new mongo.ObjectID(super_roles_data._id);
                    user_add_obj["u_usertype"] = super_roles_data.parent_role_position;
                    
                } else {

                    let parent_position =await dbUtil.get().collection(config.tbl_roles).findOne({"_id":new mongo.ObjectID(req.body.u_role)});

                    user_add_obj["u_role"] = new mongo.ObjectID(parent_position._id);
                    user_add_obj["u_usertype"] = parent_position.parent_role_position;

                    if(parent_position.parent_role_position != config.usertype_access.admin || parent_position.parent_role_position != config.usertype_access.system_admin) { 

                        u_systadmin_results = await dbUtil.get().collection(config.tbl_users).aggregate([
                            { $match: 
                                { $and : [
                                    { "u_usertype": config.usertype_access.system_admin },
                                    { "u_organizationid":  new mongo.ObjectID(req.session.user.ses_organization) }
                                ]  } }
                        ]).toArray();
                    }
                }
                
                let u_reporting_to = (req.body.u_reporting_to !== undefined && req.body.u_reporting_to != "") ? new mongo.ObjectID(req.body.u_reporting_to) : "";
                user_add_obj["u_reporting_to"] = u_reporting_to;

                if(req.session.user.ses_usertype == config.usertype_access.superadmin) { 
                    user_add_obj["u_reporting_hierarchy"] = new Array();
                } else {
                    let u_reporting_hierarchy_results = await dbUtil.get().collection(config.tbl_users).aggregate([
                        { $match: { "_id": u_reporting_to } },
                    ]).toArray();

                    console.log(u_reporting_hierarchy_results);

                    if(u_reporting_to != ""){

                        if(u_reporting_hierarchy_results.length > 0){

                            let reproting_hr = u_reporting_hierarchy_results[0].u_reporting_hierarchy;
                            reproting_hr.push(u_reporting_to);

                            if(u_systadmin_results.length > 0){

                                each(u_systadmin_results, function(sysadmin_data, next_mover){
                                    reproting_hr.push(sysadmin_data._id);                            
                                    next_mover();
                                }, function(){

                                    var data_proprietor_strconveter = new Array(); 
                                    reproting_hr.forEach(_id => {
                                      data_proprietor_strconveter.push(_id.toString()); 
                                    });
        
                                    var data_proprietor_action_stringid = new Set(data_proprietor_strconveter);
                                    var data_proprietor_ranger = new Array();
        
                                    data_proprietor_action_stringid.forEach(action_id => { 
                                      data_proprietor_ranger.push(new mongo.ObjectID(action_id)); 
                                    });

                                    user_add_obj["u_reporting_hierarchy"] = data_proprietor_ranger;
                                });
                            } else {

                                var data_proprietor_strconveter = new Array(); 
                                reproting_hr.forEach(_id => {
                                  data_proprietor_strconveter.push(_id.toString()); 
                                });
    
                                var data_proprietor_action_stringid = new Set(data_proprietor_strconveter);
                                var data_proprietor_ranger = new Array();
    
                                data_proprietor_action_stringid.forEach(action_id => { 
                                  data_proprietor_ranger.push(new mongo.ObjectID(action_id)); 
                                });

                                user_add_obj["u_reporting_hierarchy"] = data_proprietor_ranger;
                            }

                        } else {
                            user_add_obj["u_reporting_hierarchy"] = [u_reporting_to];
                        }
                    }
                }

                //creation organization and user object
                if(req.session.user.ses_usertype==1){

                    let org_add_obj = new Object();
                    org_add_obj["u_orgname"] = req.body.o_orgname;
                    org_add_obj["u_fax"] = req.body.o_fax;
                    org_add_obj["u_website"] = req.body.o_website;

                    org_add_obj["u_organizationaddress"] = req.body.o_orgaddress;
                    org_add_obj["u_organization_logo"] = upload_filename;
    
                    let org_add_collections = await dbUtil.get().collection(config.tbl_organization).save(org_add_obj);
                    user_add_obj["u_createdby"] = new mongo.ObjectID(req.session.user.ses_userid); 
                    user_add_obj["u_organizationid"] = org_add_collections.ops[0]._id;
                    let user_add_collections = await dbUtil.get().collection(config.tbl_users).insertOne(user_add_obj);

                    console.log(user_add_collections);
                    if(user_add_collections) 
                        resolve(true);
                    else
                        resolve(false);

                } else {
                    //creating user object
                    user_add_obj["u_createdby"] = new mongo.ObjectID(req.session.user.ses_userid);
                    user_add_obj["u_organizationid"] =  new mongo.ObjectID(req.session.user.ses_organization);

                    let user_add_collections = await dbUtil.get().collection(config.tbl_users).insertOne(user_add_obj);

                    console.log(user_add_collections);
                    if(user_add_collections) 
                        resolve(true);
                    else
                        resolve(false);
                }
            } catch (e) {
                console.log(e);
                reject(e);
            }
        });

        result.then(function(rtn_useradd_status){

            if(rtn_useradd_status) {
                req.flash('GOOD', 'Users added Successfully!', false);
                res.status(200).send({status:true, msg:"Users Added Successfully!"});
            } else {
                req.flash('BAD', 'Somthing went wrong, try again!', false);
                res.status(204).send({status:false, msg:"Somthing went wrong!"});
            }
        });
    }
});

/**
 * MODULE : USERS
 * USE : TO INSERT THE USERS DETAILS IN DB
 */
router.post('/delete/:id', chk_loginaccess, async (req, res) => {

    let rbac_obj = res.locals.rbac_obj;

    let rbac2 = new Object();
    rbac2["rbac_obj"] = rbac_obj;
    rbac2["roles"] = req.session.user.ses_rolename;
    rbac2["actions"] = PRMS["PER_DELETE"];
    rbac2["url"] = MDUL["MODURL_USERS"];
    rbac2["usertype"] = req.session.user.ses_usertype;
    let report_add = await common_functions.go_modules_rbacvalidations(rbac2);

    if(report_add) {

        const result = new Promise(async(resolve, reject) => {

            try {
                let user_id = req.params.id;

                if(user_id === undefined || user_id == "") {
                    resolve(false);
                } else {

                    let pk_user_id = new mongo.ObjectID(user_id);
                    let ses_userid = new mongo.ObjectID(req.session.user.ses_userid);

                    let matcher = new Object();
                    matcher["_id"] = pk_user_id;

                    if(req.session.user.ses_usertype == config.usertype_access.admin) { 
                        matcher["u_reporting_hierarchy"] = ses_userid;
                    }
                    var user_remove_collections = await dbUtil.get().collection(config.tbl_users).remove(matcher);

                    console.log(user_remove_collections);
                    
                    if(user_remove_collections) 
                        resolve(true);
                    else
                        resolve(false);
                }
            } catch (e) {
                console.log(e);
                reject(e);
            }
        });

        result.then(function(rtn_useradd_status){

            if(rtn_useradd_status) {
                req.flash('GOOD', 'Users Deleted Successfully!', false);
                res.status(200).send({status:true, msg:"Users Deleted Successfully!"});
            } else {
                req.flash('BAD', 'Somthing went wrong, try again!', false);
                res.status(204).send({status:false, msg:"Somthing went wrong!"});
            }
        });
    }
});

/**
 * MODULE : USERS
 * USE : TO CHECK THE USERSNAME SHOULD BE ALREADY EXISTS OR NOT
 */
router.get('/chk_username_exists', chk_loginaccess, async (req, res) => {

    let u_userid = req.query.u_userid;
    let u_username = decodeURI(req.query.u_username);
    let u_username_lower = u_username.toLowerCase();

    let matcher = new Object();

    if(u_userid !== undefined && u_userid != "") {
        let pk_user_id = new mongo.ObjectID(u_userid);
        matcher["_id"] = {"$ne" : pk_user_id};
    }
    matcher["u_username"] = new RegExp(["^", u_username_lower.trim(), "$"].join(""), "i");

    dbUtil.get().collection(config.tbl_users).find(matcher).toArray(function(err, users_data) {

        if(users_data.length > 0){
            return res.status(200).send(false);
        } else {
            return res.status(200).send(true);
        }
    });
});


module.exports = router;
