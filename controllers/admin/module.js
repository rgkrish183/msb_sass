var config = require('../../config/database');
var dbUtil = require('../../config/dbUtil');
var express = require('express');
var router = express.Router();
var each = require('async-each');
var auth = require('../../middlewares/auth'), chk_loginaccess = auth.chk_loginaccess;
var common_functions = require('../../middlewares/common_functions');
var mongo = require('mongodb');
var module_report = require('../../models/admin/module');

/**
 * MODULE : MODULE 
 * USE : MODULE GET LIST
 * VIEW : ADMIN/MODULE_LIST
 */
router.get('/', chk_loginaccess, async (req, res) => {

    const result = new Promise(async(resolve, reject) => {

        try {

            let module_params = new Object();

            module_report.getalldata(module_params, function(module_results){

                let rtn_users_data = new Array();
          
                if(module_results.length > 0) {
            
                    each(module_results, function (module_items, next_mover) {

                        let temp_obj = new Object();
                        temp_obj["id"] = module_items._id;
                        temp_obj["mdl_label"] = module_items.label;
                        temp_obj["mdl_field"] = module_items.field;
                        temp_obj["md1_root_url"] = parseInt(module_items.root_url);
                        let root_parent = module_items.root_parent;
                        if(root_parent.length > 0) {
                            temp_obj["mdl_root_parent_name"] = root_parent[0].label;
                        } else {
                            temp_obj["mdl_root_parent_name"] = "Parent Root";
                        }
                        
                        rtn_users_data.push(temp_obj);
                        next_mover();
                    }, function() {
                        resolve(rtn_users_data);                         
                    });
                } else {
                    resolve(rtn_users_data);                         
                }
            });

        } catch (e) {
            console.log(e);
            reject(e);
        }
    });

    result.then(function(rtn_users_data){
        res.render('admin/module_list',{"domain_url":config.domain, "pagename":"module", "ses_user" : req.session.user, "data":rtn_users_data });
    });
});

/**
 * MODULE : MODULE 
 * USE : MODULE EDIT VIEW
 * VIEW : ADMIN/MODULE_EDIT
 */
router.get('/edit/:id', chk_loginaccess, async (req, res) => {
    
    let user_id = req.params.id;
    var rtn_users_data = new Array();
    var roles_results;
    var users_reporting_results;

    const result = new Promise(async(resolve, reject) => {

        try {
            const permission=dbUtil.get().collection(config.tbl_permissions);
            let rtn_module_data = new Array();
                    
            if(user_id === undefined || user_id == "") {
                resolve(rtn_module_data);
            } else {
                let module_params = new Object();

                module_report.finddata(module_params,user_id,function(module_results){
                    if(module_results.length > 0) {
        
                        each(module_results, async (module_item, next_mover)=> {

                            let currentpermissions=[];
                            if(module_item.permissions){

                                const userpermit=module_item.permissions.map(async(datas)=>{
                                    let permissionsource_id= datas;
                                    let perm=await permission.findOne({"_id" :  permissionsource_id});
                                    return perm.label;
                                });


                                
                             currentpermissions=await Promise.all(userpermit);
                            }
                            

                    

                            let temp_obj = new Object();
                            temp_obj["id"] = module_item._id;
                            temp_obj["mdl_label"] = module_item.label;
                            temp_obj["mdl_field"] = module_item.field;
                            temp_obj["mdl_root_url"] = module_item.root_url;
                            temp_obj["mdl_root_parent_id"] = (module_item.root_parent_id).valueOf();
                            temp_obj["permissions"] =await permission.find({}).toArray();
                            temp_obj["currentpermission"]=currentpermissions;
                          


                            
                            rtn_module_data.push(temp_obj);
                            next_mover();
                        }, function() {
                            resolve(rtn_module_data);                         
                        });
                    } else {
                        resolve(rtn_module_data);                         
                    }   
                });
            }
        } catch (e) {
            console.log(e);
            reject(e);
        }
    });

    result.then(async (rtn_module_data) => {

        const tbl_module = dbUtil.get().collection(config.tbl_module);
        const modules = await tbl_module.find({"root_url":1}).toArray();

        let render_params = new Object();

        render_params["domain_url"] = config.domain;
        render_params["pagename"] = "module";
        render_params["data"] = rtn_module_data;
        render_params["ses_user"] = req.session.user;
        render_params["data_roles"] = roles_results;
        render_params["data_reporting"] = users_reporting_results;
        render_params["modules"] = modules;
        
        if(rtn_module_data.length > 0)
            res.render('admin/module_edit', render_params);
        else 
            res.render('errors/errors',{"domain_url":config.domain, "error_code":400, "error_mainmsg":"Something Went Wrong. Action id is required !"});
    });
});

/**
 * MODULE : MODULE 
 * USE : MODULE UPDATE
 */
router.post('/update/:id', chk_loginaccess, async (req, res) => {

    let user_id = req.params.id;
    var rtn_users_data = new Array();
    var roles_results;
    var users_reporting_results;

    const result = new Promise(async(resolve, reject) => {

        try {
            if(user_id === undefined || user_id == "") {
                resolve(false);
            } else {

                let mdl_update_obj = new Object();

                mdl_update_obj["label"] = req.body.m_label;
                mdl_update_obj["field"] = req.body.m_field;
                mdl_update_obj["root_url"] = parseInt(req.body.m_root);
                mdl_update_obj["root_parent_id"] = (req.body.m_root == 0 && req.body.m_parentroot_id != "") ? new mongo.ObjectID(req.body.m_parentroot_id) : "";
               
                if(typeof(req.body.permission)=='string') {
                    permitfield=[req.body.permission];
                } else {
                    permitfield=req.body.permission;
                }
    
    
                const permissions=dbUtil.get().collection(config.tbl_permissions);
                    //Getting PermissionId for selected permission        
                    const userpermit=permitfield.map(async(datas)=>{
                    let perm=await permissions.findOne({"label" : datas},{'_id':1});
                    return perm._id;
                });
                const userpermissions=await Promise.all(userpermit)
    
                mdl_update_obj["permissions"] = userpermissions;



                module_report.updatedata(user_id, mdl_update_obj, function(userupdate_collection){
                    if(userupdate_collection){
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                });
            }
        } catch (e) {
            console.log(e);
            reject(e);
        }
    });

    result.then(function(rtn_usersupdate_status){

        if(rtn_usersupdate_status) {
            req.flash('GOOD', 'Module Updated Successfully!', false);
            res.status(200).send({status:true, msg:"Module Updated Successfully!"});
        } else {
            req.flash('BAD', 'Somthing went wrong, try again!', false);
            res.status(204).send({status:false, msg:"Somthing went wrong!"});
        }
    });
});

/**
 * MODULE : MODULE 
 * USE : MODULE ADD VIEW
 * VIEW : ADMIN/MODULE_ADD
 */
router.get('/add', chk_loginaccess, async (req, res) => {

    const tbl_module = dbUtil.get().collection(config.tbl_module);
    const permission=dbUtil.get().collection(config.tbl_permissions);

    const permissions=await permission.find({}).toArray();
    const modules = await tbl_module.find({"root_url":1}).toArray();

    let render_params = new Object();
    render_params["domain_url"] = config.domain;
    render_params["pagename"] = "module";
    render_params["data"] = "";
    render_params["ses_user"] = req.session.user;
    render_params["modules"] = modules;
    render_params["permissions"] = permissions;
    

    res.render('admin/module_add', render_params);
});

/**
 * MODULE : MODULE 
 * USE : MODULE INSERT
 */
router.post('/create', chk_loginaccess, async (req, res) => {

    const result = new Promise(async(resolve, reject) => {

        try {
            let user_add_obj = new Object();

            user_add_obj["label"] = req.body.m_label;
            user_add_obj["field"] = req.body.m_field;
            user_add_obj["root_url"] = parseInt(req.body.m_root);
            user_add_obj["root_parent_id"] = (req.body.m_root == 0 && req.body.m_parentroot_id != "") ? new mongo.ObjectID(req.body.m_parentroot_id) : "";

            if(typeof(req.body.permission)=='string') {
                permitfield=[req.body.permission];
            } else {
                permitfield=req.body.permission;
            }


            const permissions=dbUtil.get().collection(config.tbl_permissions);
                //Getting PermissionId for selected permission        
                const userpermit=permitfield.map(async(datas)=>{
                let perm=await permissions.findOne({"label" : datas},{'_id':1});
                return perm._id;
            });
            const userpermissions=await Promise.all(userpermit)

            user_add_obj["permissions"] = userpermissions;

            module_report.insertdata(user_add_obj,function(user_add_collections){
                if(user_add_collections) 
                    resolve(true);
                else
                    resolve(false);
            });
        } catch (e) {
            console.log(e);
            reject(e);
        }
    });

    result.then(function(rtn_useradd_status){

        if(rtn_useradd_status) {
            req.flash('GOOD', 'Module added Successfully!', false);
            res.status(200).send({status:true, msg:"Module Added Successfully!"});
        } else {
            req.flash('BAD', 'Somthing went wrong, try again!', false);
            res.status(204).send({status:false, msg:"Somthing went wrong!"});
        }
    });
});

/**
 * MODULE : MODULE 
 * USE : MODULE DELETE
 */
router.post('/delete/:id', chk_loginaccess, async (req, res) => {

    let user_id = req.params.id;
    const result = new Promise(async(resolve, reject) => {

        try {
            if(user_id === undefined || user_id == "") {
                resolve(false);
            } else {
                let parms = new Object();

                module_report.deletedata(parms,user_id,function(user_remove_collections){
                
                    if(user_remove_collections) 
                        resolve(true);
                    else
                        resolve(false);
                });
            }
        } catch (e) {
            console.log(e);
            reject(e);
        }
    });

    result.then(function(rtn_useradd_status){

        if(rtn_useradd_status) {
            req.flash('GOOD', 'Module deleted Successfully!', false);
            res.status(200).send({status:true, msg:"Module deleted Successfully!"});
        } else {
            req.flash('BAD', 'Somthing went wrong, try again!', false);
            res.status(204).send({status:false, msg:"Somthing went wrong!"});
        }
    });
});

/**
 * MODULE : ROLES
 * USE : TO CHECK THE LABEL SHOULD BE ALREADY EXISTS OR NOT
 */
router.get('/chk_label_exists', chk_loginaccess, async (req, res) => {

    let u_userid = req.query.u_userid;
    let u_username = decodeURI(req.query.m_label);
    let u_username_lower = u_username.toLowerCase();

    let matcher = new Object();

    if(u_userid !== undefined && u_userid != "") {
        let pk_user_id = new mongo.ObjectID(u_userid);
        matcher["_id"] = {"$ne" : pk_user_id};
    }

    matcher["label"] = new RegExp(["^", u_username_lower.trim(), "$"].join(""), "i");
    console.log(matcher);
    dbUtil.get().collection(config.tbl_module).find(matcher).toArray(function(err, users_data) {

        if(users_data.length > 0){
            return res.status(200).send(false);
        } else {
            return res.status(200).send(true);
        }
    });    
});

/**
 * MODULE : ROLES
 * USE : TO CHECK THE FIELD SHOULD BE ALREADY EXISTS OR NOT
 */
router.get('/chk_field_exists', chk_loginaccess, async (req, res) => {

    let u_userid = req.query.u_userid;
    let u_username = decodeURI(req.query.m_field);
    let u_username_lower = u_username.toLowerCase();

    let matcher = new Object();

    if(u_userid !== undefined && u_userid != "") {
        let pk_user_id = new mongo.ObjectID(u_userid);
        matcher["_id"] = {"$ne" : pk_user_id};
    }
    matcher["field"] = new RegExp(["^", u_username_lower.trim(), "$"].join(""), "i");;

    dbUtil.get().collection(config.tbl_module).find(matcher).toArray(function(err, users_data) {

        if(users_data.length > 0){
            return res.status(200).send(false);
        } else {
            return res.status(200).send(true);
        }
    });
});

module.exports = router;