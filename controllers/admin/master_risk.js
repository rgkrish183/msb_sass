var config = require('../../config/database');
var dbUtil = require('../../config/dbUtil');
var express = require('express');
var router = express.Router();
var each = require('async-each');
var auth = require('../../middlewares/auth'), chk_loginaccess = auth.chk_loginaccess;
var common_functions = require('../../middlewares/common_functions');
var mongo = require('mongodb');
var module_report = require('../../models/admin/module');
var moment = require("moment");
var lodash = require('lodash');
var MODCONF = require('../../config/module_config');
const PRMS = MODCONF.PRMS;
const MDUL = MODCONF.MDUL;

/**
 * MODULE : MASTER RISK
 * USE : ONCHANGE EVENT LIST MASTER RISK DATA
 */
router.post('/commonrisk', chk_loginaccess, async(req, res, next)=>{

    try {

        let typeofrisk = req.body.risk_types;
        let session_organizationid = req.session.user.ses_organization;
        let session_userid = new mongo.ObjectID(req.session.user.ses_userid);
        
        if(typeofrisk != "" && typeofrisk !== undefined) {

            new Promise( async (resolve, reject) => {

                let tbl_risks;
                let risk_fields = new Object();
                
                //customertype
                if(config.master_risktypes[0][0] == typeofrisk) {
                    tbl_risks = config.tbl_customertype;
                    risk_fields["fld_label"] = "CUSTOMER BUSINESS";
                    risk_fields["fld_score"] = "CUSTOMER BUSINESS RISK SCORE";

                //modeofdelivery
                } else if(config.master_risktypes[1][0] == typeofrisk) {
                    tbl_risks = config.tbl_modeofdelivery;
                    risk_fields["fld_label"] = "Mode of Delivery";
                    risk_fields["fld_score"] = "Model of Delivery Score";

                //modeofpayment
                } else if(config.master_risktypes[2][0] == typeofrisk) {
                    tbl_risks = config.tbl_modeofpayment;
                    risk_fields["fld_label"] = "Mode of Payment";
                    risk_fields["fld_score"] = "Mode of Payment Risk Score";
                    
                //nationality
                } else if(config.master_risktypes[3][0] == typeofrisk) {
                    tbl_risks = config.tbl_nationality;
                    risk_fields["fld_label"] = "Nationality";
                    risk_fields["fld_score"] = "Nationality Risk Score";

                //occupation
                } else if(config.master_risktypes[4][0] == typeofrisk) {
                    tbl_risks = config.tbl_occupation;
                    risk_fields["fld_label"] = "Occupation";
                    risk_fields["fld_score"] = "Occupation Risk Score";

                //purpose
                } else if(config.master_risktypes[5][0] == typeofrisk) {
                    tbl_risks = config.tbl_purpose;
                    risk_fields["fld_label"] = "PURPOSE";
                    risk_fields["fld_score"] = "PURPOSE RISK SCORE";

                //source
                } else if(config.master_risktypes[6][0] == typeofrisk) {
                    tbl_risks = config.tbl_source;
                    risk_fields["fld_label"] = "SOURCE";
                    risk_fields["fld_score"] = "SOURCE RISK SCORE";

                //value
                } else if(config.master_risktypes[7][0] == typeofrisk) {
                    tbl_risks = config.tbl_value;
                    risk_fields["fld_label"] = "VALUE LEVEL";
                    risk_fields["fld_score"] = "VALUE RISK SCORE";
                }

                const collections_master_risk = dbUtil.get().collection(tbl_risks);
                const data_master_risk = await collections_master_risk.find({}).toArray();

                const data_master_riskscore_obj = new Object();
                data_master_riskscore_obj["risk_master_table"] = typeofrisk;
                data_master_riskscore_obj["risk_orgid"] =  new mongo.ObjectID(session_organizationid);
                
                const data_master_riskscore = await dbUtil.get().collection(config.tbl_master_riskscore).find(data_master_riskscore_obj).toArray();

                let conditions_returns = new Object();
                conditions_returns["risk_fields"] = risk_fields;
                conditions_returns["data_master_risk"] = data_master_risk;
                conditions_returns["data_master_riskscore"] = data_master_riskscore;
                
                resolve(conditions_returns)

            }).then(async(data_conditions_returns) => {

                let render_params_obj = new Object();
                render_params_obj["success"] = true;
                render_params_obj["typeofrisk"] = typeofrisk;
                render_params_obj["risk_fields"] = data_conditions_returns["risk_fields"];
                render_params_obj["data"] = data_conditions_returns["data_master_risk"];
                render_params_obj["data_master_riskscore"] = data_conditions_returns["data_master_riskscore"];

                res.status(200).json(render_params_obj);
            });
        }
    } catch(error) {
        console.log(error);
        res.json({"success":false, "typeofrisk": typeofrisk, "risk_fields":{}, "data":[]})
    }
});

/**
 * MODULE : MASTER RISK
 * USE : INSERT MASTER RISK DATA
 */
router.post('/update_commonrisk', chk_loginaccess, async(req, res, next)=>{

    try {
        let common_risktype = req.body.common_risktype;
        let common_riskdata = JSON.parse(req.body.common_riskdata);
        let session_organizationid =  new mongo.ObjectID(req.session.user.ses_organization);
        let session_userid = new mongo.ObjectID(req.session.user.ses_userid);
        
        if(common_risktype != "" && common_risktype !== undefined) {

            new Promise( async (resolve, reject) => {

                if(common_riskdata.length > 0){

                    const promiseall_riskdata = common_riskdata.map( async (riskdata) => {

                        if(riskdata.value != common_risktype) {

                            const collections_master_risk = dbUtil.get().collection(config.tbl_master_riskscore);

                            let risk_finder = new Object();
    
                            risk_finder["risk_master_table"] = common_risktype;
                            risk_finder["risk_orgid"] = new mongo.ObjectID(session_organizationid);
                            risk_finder["risk_id"] = new mongo.ObjectID(riskdata.name);
    
                            let risk_values = new Object();
    
                            risk_values["risk_master_table"] = common_risktype;
                            risk_values["risk_id"] = new mongo.ObjectID(riskdata.name);
                            risk_values["risk_point"] = parseInt(riskdata.value);
                            risk_values["risk_orgid"] = new mongo.ObjectID(session_organizationid);
                            risk_values["risk_lastupdated_by"] = session_userid;
                            risk_values["risk_lastupdated_date"] = moment().format();
                            
                            const data_master_risk = await collections_master_risk.update(risk_finder, risk_values, { upsert: true });
    
                            return data_master_risk;
                        } else {
                            return true;
                        }

                    });
                    const results = await Promise.all(promiseall_riskdata)
                    resolve(results)
                } else {
                    resolve([]);
                }

            }).then(async(data_conditions_returns) => {

                if(data_conditions_returns.length > 0) {
                    req.flash('GOOD', 'Master risk saved Successfully!', false);
                    res.status(200).send({status:true, msg:"Master risk saved Successfully!"});
                } else {
                    req.flash('BAD', 'Somthing went wrong, try again!', false);
                    res.status(204).send({status:false, msg:"Somthing went wrong!"});
                }
            });
        }
    } catch(error) {
        console.log(error);
        res.json({"status":false, msg: error})
    }
});


module.exports = router;