var config = require('../../config/database');
var dbUtil = require('../../config/dbUtil');
var express = require('express');
var router = express.Router();
var each = require('async-each');
var auth = require('../../middlewares/auth'), chk_loginaccess = auth.chk_loginaccess;
var common_functions = require('../../middlewares/common_functions');
var mongo = require('mongodb');
var module_report = require('../../models/admin/roles');

/**
 * MODULE : ROLES 
 * USE : ROLES GET LIST
 * VIEW : ADMIN/ROLES_LIST
 */
router.get('/', chk_loginaccess, async (req, res) => {

    const result = new Promise(async(resolve, reject) => {

        try {
            let module_params = new Object();

            module_report.getalldata(module_params, function(module_results){

                let rtn_users_data = new Array();
          
                if(module_results.length > 0) {
            
                    each(module_results, function (module_items, next_mover) {

                        let temp_obj = new Object();
                        temp_obj["id"] = module_items._id;
                        temp_obj["mdl_label"] = module_items.label;
                        temp_obj["mdl_field"] = module_items.field;
                        
                        rtn_users_data.push(temp_obj);
                        next_mover();
                    }, function() {
                        resolve(rtn_users_data);                         
                    });
                } else {
                    resolve(rtn_users_data);                         
                }
            });

        } catch (e) {
            console.log(e);
            reject(e);
        }
    });
    result.then(function(rtn_users_data){
        res.render('admin/roles_list',{"domain_url":config.domain, "pagename":"roles", "ses_user" : req.session.user, "data":rtn_users_data });
    });

});

/**
 * MODULE : ROLES 
 * USE : ROLES TO VIEW EDIT PAGE
 * VIEW : ADMIN/ROLES_EDIT
 */
router.get('/edit/:id', chk_loginaccess, async (req, res) => {
    
    let user_id = req.params.id;    
    var rtn_users_data = new Array();
    var roles_results;
    var users_reporting_results;

    const result = new Promise(async(resolve, reject) => {

        try {
            let rtn_module_data = new Array();
            console.log(user_id === undefined || user_id == "");

            if(user_id === undefined || user_id == "") {
                resolve(rtn_module_data);
            } else {
                let module_params = new Object();

                 module_report.finddata(module_params,user_id,function(module_results){
                    if(module_results.length > 0) {
        
                        each(module_results, function (module_item, next_mover) {

                            let temp_obj = new Object();
                            temp_obj["id"] = module_item._id;
                            temp_obj["mdl_label"] = module_item.label;
                            temp_obj["mdl_field"] = module_item.field;
                            
                            rtn_module_data.push(temp_obj);
                            next_mover();
                        }, function() {
                            resolve(rtn_module_data);                         
                        });
                    } else {
                        resolve(rtn_module_data);                         
                    }   
                });
            }
        } catch (e) {
            console.log(e);
            reject(e);
        }
    });

    result.then(function(rtn_module_data){
        if(rtn_module_data.length > 0)
            res.render('admin/roles_edit',{"domain_url":config.domain, "pagename":"roles", "data":rtn_module_data, "ses_user" : req.session.user, "data_roles" : roles_results, "data_reporting" : users_reporting_results });
        else 
            res.render('errors/errors',{"domain_url":config.domain, "error_code":400, "error_mainmsg":"Something Went Wrong. Action id is required !", });
    });

});

/**
 * MODULE : ROLES 
 * USE : ROLES UPDATE
 */
router.post('/update/:id', chk_loginaccess, async (req, res) => {

    let user_id = req.params.id;
    
    var rtn_users_data = new Array();
    var roles_results;
    var users_reporting_results;

    const result = new Promise(async(resolve, reject) => {

        try {
            if(user_id === undefined || user_id == "") {
                resolve(false);
            } else {

                let mdl_update_obj = new Object();

                mdl_update_obj["label"] = req.body.m_label;
                mdl_update_obj["field"] = req.body.m_field;
                
                module_report.updatedata(user_id, mdl_update_obj, function(userupdate_collection){
                    
                    if(userupdate_collection){
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                });
            }
        } catch (e) {
            console.log(e);
            reject(e);
        }
    });
    result.then(function(rtn_usersupdate_status){

        if(rtn_usersupdate_status) {
            req.flash('GOOD', 'Roles Updated Successfully!', false);
            res.status(200).send({status:true, msg:"Roles Updated Successfully!"});
        } else {
            req.flash('BAD', 'Somthing went wrong, Try again!', false);
            res.status(204).send({status:false, msg:"Somthing went wrong!"});
        } 
    });
});

/**
 * MODULE : ROLES 
 * USE : ROLES TO ADD VIEW PAGE
 * VIEW : ADMIN/ROLES_ADD
 */
router.get('/add', chk_loginaccess, async (req, res) => {

    const result = new Promise(async(resolve, reject) => {

        try {
            let module_params = new Object();

            module_report.getalldata(module_params, function(module_results){

                let rtn_users_data = new Array();
          
                if(module_results.length > 0) {
            
                    each(module_results, function (module_items, next_mover) {

                        let temp_obj = new Object();
                        temp_obj["id"] = module_items._id;
                        temp_obj["md_label"] = module_items.label;
                        temp_obj["md_field"] = module_items.field;
                        temp_obj["md_parent_role_position"] = module_items.parent_role_position;
                        
                        rtn_users_data.push(temp_obj);
                        next_mover();
                    }, function() {
                        resolve(rtn_users_data);                         
                    });
                } else {
                    resolve(rtn_users_data);                         
                }
            });

        } catch (e) {
            console.log(e);
            reject(e);
        }
    });


result.then((result)=>{
     res.render('admin/roles_add',{"domain_url":config.domain, "pagename":"roles", "data":result, "ses_user" : req.session.user });

}).catch((error)=>{
    res.render('admin/roles_add',{"domain_url":config.domain, "pagename":"roles", "data":[], "ses_user" : req.session.user });
    
})


   });

/**
 * MODULE : ROLES 
 * USE : ROLES INSERT
 */
router.post('/create', chk_loginaccess, async (req, res) => {

    const result = new Promise(async(resolve, reject) => {

        try {
            let user_add_obj = new Object();

            user_add_obj["label"] = req.body.m_label;
            user_add_obj["field"] = req.body.m_field;
            user_add_obj["parent_role_position"] = parseInt(req.body.m_parentrole) + 1 ;

            module_report.insertdata(user_add_obj,function(user_add_collections){
                if(user_add_collections) 
                    resolve(true);
                else
                    resolve(false);
            });
        } catch (e) {
            console.log(e);
            reject(e);
        }
    });

    result.then(function(rtn_useradd_status){
        
        if(rtn_useradd_status) {
            req.flash('GOOD', 'Roles added Successfully!', false);
            res.status(200).send({status:true, msg:"Roles Added Successfully!"});
        } else {
            req.flash('BAD', 'Somthing went wrong, try again!', false);
            res.status(204).send({status:false, msg:"Somthing went wrong!"});
        }
    });
});

/**
 * MODULE : ROLES 
 * USE : ROLES DELETE
 */
router.post('/delete/:id', chk_loginaccess, async (req, res) => {

    const result = new Promise(async(resolve, reject) => {

        try {

            if(user_id === undefined || user_id == "") {
                resolve(false);
            } else {
                let parms = new Object();

                module_report.deletedata(parms,user_id,function(user_remove_collections){
                
                    if(user_remove_collections) 
                        resolve(true);
                    else
                        resolve(false);
                });
            }
        } catch (e) {
            console.log(e);
            reject(e);
        }
    });

    result.then(function(rtn_useradd_status){

        if(rtn_useradd_status) {
            req.flash('GOOD', 'Roles deleted Successfully!', false);
            res.status(200).send({status:true, msg:"Roles deleted Successfully!"});
        } else {
            req.flash('BAD', 'Somthing went wrong, try again!', false);
            res.status(204).send({status:false, msg:"Somthing went wrong!"});
        } 
    });

});

/**
 * MODULE : ROLES
 * USE : TO CHECK THE LABEL SHOULD BE ALREADY EXISTS OR NOT
 */
router.get('/chk_label_exists', chk_loginaccess, async (req, res) => {

    let u_userid = req.query.u_userid;
    let u_username = decodeURI(req.query.m_label);
    let u_username_lower = u_username.toLowerCase();

    let matcher = new Object();

    if(u_userid !== undefined && u_userid != "") {
        let pk_user_id = new mongo.ObjectID(u_userid);
        matcher["_id"] = {"$ne" : pk_user_id};
    }
    matcher["label"] = new RegExp(["^", u_username_lower.trim(), "$"].join(""), "i");

    dbUtil.get().collection(config.tbl_roles).find(matcher).toArray(function(err, users_data) {

        if(users_data.length > 0){
            return res.status(200).send(false);
        } else {
            return res.status(200).send(true);
        }
    });    
});

/**
 * MODULE : ROLES
 * USE : TO CHECK THE FIELD SHOULD BE ALREADY EXISTS OR NOT
 */
router.get('/chk_field_exists', chk_loginaccess, async (req, res) => {

    let u_userid = req.query.u_userid;
    let u_username = decodeURI(req.query.m_field);
    let u_username_lower = u_username.toLowerCase();

    let matcher = new Object();

    if(u_userid !== undefined && u_userid != "") {
        let pk_user_id = new mongo.ObjectID(u_userid);
        matcher["_id"] = {"$ne" : pk_user_id};
    }
    matcher["field"] = new RegExp(["^", u_username_lower.trim(), "$"].join(""), "i");

    dbUtil.get().collection(config.tbl_roles).find(matcher).toArray(function(err, users_data) {

        if(users_data.length > 0){
            return res.status(200).send(false);
        } else {
            return res.status(200).send(true);
        }
    });
});

module.exports = router;