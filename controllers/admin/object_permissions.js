var express = require('express');
var router = express.Router();
var dbUtil = require('../../config/dbUtil');
var async=require('async');
var each = require('async-each');
var mongo = require('mongodb');
var config = require('../../config/database');
var auth = require('../../middlewares/auth'), chk_loginaccess = auth.chk_loginaccess;
var common_functions = require('../../middlewares/common_functions');
var MODCONF = require('../../config/module_config');
const PRMS = MODCONF.PRMS;
const MDUL = MODCONF.MDUL;
var lodash=require('lodash');
var module_report = require('../../models/admin/roles');

/**
 * MODULE : OBJECT PERMISSIONS
 * USE : TO VIEW MODEL PERMISIONS LIST
 * VIEW : ADMIN/OBJECT_PERMISSIONS_GET 
 */
router.get('/', chk_loginaccess, async(req,res)=>{

    let rbac_obj = res.locals.rbac_obj;

    // URL VALIDATIONS
    let rbac_urls = new Object();
    rbac_urls["rbac_obj"] = rbac_obj;
    rbac_urls["roles"] = req.session.user.ses_rolename;
    rbac_urls["actions"] = PRMS["PER_URL"];
    rbac_urls["url"] = MDUL["MODURL_MODULEPERMISSIONS"];
    rbac_urls["usertype"] = req.session.user.ses_usertype;

    var url_validation = await common_functions.go_modules_rbacvalidations(rbac_urls);

    if(url_validation) {

        //OBJECT PERMISSIOIN LISTVIEW 
        let rbac1 = new Object();
        rbac1["rbac_obj"] = rbac_obj;
        rbac1["roles"] = req.session.user.ses_rolename;
        rbac1["actions"] = PRMS["PER_LISTVIEW"];
        rbac1["url"] = MDUL["MODURL_MODULEPERMISSIONS"];
        rbac1["usertype"] = req.session.user.ses_usertype;
        let report_list = await common_functions.go_modules_rbacvalidations(rbac1);

        //OBJECT PERMISSIOIN ADD 
        let rbac2 = new Object();
        rbac2["rbac_obj"] = rbac_obj;
        rbac2["roles"] = req.session.user.ses_rolename;
        rbac2["actions"] = PRMS["PER_ADD"];
        rbac2["url"] = MDUL["MODURL_MODULEPERMISSIONS"];
        rbac2["usertype"] = req.session.user.ses_usertype;
        let report_add = await common_functions.go_modules_rbacvalidations(rbac2);

        //OBJECT PERMISSIOIN EDIT 
        let rbac3 = new Object();
        rbac3["rbac_obj"] = rbac_obj;
        rbac3["roles"] = req.session.user.ses_rolename;
        rbac3["actions"] = PRMS["PER_EDIT"];
        rbac3["url"] = MDUL["MODURL_MODULEPERMISSIONS"];
        rbac3["usertype"] = req.session.user.ses_usertype;
        let report_edit = await common_functions.go_modules_rbacvalidations(rbac3);

        //OBJECT PERMISSIOIN DELETE 
        let rbac4 = new Object();
        rbac4["rbac_obj"] = rbac_obj;
        rbac4["roles"] = req.session.user.ses_rolename;
        rbac4["actions"] = PRMS["PER_DELETE"];
        rbac4["url"] = MDUL["MODURL_MODULEPERMISSIONS"];
        rbac4["usertype"] = req.session.user.ses_usertype;
        let report_delete = await common_functions.go_modules_rbacvalidations(rbac4);
        
        const result=new Promise(async(resolve,reject)=>{
            try {

                const role = dbUtil.get().collection(config.tbl_roles);
                const permission = dbUtil.get().collection(config.tbl_permissions);
                const module_permissions=dbUtil.get().collection(config.tbl_object_permissions);

                let finder = new Object();

                if(req.session.user.ses_usertype == config.usertype_access.superadmin) {
                    var usertype = config.usertype_access.admin;
                    finder["parent_role_position"] = usertype;
                } else if(req.session.user.ses_usertype == config.usertype_access.admin) {
                        var usertype = config.usertype_access.system_admin;
                        finder["parent_role_position"] = usertype;
                } else {
                    var usertype = req.session.user.ses_usertype;
                    finder["parent_role_position"] = {$gt : usertype};
                }

                let module_results = await role.find(finder).toArray();
                let rtn_users_data = new Array();
                
                if(module_results.length > 0) {
            
                    each(module_results, async (module_items, next_mover) => {

                        let temp_obj = new Object();
                        temp_obj["id"] = module_items._id;
                        temp_obj["label"] = module_items.label;
                        temp_obj["field"] = module_items.field;

                        console.log(module_items._id.toString());

                        var permission_checker = await module_permissions.find({"role": new mongo.ObjectId(module_items._id.toString())}).toArray();
                        temp_obj["current_action"] = (permission_checker.length > 0) ? "update" : "create";
                        
                        rtn_users_data.push(temp_obj);
                        next_mover();
                    }, function() {
                        resolve(rtn_users_data);                         
                    });
                } else {
                    resolve(rtn_users_data);                         
                }

            } catch(e) {
                res.send(e.message);
            }
        });
        
        // URL PARAMS
        var urlquery_request = new Object();
        urlquery_request["fromdate"] = req.query.fromdate;
        urlquery_request["todate"] = req.query.todate;

        // RENDER PAGE PARAMS 
        let render_params_obj = new Object();
        render_params_obj["pagename"] = "dashboard";
        render_params_obj["domain_url"] = config.domain;
        render_params_obj["ses_user"] = req.session.user;
        render_params_obj["urlquery_request"] = urlquery_request;
        render_params_obj["pos_objectperms"] = {"report_list":report_list, "report_add":report_add, "report_edit":report_edit, "report_delete":report_delete};
        
        result.then(function(rtn_users_data){
            render_params_obj["operms"] = rtn_users_data;
            res.render('admin/object_permissions_get', render_params_obj);
        });    

    } else {
        res.render('errors/errors',{"domain_url":config.domain, "error_code":401, "error_mainmsg":"Module Permission Denied !"});
    }
});

/**
 * MODULE : OBJECT PERMISSIONS
 * USE : TO VIEW MODEL PERMISIONS INSERT
 * VIEW : ADMIN/OBJECT_PERMISSIONS_CREATE 
 */
router.get('/create/get/:id', chk_loginaccess, async(req,res)=>{
   
    let rbac_obj = res.locals.rbac_obj;
    
    let rbac2 = new Object();
    rbac2["rbac_obj"] = rbac_obj;
    rbac2["roles"] = req.session.user.ses_rolename;
    rbac2["actions"] = PRMS["PER_ADD"];
    rbac2["url"] = MDUL["MODURL_MODULEPERMISSIONS"];
    rbac2["usertype"] = req.session.user.ses_usertype;
    let report_add = await common_functions.go_modules_rbacvalidations(rbac2);

    if(report_add) {
        const result=new Promise(async(resolve,reject)=>{
            try {
        
                const role=dbUtil.get().collection(config.tbl_roles);
                const modulle = dbUtil.get().collection(config.tbl_module);
                const permission=dbUtil.get().collection(config.tbl_permissions);
        
                let finder = new Object();
                finder["_id"] = new mongo.ObjectId(req.params.id);
                const roles = await role.findOne(finder);

                // let ses_roleid = new mongo.ObjectID(req.session.user.ses_role);
                // const  parent_position =await role.findOne({"_id":ses_roleid});
                // const  roles = await role.find({ parent_role_position: { $gt:  parent_position.parent_role_position } }).toArray();
               
                const header=await permission.find({}).toArray();
        
                const module_permissions=await modulle.aggregate( [
                    { $lookup: { from: "tbl_module", localField: '_id', foreignField: 'root_parent_id', as: 'object' } },
                    { $match: { root_url : 1} },{ $unwind: { path: "$object", preserveNullAndEmptyArrays: true } }
                ] ).toArray();
               
                const result=module_permissions.map(async(data)=>{
        
                    let user=new Object();
                    if(data.object){
        
                        user['parentmodule']= data.label;
                        user['childmodule']= data.object.label;
                        user['childmoduleid']=data.object._id;
                        user['parentmoduleid']=data._id;
                        user['field']=data.object.field;
                        if(data.object.permissions){
                            if(data.object.permissions.length>0){
                                let permissn=data.object.permissions.map(async(data1)=>{
                                    return await permission.findOne({'_id':data1});
                                });
                                user['permissions']=await Promise.all(permissn)
                            }  
                        }
                        return user;
                    } else {

                        var page_validator = true;

                        //BLOCKING USERS PAGE FOR SYSTEM ADMIN, BM(USERS), SUPERVISOR, TELLERS
                        var chk_sysadmin_login_usersmodule = (req.session.user.ses_usertype == config.usertype_access.system_admin && data.label == "USERS") ? true : false;
                        var chk_users_login_usersmodule = (req.session.user.ses_usertype == config.usertype_access.users && data.label == "USERS") ? true : false;
                        var chk_supervisors_login_usersmodule = (req.session.user.ses_usertype == config.usertype_access.supervisors && data.label == "USERS") ? true : false;
                        var chk_tellers_login_usersmodule = (req.session.user.ses_usertype == config.usertype_access.tellers && data.label == "USERS") ? true : false;

                        //BLOCKING MOUDLE PERMISSION PAGE FOR SYSTEM ADMIN, BM(USERS), SUPERVISOR, TELLERS
                        var chk_sysadmin_login_modulepermssion = (req.session.user.ses_usertype == config.usertype_access.system_admin && data.label == "MODULE PERMISSION") ? true : false;
                        var chk_users_login_modulepermssion = (req.session.user.ses_usertype == config.usertype_access.users && data.label == "MODULE PERMISSION") ? true : false;
                        var chk_supervisors_login_modulepermssion = (req.session.user.ses_usertype == config.usertype_access.supervisors && data.label == "MODULE PERMISSION") ? true : false;
                        var chk_tellers_login_modulepermssion = (req.session.user.ses_usertype == config.usertype_access.tellers && data.label == "MODULE PERMISSION") ? true : false;

                        //BLOCKING MASTER RISK PAGE FOR SYSTEM ADMIN, BM(USERS), SUPERVISOR, TELLERS
                        var chk_sysadmin_login_masterrisk = (req.session.user.ses_usertype == config.usertype_access.system_admin && data.label == "MASTER RISK") ? true : false;
                        var chk_users_login_masterrisk = (req.session.user.ses_usertype == config.usertype_access.users && data.label == "MASTER RISK") ? true : false;
                        var chk_supervisors_login_masterrisk = (req.session.user.ses_usertype == config.usertype_access.supervisors && data.label == "MASTER RISK") ? true : false;
                        var chk_tellers_login_masterrisk = (req.session.user.ses_usertype == config.usertype_access.tellers && data.label == "MASTER RISK") ? true : false;

                        if(chk_sysadmin_login_usersmodule || chk_users_login_usersmodule || chk_supervisors_login_usersmodule || chk_tellers_login_usersmodule){
                            page_validator = false;
                        } else if(chk_sysadmin_login_modulepermssion || chk_users_login_modulepermssion || chk_supervisors_login_modulepermssion || chk_tellers_login_modulepermssion) {
                            page_validator = false;
                        } else if(chk_sysadmin_login_masterrisk || chk_users_login_masterrisk || chk_supervisors_login_masterrisk || chk_tellers_login_masterrisk) {
                            page_validator = false;
                        }

                        if(page_validator) {
                            user['field']= data.field;
                            user['parentmodule']=data.label;
                            user['childmodule']=data.label;
                            user['childmoduleid']=data._id;
                            user['parentmoduleid']=data._id;
            
                            if(data.permissions){
                                if(data.permissions.length>0){
                                    let permissn=data.permissions.map(async(data1)=>{
                                        return await permission.findOne({'_id':data1});
                                    });
                                    user['permissions']=await Promise.all(permissn)
                                }  
                            }
                            return user;
                        } else {
                            return null;
                        }
                    }
                });
                const results=await Promise.all(result);
                var result_ender = results.filter(function(n){ return n != null }); 
               
                resolve({'moduledata':result_ender,"header":header,"role":roles})
            } catch(e) {
               reject(e.message)
            }
        });

        result.then(async(data)=>{ 
        
            // URL PARAMS
            var urlquery_request = new Object();
            urlquery_request["fromdate"] = req.query.fromdate;
            urlquery_request["todate"] = req.query.todate;
        
            // RENDER PAGE PARAMS 
            let render_params_obj = new Object();
            render_params_obj["pagename"] = "objectPermissions";
            render_params_obj["domain_url"] = config.domain;
            render_params_obj["ses_user"] = req.session.user;
            render_params_obj["urlquery_request"] = urlquery_request;
            render_params_obj["users"]=data;
            render_params_obj["roleid"] = req.params.id;
        
            res.render('./admin/object_permissions_create', render_params_obj);

        }).catch((e)=>res.send(e.message));
    } else {
        res.render('errors/errors',{"domain_url":config.domain, "error_code":401, "error_mainmsg":"Module Permission Denied !"});
    }
});

/**
 * MODULE : OBJECT PERMISSIONS
 * USE : TO INSERT MODULE PERMISSIONS
 */
router.post('/creates', chk_loginaccess, async (req,res) => {

    let usertype=req.session.user.ses_usertype;
    let organizationid=req.session.user.ses_organization;
    
    let rbac_obj = res.locals.rbac_obj;
      
    //OBJECT PERMISSIOIN ADD 
    let rbac2 = new Object();
    rbac2["rbac_obj"] = rbac_obj;
    rbac2["roles"] = req.session.user.ses_rolename;
    rbac2["actions"] = PRMS["PER_ADD"];
    rbac2["url"] = MDUL["MODURL_MODULEPERMISSIONS"];
    rbac2["usertype"] = req.session.user.ses_usertype;
    let report_add = await common_functions.go_modules_rbacvalidations(rbac2);

    if(report_add) {

        const objectrole = (data)=> {

            try { 
                return new Promise(async(resolve,reject)=>{

                    const role=dbUtil.get().collection(config.tbl_roles);
                    const  objects = dbUtil.get().collection(config.tbl_module);
                    const permissions=dbUtil.get().collection(config.tbl_permissions);
                    const module_permissions=dbUtil.get().collection(config.tbl_object_permissions);

                    const allpermissions=await permissions.find({}).toArray();

                    const modulefield=JSON.parse(req.body.table);
                    const urlrole_data = await role.findOne({'_id':new mongo.ObjectId(req.body.role)},{'_id':1, 'parent_role_position':1})

                    async.each(modulefield, async(modules, callback)=> {
                        
                        let module_permissions_obj = new Object();
                        module_permissions_obj["role"] = new mongo.ObjectId(req.body.role);
                        module_permissions_obj["object"] =new mongo.ObjectId(modules.childmoduleid);
                        module_permissions_obj["parent_object_id"] = new mongo.ObjectId(modules.parentmoduleid);

                        if(req.session.user.ses_usertype== config.usertype_access.superadmin){
                            module_permissions_obj["organizationid"] = organizationid;
                        } else {
                            module_permissions_obj["organizationid"] = new mongo.ObjectId(organizationid);
                        }
                        
                        let permissionid=[];
                        allpermissions.forEach((data)=>{
                            const resultt=lodash.get(modules, [data.label])
                            if(resultt==true || resultt=="true"){
                                permissionid.push(data._id)
                            }
                        });

                        module_permissions_obj["permissions"] = permissionid;
                        module_permissions_obj["usertype"] = urlrole_data.parent_role_position;
                        const insert = await module_permissions.insertOne(module_permissions_obj);

                        callback(insert);

                    },function(err) {
                        resolve({"success":true,"result":[]});
                    });
                });
            } catch(error) {
                console.log(error.message);
                res.send({"success":false,"error":error.message});
            }
        }
        objectrole(req.body).then((data)=>{

            if(data.success) {
                req.flash('GOOD', 'Module Permissions Added Successfully!', false);
                res.status(200).send({status:true, msg:"Module Permissions Added Successfully!"});
            } else {
                req.flash('BAD', 'Somthing went wrong, Try again!', false);
                res.status(204).send({status:false, msg:"Somthing went wrong!"});
            }
        }).catch((error)=>{
            res.render('errors/errors',{"domain_url":config.domain, "error_code":401, "error_mainmsg":error.message});
        });
    } else {
        res.render('errors/errors',{"domain_url":config.domain, "error_code":401, "error_mainmsg":"Module Permission Denied !"});
    }        
});
  
/**
 * MODULE : OBJECT PERMISSIONS
 * USE : TO VIEW MODEL PERMISIONS UPDATE
 * VIEW : ADMIN/OBJECT_PERMISSIONS_UPDATE 
 */
router.get('/update/get/:id', chk_loginaccess, async(req,res)=>{
   
    let rbac_obj = res.locals.rbac_obj;
    
    let rbac2 = new Object();
    rbac2["rbac_obj"] = rbac_obj;
    rbac2["roles"] = req.session.user.ses_rolename;
    rbac2["actions"] = PRMS["PER_ADD"];
    rbac2["url"] = MDUL["MODURL_MODULEPERMISSIONS"];
    rbac2["usertype"] = req.session.user.ses_usertype;
    let report_add = await common_functions.go_modules_rbacvalidations(rbac2);

    if(report_add) {

        const result=new Promise(async(resolve,reject)=>{

            try {
        
                const role=dbUtil.get().collection(config.tbl_roles);
                const modulle = dbUtil.get().collection(config.tbl_module);
                const permission=dbUtil.get().collection(config.tbl_permissions);
                const module_permissions_table=dbUtil.get().collection(config.tbl_object_permissions);
        
                // let ses_roleid = new mongo.ObjectId(req.params.id);
                // const  parent_position =await role.findOne({"_id":ses_roleid});
                // const  roles = await role.find({ parent_role_position: { $gt:  parent_position.parent_role_position } }).toArray();
                let finder = new Object();
                finder["_id"] = new mongo.ObjectId(req.params.id);
                const roles = await role.findOne(finder);
               
                const header=await permission.find({}).toArray();

                if(req.session.user.ses_usertype == config.usertype_access.superadmin) {
                    var new_organizationid= parseInt(req.session.user.ses_organization);
                } else {
                    var new_organizationid = new mongo.ObjectId(req.session.user.ses_organization);
                }                

                const module_permissions=await modulle.aggregate( [
                    { $lookup: { from: "tbl_module", localField: '_id', foreignField: 'root_parent_id', as: 'object' } }, 
                    { $match: { root_url : 1} },{ $unwind: { path: "$object", preserveNullAndEmptyArrays: true } }
                ] ).toArray();
               
                const result=module_permissions.map(async(data)=>{
        

                    if(data.object) {
                        let user=new Object();

                        user['parentmodule']= data.label;
                        user['childmodule']= data.object.label;
                        user['childmoduleid']=data.object._id;
                        user['parentmoduleid']=data._id;
                        user['field']=data.object.field;
                        if(data.object.permissions){
                            if(data.object.permissions.length>0){
                                let permissn=data.object.permissions.map(async(data1)=>{
                                    return await permission.findOne({'_id':data1});
                                });
                                user['permissions']=await Promise.all(permissn)
                            } else{
                                user['permissions']=[];
                            } 
                        }
                        user['selected_permissions']=await module_permissions_table.findOne({"parent_object_id": new mongo.ObjectId(data._id),"organizationid": new_organizationid, "usertype" : roles.parent_role_position,"object": new mongo.ObjectID(data.object._id),"role":new mongo.ObjectID(req.params.id)},{"permissions":1})
                        return user;
                        
                    } else {

                        var page_validator = true;

                        //BLOCKING USERS PAGE FOR SYSTEM ADMIN, BM(USERS), SUPERVISOR, TELLERS
                        var chk_sysadmin_login_usersmodule = (req.session.user.ses_usertype == config.usertype_access.system_admin && data.label == "USERS") ? true : false;
                        var chk_users_login_usersmodule = (req.session.user.ses_usertype == config.usertype_access.users && data.label == "USERS") ? true : false;
                        var chk_supervisors_login_usersmodule = (req.session.user.ses_usertype == config.usertype_access.supervisors && data.label == "USERS") ? true : false;
                        var chk_tellers_login_usersmodule = (req.session.user.ses_usertype == config.usertype_access.tellers && data.label == "USERS") ? true : false;

                        //BLOCKING MOUDLE PERMISSION PAGE FOR SYSTEM ADMIN, BM(USERS), SUPERVISOR, TELLERS
                        var chk_sysadmin_login_modulepermssion = (req.session.user.ses_usertype == config.usertype_access.system_admin && data.label == "MODULE PERMISSION") ? true : false;
                        var chk_users_login_modulepermssion = (req.session.user.ses_usertype == config.usertype_access.users && data.label == "MODULE PERMISSION") ? true : false;
                        var chk_supervisors_login_modulepermssion = (req.session.user.ses_usertype == config.usertype_access.supervisors && data.label == "MODULE PERMISSION") ? true : false;
                        var chk_tellers_login_modulepermssion = (req.session.user.ses_usertype == config.usertype_access.tellers && data.label == "MODULE PERMISSION") ? true : false;

                        //BLOCKING MASTER RISK PAGE FOR SYSTEM ADMIN, BM(USERS), SUPERVISOR, TELLERS
                        var chk_sysadmin_login_masterrisk = (req.session.user.ses_usertype == config.usertype_access.system_admin && data.label == "MASTER RISK") ? true : false;
                        var chk_users_login_masterrisk = (req.session.user.ses_usertype == config.usertype_access.users && data.label == "MASTER RISK") ? true : false;
                        var chk_supervisors_login_masterrisk = (req.session.user.ses_usertype == config.usertype_access.supervisors && data.label == "MASTER RISK") ? true : false;
                        var chk_tellers_login_masterrisk = (req.session.user.ses_usertype == config.usertype_access.tellers && data.label == "MASTER RISK") ? true : false;
                        
                        if(chk_sysadmin_login_usersmodule || chk_users_login_usersmodule || chk_supervisors_login_usersmodule || chk_tellers_login_usersmodule){
                            page_validator = false;
                        } else if(chk_sysadmin_login_modulepermssion || chk_users_login_modulepermssion || chk_supervisors_login_modulepermssion || chk_tellers_login_modulepermssion) {
                            page_validator = false;
                        } else if(chk_sysadmin_login_masterrisk || chk_users_login_masterrisk || chk_supervisors_login_masterrisk || chk_tellers_login_masterrisk) {
                            page_validator = false;
                        }

                        if(page_validator) {                        
                            let user_else=new Object();
                        
                            user_else['field']= data.field;
                            user_else['parentmodule']=data.label;
                            user_else['childmodule']=data.label;
                            user_else['childmoduleid']=data._id;
                            user_else['parentmoduleid']=data._id;
            
                            if(data.permissions){
                                if(data.permissions.length>0){
                                    let permissn=data.permissions.map(async(data1)=>{
                                        return await permission.findOne({'_id':data1});
                                    });
                                    user_else['permissions']=await Promise.all(permissn)
                                }  
                                else{
                                    user_else['permissions']=[];
                                }
                            }
                            user_else['selected_permissions']=await module_permissions_table.findOne({"parent_object_id": new mongo.ObjectId(data._id),"organizationid": new_organizationid, "usertype" : roles.parent_role_position,"object":new mongo.ObjectID(data._id)},{"permissions":1})
                            return user_else;
                        } else {
                            return null;
                        }
                    }
                   
                });
                console.log(result);
                const results=await Promise.all(result);
                var result_ender = results.filter(function(n){ return n != null }); 
                resolve({'moduledata':result_ender,"header":header,"role":roles})
                //   res.send(results);
            } catch(e) {
               reject(e.message);
            }
        });
        result.then(async(data)=>{ 

            // URL PARAMS
            var urlquery_request = new Object();
            urlquery_request["fromdate"] = req.query.fromdate;
            urlquery_request["todate"] = req.query.todate;
        
            // RENDER PAGE PARAMS 
            let render_params_obj = new Object();
            render_params_obj["pagename"] = "objectPermissions";
            render_params_obj["domain_url"] = config.domain;
            render_params_obj["ses_user"] = req.session.user;
            render_params_obj["urlquery_request"] = urlquery_request;
            render_params_obj["users"]=data;
            render_params_obj["roleid"] = req.params.id;

            res.render('./admin/object_permissions_update', render_params_obj);
        }).catch((e)=>res.send(e.message));
    } else {
        res.render('errors/errors',{"domain_url":config.domain, "error_code":401, "error_mainmsg":"Module Permission Denied !"});
    }
});

/**
 * MODULE : OBJECT PERMISSIONS
 * USE : TO UPDATE OBJECT PERMISSIONS 
 */
router.post('/update', chk_loginaccess, async(req,res)=>{

    let rbac_obj = res.locals.rbac_obj;
   
    //OBJECT PERMISSIOIN EDIT 
    let rbac3 = new Object();
    rbac3["rbac_obj"] = rbac_obj;
    rbac3["roles"] = req.session.user.ses_rolename;
    rbac3["actions"] = PRMS["PER_EDIT"];
    rbac3["url"] = MDUL["MODURL_MODULEPERMISSIONS"];
    rbac3["usertype"] = req.session.user.ses_usertype;
    let report_edit = await common_functions.go_modules_rbacvalidations(rbac3);

    if(report_edit) {

        const updaterole=async(data)=>{

            return new Promise(async(resolve,reject)=>{

                const role=dbUtil.get().collection(config.tbl_roles);
                const  objects = dbUtil.get().collection(config.tbl_module);
                const permissions=dbUtil.get().collection(config.tbl_permissions);
                const module_permissions=dbUtil.get().collection(config.tbl_object_permissions);

                let usertype=req.session.user.ses_usertype;
                let organizationid=req.session.user.ses_organization;

                const allpermissions=await permissions.find({}).toArray();

                const modulefield=JSON.parse(req.body.table);
                const roleid=await role.findOne({'_id': new mongo.ObjectId(req.body.role) },{'_id':1});

                // console.log(modulefield);
                // console.log(roleid);

                async.each(modulefield, async(modules, callback)=> {

                    let roleid_beforegetting  = new mongo.ObjectId(roleid._id);
                    let objectid_beforegetting  = new mongo.ObjectId(modules.childmoduleid);
                    let parentobjid_beforegetting  = new mongo.ObjectId(modules.parentmoduleid);

                    if(usertype == config.usertype_access.superadmin) {
                        var new_organizationid= parseInt(organizationid);
                    } else {
                        var new_organizationid = new mongo.ObjectId(organizationid);
                    }

                    let permissionid = new Array();

                    allpermissions.forEach((data)=>{
 
                        const resultt=lodash.get(modules, [data.label])
                        if(resultt==true || resultt=="true"){
                            permissionid.push(new mongo.ObjectId(data._id));
                        }
                    });
                    let module_permissions_obj = new Object();

                    module_permissions_obj["permissions"] = permissionid;
                   
                    let module_permission_update_params = new Object();
                    module_permission_update_params["role"] = roleid_beforegetting;
                    module_permission_update_params["object"] = objectid_beforegetting;
                    module_permission_update_params["parent_object_id"] = parentobjid_beforegetting;
                    module_permission_update_params["organizationid"] = new_organizationid;

                    var moduledeatils = module_permissions.update(module_permission_update_params, { $set: module_permissions_obj });

                    callback();
                },function() {
                    resolve({"success":true,"result":[]});
                });
            });
        }
        updaterole(req.body).then((data)=>{

            if(data.success) {
                req.flash('GOOD', 'Module Permissions Updated Successfully!', false);
                res.status(200).send({status:true, msg:"Module Permissions Updated Successfully!"});
            } else {
                req.flash('BAD', 'Somthing went wrong, Try again!', false);
                res.status(204).send({status:false, msg:"Somthing went wrong!"});
            }
        }).catch((error)=>{
            res.send(error);
        });
    } else {
        res.render('errors/errors',{"domain_url":config.domain, "error_code":401, "error_mainmsg":"Module Permission Denied !"});
    }        
});

/**
 * MODULE : OBJECT PERMISSIONS
 * USE : TO REMOVE RECORD FROM OBJECT PERMISSIONS
 */
router.post('/delete/:id', chk_loginaccess, async(req,res)=>{

    let rbac_obj = res.locals.rbac_obj;
    
    //OBJECT PERMISSIOIN DELETE 
    let rbac4 = new Object();
    rbac4["rbac_obj"] = rbac_obj;
    rbac4["roles"] = req.session.user.ses_rolename;
    rbac4["actions"] = PRMS["PER_DELETE"];
    rbac4["url"] = MDUL["MODURL_MODULEPERMISSIONS"];
    rbac4["usertype"] = req.session.user.ses_usertype;
    let report_delete = await common_functions.go_modules_rbacvalidations(rbac4);

    if(report_delete) {

        let datasource_id = new mongo.ObjectID(req.params.id);

        const  object_permission = dbUtil.get().collection(config.tbl_object_permissions);
        object_permission.remove({'_id':datasource_id},function(err, data){
            if(err) {
                return res.send(err);
            } else {
    
                if(data) {
                    req.flash('GOOD', 'Module Permissions Deleted Successfully!', false);
                    res.status(200).send({status:true, msg:"Module Permissions Deleted Successfully!"});
                } else {
                    req.flash('BAD', 'Somthing went wrong, Try again!', false);
                    res.status(204).send({status:false, msg:"Somthing went wrong!"});
                }            
            }
        });
    } else {
        res.render('errors/errors',{"domain_url":config.domain, "error_code":401, "error_mainmsg":"Module Permission Denied !"});
    }
});

/**
 * MODULE : OBJECT PERMISSIONS
 * USE : CHECKING OBJECT PERMISSIONS ROLE IS ALREADY EXISTIS
 */
router.get('/chk_module_exists', async (req, res) => {
    try {
        const role=dbUtil.get().collection(config.tbl_roles);
        const modules = dbUtil.get().collection(config.tbl_module);
        const module_permissions=dbUtil.get().collection(config.tbl_object_permissions);

        //To check if child module exists
        if(req.query.action=="parentmodule") {

            let parentmoduleid=new mongo.ObjectId(req.query.parent_module_id);

            const modulle = dbUtil.get().collection(config.tbl_module);
        
            const parentid_based_modules = await modulle.find({"root_parent_id":parentmoduleid}).toArray();

            if(parentid_based_modules.length>0){
                return res.send(true);
            }else{
                let qry_role = decodeURI(req.query.role);
                let role_lower = qry_role.toLowerCase();
                let role_matcher = new Object();
                role_matcher["field"] = new RegExp(["^", role_lower.trim(), "$"].join(""), "i");
                const roleid=await role.find(role_matcher,{'_id':1}).toArray();
                  if(roleid.length>0){
                    let matcher = new Object();
                    matcher["role"] = roleid[0]._id;
                    matcher["object"] = new mongo.ObjectId(req.query.parent_module_id);
                    if(req.session.user.ses_usertype != config.usertype_access.superadmin){
                        matcher["organizationid"] = new mongo.ObjectID(req.session.user.ses_organization);
                    }else{
                        matcher["organizationid"] = req.session.user.ses_organization;
                    }
    
                    const find = await module_permissions.find(matcher).toArray();
                    if(find.length > 0) {
                        return res.send(false);
                    } else {
                        return res.send(true);
                    }
    
                  }else{
    
                    return res.send(true);
                  }
               

            }

        } else {
            let u_userid = req.query.u_userid;
            let matcher = new Object();
            if(u_userid !== undefined && u_userid != "") {
                let pk_user_id = new mongo.ObjectID(u_userid);
                matcher["_id"] = {"$nin" : [pk_user_id]};
            }        
          
            let qry_role = decodeURI(req.query.role);
            let role_lower = qry_role.toLowerCase();
            let role_matcher = new Object();
            role_matcher["field"] = new RegExp(["^", role_lower.trim(), "$"].join(""), "i");
            const roleid=await role.find(role_matcher,{'_id':1}).toArray();
    
            let module = decodeURI(req.query.module);
            let module_lower = module.toLowerCase();
            let module_matcher = new Object();
            module_matcher["field"] = new RegExp(["^", module_lower.trim(), "$"].join(""), "i");
            const moduleid=await modules.find(module_matcher,{'_id':1}).toArray();
    
            if(roleid.length > 0 && moduleid.length > 0) {
    
                matcher["role"] = roleid[0]._id;
                matcher["object"] = moduleid[0]._id;
                
                if(req.session.user.ses_usertype != config.usertype_access.superadmin){
                    matcher["organizationid"] = new mongo.ObjectID(req.session.user.ses_organization);
                }else{
                    matcher["organizationid"] = req.session.user.ses_organization;
                }
               
    
                console.log(matcher);
                const find = await module_permissions.find(matcher).toArray();
        
                if(find.length > 0) {
                    return res.send(false);
                } else {
                    return res.send(true);
                }
            } else {
                return res.send(true);
            }

        }
          
    } catch(e) {
        console.log(e.message);
        return res.send(false);
    }
});

/**
 * MODULE : OBJECT PERMISSIONS
 * USE : TO VIEW MODEL PERMISIONS INSERT
 * VIEW : ADMIN/OBJECT_PERMISSIONS_CREATE 
 */
router.get('/submodules', async(req, res)=>{
   
    const result = new Promise(async(resolve, reject)=>{

        try {

            if(req.query.parent_module_id == "" || req.query.parent_module_id == undefined){
                resolve(new Array());
            } else {
                let parent_module_id = new mongo.ObjectID(req.query.parent_module_id);

                const modulle = dbUtil.get().collection(config.tbl_module);
    
                const parentid_based_modules = await modulle.find({"root_parent_id":parent_module_id}).toArray();
    
                resolve(parentid_based_modules);
            }

        } catch(e) {
            reject(e);
        }
    });
    result.then((data)=>{ 
        res.status(200).send(data);
    }).catch((e)=>res.status(500).send(e.message));
});

module.exports = router;