const fs = require('fs');
var express = require('express');
var router = express.Router();
var config = require('../config/database');
var dbUtil = require('../config/dbUtil');
var auth = require('../middlewares/auth'), chk_loginaccess = auth.chk_loginaccess;
var session = require('express-session');
var each = require('async-each');
const bcrypt = require('bcrypt');
var mongo = require('mongodb');
var moment = require("moment");
var common_functions = require('../middlewares/common_functions');
var MODCONF = require('../config/module_config');
const PRMS = MODCONF.PRMS;
const MDUL = MODCONF.MDUL;
const uuidv4 = require('uuid/v4');
const uuidv1 = require('uuid/v1');
const nodemailer = require('nodemailer');
const forgot_expiry_mins = 5;
var Recaptcha = require('express-recaptcha').Recaptcha;
var recaptcha = new Recaptcha('6LeCP2IUAAAAAFAmqBfSG8pVTJLzy88JIPlB1NDg', '6LeCP2IUAAAAAGn_2HXEkCpxsLeGGeTUaKGEfxtb');
var ejs = require('ejs');

/**
 * USE : ROOT LOADER
 * MODULE : OVERVIEW DASHBOARD
 */
router.get('/dashboard', chk_loginaccess, async (req, res, next) => {

  let rbac_obj = res.locals.rbac_obj;

  //URL VALIDATIONS
  let rbac_urls = new Object();
  rbac_urls["rbac_obj"] = rbac_obj;
  rbac_urls["roles"] = req.session.user.ses_rolename;
  rbac_urls["actions"] = PRMS["PER_URL"];
  rbac_urls["url"] = MDUL["MODURL_DASHBOARD"];

  var url_validation = await common_functions.go_modules_rbacvalidations(rbac_urls);

  if(url_validation) {

    //OVERVIEW BUYSELL LIST, DOWNLOAD, PRINT
    let report_buysell = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_OVERVIEWBUYSELL"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_buysell_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_OVERVIEWBUYSELL"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_buysell_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_OVERVIEWBUYSELL"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});

    // OVERVIEW RMRANGE LIST, DOWNLOAD, PRINT
    let report_rmrange = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_OVERVIEWRMRANGE"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_rmrange_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_OVERVIEWRMRANGE"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_rmrange_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_OVERVIEWRMRANGE"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    
    // OVERVIEW CUSTOMERS RISK LIST, DOWNLOAD, PRINT
    let report_customerrisk = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_OVERVIEWCUSTOMERRISK"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_customerrisk_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_OVERVIEWCUSTOMERRISK"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_customerrisk_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_OVERVIEWCUSTOMERRISK"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});

    // OVERVIEW HIGHVALUE TRANSACTIONS RM RANGE LIST, DOWNLOAD, PRINT
    let report_hightransrmrange = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_OVERVIEWHIGHVALUETRANSACTIONSBYRMRANGE"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_hightransrmrange_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_OVERVIEWHIGHVALUETRANSACTIONSBYRMRANGE"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_hightransrmrange_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_OVERVIEWHIGHVALUETRANSACTIONSBYRMRANGE"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});

    // URL PARAMS
    var urlquery_request = new Object();
    urlquery_request["fromdate"] = req.query.fromdate;
    urlquery_request["todate"] = req.query.todate;

    // RENDER PAGE PARAMS 
    let render_params_obj = new Object();
    render_params_obj["pagename"] = "dashboard";
    render_params_obj["domain_url"] = config.domain;
    render_params_obj["ses_user"] = req.session.user;
    render_params_obj["urlquery_request"] = urlquery_request;
    render_params_obj["pos_buysell"] = {"report":report_buysell, "print":report_buysell_print, "download":report_buysell_download};
    render_params_obj["pos_rmrange"] = {"report":report_rmrange, "print":report_rmrange_print, "download":report_rmrange_download};
    render_params_obj["pos_cusrisk"] = {"report":report_customerrisk, "print":report_customerrisk_print, "download":report_customerrisk_download};
    render_params_obj["pos_hightransrmrange"] = {"report":report_hightransrmrange, "print":report_hightransrmrange_print, "download":report_hightransrmrange_download};

    res.render('home', render_params_obj);
  } else {
    res.render('errors/errors',{"domain_url":config.domain, "error_code":401, "error_mainmsg":"Module Permission Denied !"});
  }
});

/**
 * USE : LIST LOADER CUSTOMER COUNT BASED ON RISK TYPES
 * MODULE : OVERALL CUSTOMER RISK
 */
router.get('/overallCustomerRisk', chk_loginaccess, async (req, res) => {

  let rbac_obj = res.locals.rbac_obj;
  
  //URL VALIDATIONS
  let rbac_urls = new Object();
  rbac_urls["rbac_obj"] = rbac_obj;
  rbac_urls["roles"] = req.session.user.ses_rolename;
  rbac_urls["actions"] = PRMS["PER_URL"];
  rbac_urls["url"] = MDUL["MODURL_OVERALLCUSTOMERRISK"];

  var url_validation = await common_functions.go_modules_rbacvalidations(rbac_urls);

  if(url_validation) {

    ///OCR OVERALL RISKBASED ANALYSIS LIST, PRINT, DOWNLOAD
    let report_overall_rbc = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_OCROVERALLRISKBASEDANALYSIS"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_overall_rbc_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_OCROVERALLRISKBASEDANALYSIS"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_overall_rbc_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_OCROVERALLRISKBASEDANALYSIS"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});

    var urlquery_request = new Object();
    urlquery_request["fromdate"] = req.query.fromdate;
    urlquery_request["todate"] = req.query.todate;

    var fromdate = req.query.fromdate || "";
    var todate = req.query.todate || "";
    
    var datasource_subqry = new Object();
    datasource_subqry["data_proprietor"] = new mongo.ObjectID(req.session.user.ses_userid);

    if(fromdate == '' || todate == '') {
      var global_dater = new Date();
      var pydate = new Date();
      pydate.setFullYear( pydate.getFullYear() - 1 );

      var fmdt_cnter = new Date(pydate.getFullYear()+"-"+(pydate.getMonth()+1)+"-"+pydate.getDate());
      var todt_cnter = new Date(global_dater.getFullYear()+"-"+(global_dater.getMonth()+1)+"-"+global_dater.getDate());
    } else {
      var from_spliter = fromdate.split("-");
      var to_spliter = todate.split("-");
      
      var fmdt_cnter = new Date(from_spliter[2]+"-"+from_spliter[1]+"-"+from_spliter[0]);
      var todt_cnter = new Date(to_spliter[2]+"-"+to_spliter[1]+"-"+to_spliter[0]);
    }
  
    var collection = dbUtil.get().collection(config.tbl_sanitized_data);
      
    var risk_field_arr = [
      "Value Risk Score", 
      "Nationality risk Score", 
      "Customer Type Risk Score", 
      "Purpose Risk Score", 
      "Source Risk score", 
      "Occupation Risk Score", 
      "Mode of Payment Risk Score", 
      "Mode of Delivery Risk Score"
    ];

    var final_arr = [];
    var low_arr = {};
    low_arr["risk_type"] = "low";
      
    each(risk_field_arr, function(fieldname, next){

      var low_params = {
        "collection":collection, 
        "fieldname":"", 
        "fieldvalue":1, 
        "fmdt_cnter":fmdt_cnter, 
        "todt_cnter":todt_cnter, 
        "datasource_subqry":datasource_subqry
      };

      low_params["fieldname"] = fieldname;
      
      qry_riskfactors(low_params, function(risk_data){
        low_arr[(fieldname.replace(/ /g,"_")).toLowerCase()] = risk_data;
        next();
      });

    },function(){

      final_arr.push(low_arr);
      
      var medium_arr = {};
      medium_arr["risk_type"] = "medium";
      
      each(risk_field_arr, function(fieldname, next){
        
        var medium_params = {
          "collection":collection, 
          "fieldname":"", 
          "fieldvalue":2, 
          "fmdt_cnter":fmdt_cnter, 
          "todt_cnter":todt_cnter, 
          "datasource_subqry":datasource_subqry
        };

        medium_params["fieldname"] = fieldname;
        
        qry_riskfactors(medium_params, function(risk_data){
          medium_arr[(fieldname.replace(/ /g,"_")).toLowerCase()] = risk_data;
          next();
        });

      },function(){
        
        final_arr.push(medium_arr);
        
        var high_arr = {};
        high_arr["risk_type"] = "high";
        
        each(risk_field_arr, function(fieldname, next){
          
          var high_params = {
            "collection":collection, 
            "fieldname":"", 
            "fieldvalue":3, 
            "fmdt_cnter":fmdt_cnter, 
            "todt_cnter":todt_cnter, 
            "datasource_subqry":datasource_subqry
          };
  
          high_params["fieldname"] = fieldname;
          
          qry_riskfactors(high_params, function(risk_data){
            high_arr[(fieldname.replace(/ /g,"_")).toLowerCase()] = risk_data;
            next();
          });
  
        },function(){
          
          final_arr.push(high_arr);

          // RENDER PAGE PARAMS 
          let render_params_obj = new Object();
          render_params_obj["pagename"] = "overall_customer_risk";
          render_params_obj["domain_url"] = config.domain;
          render_params_obj["ses_user"] = req.session.user;
          render_params_obj["urlquery_request"] = urlquery_request;
          render_params_obj["pos_overall_rbc"] = {"report":report_overall_rbc, "print": report_overall_rbc_print, "download": report_overall_rbc_download };
          render_params_obj["data"] = final_arr;

          res.render("overall_customerrisk_overview", render_params_obj);  
        });          
      });
    });

  } else {
    res.render('errors/errors',{"domain_url":config.domain, "error_code":401, "error_mainmsg":"Module Permission Denied !"});
  }  
});

/**
 * USE : ROOT LOADER
 * MODULE : RISKBASED ANALYSIS
 */
router.get('/customerRisk', chk_loginaccess, async (req, res) => {

  let rbac_obj = res.locals.rbac_obj;

  //URL VALIDATIONS
  let rbac_urls = new Object();
  rbac_urls["rbac_obj"] = rbac_obj;
  rbac_urls["roles"] = req.session.user.ses_rolename;
  rbac_urls["actions"] = PRMS["PER_URL"];
  rbac_urls["url"] = MDUL["MODURL_CUSTOMERRISK"];

  var url_validation = await common_functions.go_modules_rbacvalidations(rbac_urls);

  if(url_validation) {

    //RBA NATIONALITY LIST, DOWNLOAD, PRINT
    let report_nationality = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_RBANATIONALITY"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_nationality_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_RBANATIONALITY"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_nationality_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_RBANATIONALITY"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});

    //RBA VALUE RISK LIST, DOWNLOAD, PRINT
    let report_valuerisk = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_RBAVALUERISK"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_valuerisk_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_RBAVALUERISK"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_valuerisk_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_RBAVALUERISK"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});

    //RBA CUSTOMER TYPE LIST, DOWNLOAD, PRINT
    let report_customertype = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_RBACUSTOMERTYPE"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_customertype_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_RBACUSTOMERTYPE"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_customertype_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_RBACUSTOMERTYPE"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});

    //RBA PURPOSE LIST, DOWNLOAD, PRINT
    let report_purpose = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_RBAPURPOSE"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_purpose_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_RBAPURPOSE"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_purpose_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_RBAPURPOSE"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});

    //RBA SOURCE LIST, DOWNLOAD, PRINT
    let report_source = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_RBASOURCE"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_source_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_RBASOURCE"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_source_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_RBASOURCE"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});

    //RBA OCCUPATION LIST, DOWNLOAD, PRINT
    let report_occupation = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_RBAOCCUPATION"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_occupation_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_RBAOCCUPATION"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_occupation_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_RBAOCCUPATION"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    
    //RBA PAYMENT LIST, DOWNLOAD, PRINT
    let report_payment = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_RBAPAYMENT"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_payment_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_RBAPAYMENT"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_payment_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_RBAPAYMENT"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});

    //RBA DELIVERY
    let report_delivery = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_RBADELIVERY"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_delivery_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_RBADELIVERY"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_delivery_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_RBADELIVERY"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});

    // URL PARAMS
    var urlquery_request = new Object();
    urlquery_request["fromdate"] = req.query.fromdate;
    urlquery_request["todate"] = req.query.todate;

    // RENDER PAGE PARAMS 
    let render_params_obj = new Object();
    render_params_obj["pagename"] = "customer_risk";
    render_params_obj["domain_url"] = config.domain;
    render_params_obj["ses_user"] = req.session.user;
    render_params_obj["urlquery_request"] = urlquery_request;

    render_params_obj["pos_nationality"] = {"report":report_nationality, "print":report_nationality_print, "download":report_nationality_download};
    render_params_obj["pos_valuerisk"] = {"report":report_valuerisk, "print":report_valuerisk_print, "download":report_valuerisk_download };
    render_params_obj["pos_customertype"] = {"report":report_customertype, "print":report_customertype_print, "download":report_customertype_download};
    render_params_obj["pos_purpose"] = {"report":report_purpose, "print":report_purpose_print, "download":report_purpose_download};
    render_params_obj["pos_source"] = {"report":report_source, "print":report_source_print, "download":report_source_download};
    render_params_obj["pos_occupation"] = {"report":report_occupation, "print":report_occupation_print, "download":report_occupation_download};
    render_params_obj["pos_payment"] = {"report":report_payment, "print":report_payment_print, "download":report_payment_download};
    render_params_obj["pos_delivery"] = {"report":report_delivery, "print":report_delivery_print, "download":report_delivery_download};
 
    res.render("overall_customerrisk", render_params_obj);
  } else {
    res.render('errors/errors',{"domain_url":config.domain, "error_code":401, "error_mainmsg":"Module Permission Denied !"});
  }
});

/**
 * USE : ROOT LOADER
 * MODULE : HIGH RISK CUSTOMERS
 */
router.get('/highriskCustomerlistReport', chk_loginaccess, async (req, res) => {

  let rbac_obj = res.locals.rbac_obj;
  
  //URL VALIDATIONS
  let rbac_urls = new Object();
  rbac_urls["rbac_obj"] = rbac_obj;
  rbac_urls["roles"] = req.session.user.ses_rolename;
  rbac_urls["actions"] = PRMS["PER_URL"];
  rbac_urls["url"] = MDUL["MODURL_HIGHRISKCUSTOMERLISTREPORT"];

  var url_validation = await common_functions.go_modules_rbacvalidations(rbac_urls);

  if(url_validation) {

    //HRC OVERALL OUTLET HIGHRISK LIST, PRINT, DOWNLOAD
    let report_hrc_outlet = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_HRCOVERALLOUTLETHIGHRISK"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_hrc_outlet_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_HRCOVERALLOUTLETHIGHRISK"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_hrc_outlet_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_HRCOVERALLOUTLETHIGHRISK"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});

    //HRC LOCAL INDIVIDUALS CUSTOMERS LIST, PRINT, DOWNLOAD
    let report_hrc_lic = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_HRCLOCALINDIVIDUALCUSTOMER"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_hrc_lic_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_HRCLOCALINDIVIDUALCUSTOMER"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_hrc_lic_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_HRCLOCALINDIVIDUALCUSTOMER"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});

    //HRC FOREIGN INDIVIDUALS CUSTOMERS LIST, PRINT, DOWNLOAD
    let report_hrc_fic = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_HRCFOREIGNINDIVIDUALSCUSTOMER"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_hrc_fic_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_HRCFOREIGNINDIVIDUALSCUSTOMER"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_hrc_fic_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_HRCFOREIGNINDIVIDUALSCUSTOMER"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    
    //HRC LOCAL BUSINESSES CUSTOMERS LIST, PRINT, DOWNLOA
    let report_hrc_lbc = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_HRCLOCALBUSINESSESCUSTOMER"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_hrc_lbc_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_HRCLOCALBUSINESSESCUSTOMER"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_hrc_lbc_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_HRCLOCALBUSINESSESCUSTOMER"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    
    var urlquery_request = new Object();
    urlquery_request["fromdate"] = req.query.fromdate;
    urlquery_request["todate"] = req.query.todate;

    // RENDER PAGE PARAMS 
    let render_params_obj = new Object();
    render_params_obj["pagename"] = "highrisk_customerlist";
    render_params_obj["domain_url"] = config.domain;
    render_params_obj["ses_user"] = req.session.user;
    render_params_obj["urlquery_request"] = urlquery_request;
    render_params_obj["pos_hrcoutlet"] = {"report":report_hrc_outlet, "print":report_hrc_outlet_print, "download":report_hrc_outlet_download};
    render_params_obj["pos_hrclic"] = {"report":report_hrc_lic, "print":report_hrc_lic_print, "download":report_hrc_lic_download};
    render_params_obj["pos_hrcfic"] = {"report":report_hrc_fic, "print":report_hrc_fic_print, "download":report_hrc_fic_download};
    render_params_obj["pos_hrclbc"] = {"report":report_hrc_lbc, "print":report_hrc_lbc_print, "download":report_hrc_lbc_download};

    res.render("highrisk_customerlist_report",  render_params_obj);    
  } else {
    res.render('errors/errors',{"domain_url":config.domain, "error_code":401, "error_mainmsg":"Module Permission Denied !"});
  }
});  

/**
 * USE : ROOT LOADER
 * MODULE : HIGHRISK NETWORTH
 */
router.get('/highNetworkCustomerReport', chk_loginaccess, async (req, res) => {

  let rbac_obj = res.locals.rbac_obj;
  
  //URL VALIDATIONS
  let rbac_urls = new Object();
  rbac_urls["rbac_obj"] = rbac_obj;
  rbac_urls["roles"] = req.session.user.ses_rolename;
  rbac_urls["actions"] = PRMS["PER_URL"];
  rbac_urls["url"] = MDUL["MODURL_HIGHNETWORKCUSTOMERREPORT"];

  var url_validation = await common_functions.go_modules_rbacvalidations(rbac_urls);

  if(url_validation) {

    //HNC OVERALL OUTLET HIGHRISK RM RANGE LIST, PRINT, DOWNLOAD 
    let report_hnc_outlet = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_HNCOVERALLOUTLETHIGHRISKRMRANGE"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_hnc_outlet_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_HNCOVERALLOUTLETHIGHRISKRMRANGE"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_hnc_outlet_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_HNCOVERALLOUTLETHIGHRISKRMRANGE"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});

    //HNC LOCAL INDIVIDUALS CUSTOMERS RM RANGE LIST, PRINT, DOWNLOAD
    let report_hnc_lic = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_HNCLOCALINDIVIDUALCUSTOMERRMRANGE"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_hnc_lic_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_HNCLOCALINDIVIDUALCUSTOMERRMRANGE"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_hnc_lic_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_HNCLOCALINDIVIDUALCUSTOMERRMRANGE"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    
    //HNC FOREIGN INDIVIDUALS CUSTOMERS RM RANGE LIST, PRINT, DOWNLOAD
    let report_hnc_fic = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_HNCFOREIGNINDIVIDUALSCUSTOMERRMRANGE"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_hnc_fic_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_HNCFOREIGNINDIVIDUALSCUSTOMERRMRANGE"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_hnc_fic_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_HNCFOREIGNINDIVIDUALSCUSTOMERRMRANGE"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});

    //HNC LOCAL BUSINESSES CUSTOMERS RM RANGE LIST, PRINT, DOWNLOAD
    let report_hnc_lbc = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_HNCLOCALBUSINESSESCUSTOMERRMRANGE"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_hnc_lbc_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_HNCLOCALBUSINESSESCUSTOMERRMRANGE"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_hnc_lbc_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_HNCLOCALBUSINESSESCUSTOMERRMRANGE"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});

    var urlquery_request = new Object();
    urlquery_request["fromdate"] = req.query.fromdate;
    urlquery_request["todate"] = req.query.todate;

    // RENDER PAGE PARAMS 
    let render_params_obj = new Object();
    render_params_obj["pagename"] = "highrisk_networklist";
    render_params_obj["domain_url"] = config.domain;
    render_params_obj["ses_user"] = req.session.user;
    render_params_obj["urlquery_request"] = urlquery_request;
    render_params_obj["pos_hncoutlet_rmrange"] = {"report":report_hnc_outlet, "print":report_hnc_outlet_print, "download":report_hnc_outlet_download};
    render_params_obj["pos_hnclic_rmrange"] = {"report":report_hnc_lic, "print":report_hnc_lic_print, "download":report_hnc_lic_download};
    render_params_obj["pos_hncfic_rmrange"] = {"report":report_hnc_fic, "print":report_hnc_fic_print, "download":report_hnc_fic_download};
    render_params_obj["pos_hnclbc_rmrange"] = {"report":report_hnc_lbc, "print":report_hnc_lbc_print, "download":report_hnc_lbc_download};

    res.render("high_network_customer_report",  render_params_obj);    
  } else {
    res.render('errors/errors',{"domain_url":config.domain, "error_code":401, "error_mainmsg":"Module Permission Denied !"});
  }  
});  

/**
 * USE : ROOT LOADER FOR FREQUENT TRANSACTION
 * MODULE : TRANSACTION RISK ANALYSIS
 */
router.get('/frequentTransactionsReport', chk_loginaccess, async (req, res) => {

  let rbac_obj = res.locals.rbac_obj;
  
  //URL VALIDATIONS
  let rbac_urls = new Object();
  rbac_urls["rbac_obj"] = rbac_obj;
  rbac_urls["roles"] = req.session.user.ses_rolename;
  rbac_urls["actions"] = PRMS["PER_URL"];
  rbac_urls["url"] = MDUL["MODURL_FREQUENTTRANSACTIONSREPORT"];

  var url_validation = await common_functions.go_modules_rbacvalidations(rbac_urls);

  if(url_validation) {

    //HRC OVERALL OUTLET HIGHRISK LIST, PRINT, DOWNLOAD
    let report_tra_frequenttrans = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_TRAFREQUENTTRANSACTION"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_tra_frequenttrans_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_TRAFREQUENTTRANSACTION"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_tra_frequenttrans_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_TRAFREQUENTTRANSACTION"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});

    //HRC LOCAL INDIVIDUALS CUSTOMERS LIST, PRINT, DOWNLOAD
    let report_tra_multicurrencytrans = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_TRAMULTIPLECURRENCYTRANSACTION"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_tra_multicurrencytrans_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_TRAMULTIPLECURRENCYTRANSACTION"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_tra_multicurrencytrans_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_TRAMULTIPLECURRENCYTRANSACTION"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});

    //TRAMB FREQUENT TRANSACTIONS
    let report_mbtra_ft = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_TRAMBFREQUENTTRANSACTION"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_mbtra_ft_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_TRAMBFREQUENTTRANSACTION"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_mbtra_ft_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_TRAMBFREQUENTTRANSACTION"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});

    //TRAMB MULTIPLE CURRENCY TRANSACTIONS
    let report_mbtra_fct = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_TRAMBMULTIPLECURRENCYTRANSACTION"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_mbtra_fct_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_TRAMBMULTIPLECURRENCYTRANSACTION"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_mbtra_fct_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_TRAMBMULTIPLECURRENCYTRANSACTION"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    
    var urlquery_request = new Object();
    urlquery_request["fromdate"] = req.query.fromdate;
    urlquery_request["todate"] = req.query.todate;
    
    // RENDER PAGE PARAMS 
    let render_params_obj = new Object();
    render_params_obj["pagename"] = "frequency_transactions";
    render_params_obj["domain_url"] = config.domain;
    render_params_obj["ses_user"] = req.session.user;
    render_params_obj["urlquery_request"] = urlquery_request;
    render_params_obj["pos_tra_frequenttrans"] = {"report":report_tra_frequenttrans};
    render_params_obj["pos_tra_multicurrencytrans"] = {"report":report_tra_multicurrencytrans};

    render_params_obj["pos_mbtra_ft"] = {"report":report_mbtra_ft, "print":report_mbtra_ft_print, "download":report_mbtra_ft_download};
    render_params_obj["pos_mbtra_fct"] = {"report":report_mbtra_fct, "print":report_mbtra_fct_print, "download":report_mbtra_fct_download};
    
    res.render("frequent_transactions_report", render_params_obj);
  } else {
    res.render('errors/errors',{"domain_url":config.domain, "error_code":401, "error_mainmsg":"Module Permission Denied !"});
  }
});  

/**
 * USE : ROOT LOADER
 * MODULE : MONTHLY HIGHRISK TRANSACTIONS ANALYSIS
 */
router.get('/monthlyHighriskCustomerlistReport', chk_loginaccess, async (req, res) => {

  let rbac_obj = res.locals.rbac_obj;

  //URL VALIDATIONS
  let rbac_urls = new Object();
  rbac_urls["rbac_obj"] = rbac_obj;
  rbac_urls["roles"] = req.session.user.ses_rolename;
  rbac_urls["actions"] = PRMS["PER_URL"];
  rbac_urls["url"] = MDUL["MODURL_MONTHLYHIGHRISKCUSTOMERLISTREPORT"];

  var url_validation = await common_functions.go_modules_rbacvalidations(rbac_urls);

  if(url_validation) {

    //MBHRTA OVERALL OUTLET HIGHRISK, PRINT, DOWNLOAD
    let report_mbhrta_outlet = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_MBHRTAOVERALLOUTLETHIGHRISK"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_mbhrta_outlet_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_MBHRTAOVERALLOUTLETHIGHRISK"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_mbhrta_outlet_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_MBHRTAOVERALLOUTLETHIGHRISK"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});

    //MBHRTA LOCAL INDIVIDUALS CUSTOMERS, PRINT, DOWNLOAD
    let report_mbhrta_lic = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_MBHRTALOCALINDIVIDUALCUSTOMER"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_mbhrta_lic_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_MBHRTALOCALINDIVIDUALCUSTOMER"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_mbhrta_lic_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_MBHRTALOCALINDIVIDUALCUSTOMER"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});

    //MBHRTA FOREIGN INDIVIDUALS CUSTOMERS, PRINT, DOWNLOAD
    let report_mbhrta_fic = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_MBHRTAFOREIGNINDIVIDUALSCUSTOMER"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_mbhrta_fic_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_MBHRTAFOREIGNINDIVIDUALSCUSTOMER"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_mbhrta_fic_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_MBHRTAFOREIGNINDIVIDUALSCUSTOMER"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});

    //MBHRTA LOCAL BUSINESSES CUSTOMERS, PRINT, DOWNLOAD
    let report_mbhrta_lbc = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_MBHRTALOCALBUSINESSESCUSTOMER"], "actions":PRMS["PER_LISTVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_mbhrta_lbc_print = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_MBHRTALOCALBUSINESSESCUSTOMER"], "actions":PRMS["PER_DPRINT"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});
    let report_mbhrta_lbc_download = await common_functions.go_modules_rbacvalidations({"url":MDUL["MOD_MBHRTALOCALBUSINESSESCUSTOMER"], "actions":PRMS["PER_DVIEW"], "rbac_obj":rbac_obj, "roles":req.session.user.ses_rolename});

    var urlquery_request = new Object();
    urlquery_request["fromdate"] = req.query.fromdate;
    urlquery_request["todate"] = req.query.todate;

    // RENDER PAGE PARAMS 
    let render_params_obj = new Object();
    render_params_obj["pagename"] = "monthly_highrisk_customerlist";
    render_params_obj["domain_url"] = config.domain;
    render_params_obj["ses_user"] = req.session.user;
    render_params_obj["urlquery_request"] = urlquery_request;
    render_params_obj["pos_mbhrta_outlet"] = {"report":report_mbhrta_outlet, "print":report_mbhrta_outlet_print, "download":report_mbhrta_outlet_download};
    render_params_obj["pos_mbhrta_lic"] = {"report":report_mbhrta_lic, "print":report_mbhrta_lic_print, "download":report_mbhrta_lic_download};
    render_params_obj["pos_mbhrta_fic"] = {"report":report_mbhrta_fic, "print":report_mbhrta_fic_print, "download":report_mbhrta_fic_download};
    render_params_obj["pos_mbhrta_lbc"] = {"report":report_mbhrta_lbc, "print":report_mbhrta_lbc_print, "download":report_mbhrta_lbc_download};

    res.render("monthly_highrisk_customerlist_report",  render_params_obj);    
  } else {
    res.render('errors/errors',{"domain_url":config.domain, "error_code":401, "error_mainmsg":"Module Permission Denied !"});
  }
});  

/**
 * USE : ROOT LOADER SANITIZED DOCUMENT UPLOAD VIEW
 * MODULE : DATASOURCE UPLOAD
 */
router.get('/xlupload', chk_loginaccess, async (req, res, next) => {

  let rbac_obj = res.locals.rbac_obj;
  
  //URL VALIDATIONS
  let rbac_urls = new Object();
  rbac_urls["rbac_obj"] = rbac_obj;
  rbac_urls["roles"] = req.session.user.ses_rolename;
  rbac_urls["actions"] = PRMS["PER_URL"];
  rbac_urls["url"] = MDUL["MODURL_DATASOURCEUPLOAD"];

  var url_validation = await common_functions.go_modules_rbacvalidations(rbac_urls);

  if(url_validation) {

    var urlquery_request = new Object();
    urlquery_request["fromdate"] = req.query.fromdate;
    urlquery_request["todate"] = req.query.todate;

    // RENDER PAGE PARAMS 
    let render_params_obj = new Object();
    render_params_obj["pagename"] = "xlupload";
    render_params_obj["domain_url"] = config.domain;
    render_params_obj["ses_user"] = req.session.user;
    render_params_obj["urlquery_request"] = urlquery_request;

    res.render("sanitized_import",  render_params_obj);    
  } else {
    res.render('errors/errors',{"domain_url":config.domain, "error_code":401, "error_mainmsg":"Module Permission Denied !"});
  }
});

/**
 * MODULE : MASTER RISK SCORE
 * VIEW : ADMIN/MASTER_RISK
 */
router.get('/masterrisk', chk_loginaccess, async (req, res, next) => {

  let rbac_obj = res.locals.rbac_obj;

  //URL VALIDATIONS
  let rbac_urls = new Object();
  rbac_urls["rbac_obj"] = rbac_obj;
  rbac_urls["roles"] = req.session.user.ses_rolename;
  rbac_urls["actions"] = PRMS["PER_URL"];
  rbac_urls["url"] = MDUL["MODURL_MASTERRISK"];

  var url_validation = await common_functions.go_modules_rbacvalidations(rbac_urls);

  if(url_validation) {

      // URL PARAMS
      var urlquery_request = new Object();
      urlquery_request["fromdate"] = "";
      urlquery_request["todate"] = "";

      // RENDER PAGE PARAMS 
      let render_params_obj = new Object();
      render_params_obj["pagename"] = "masterrisk";
      render_params_obj["domain_url"] = config.domain;
      render_params_obj["ses_user"] = req.session.user;
      render_params_obj["master_risktypes"] = config.master_risktypes;
      render_params_obj["urlquery_request"] = urlquery_request;
  
      res.render("./admin/master_risk", render_params_obj);

  } else {
      res.render('errors/errors',{"domain_url":config.domain, "error_code":401, "error_mainmsg":"Module Permission Denied !"});
  }
});

/**
 * USE : LOGIN REDIRECTION 
 * MODULE : LOGIN PAGE
 */
router.get('/', function (req, res, next) {

  console.log("Root :: Happy loginer")

  if(!req.session.user) 
    res.render('login',{"domain_url":config.domain, "access_msg":""});
  else {
    if(req.session.user.ses_usertype == config.usertype_access.superadmin)
      res.redirect('/users');
    else
      res.redirect('/dashboard');
  }
});

/**
 * USE : LOGIN REDIRECTION 
 * MODULE : LOGIN PAGE
 */
router.get('/login', function (req, res, next) {

  console.log("Login :: Happy loginer");
  console.log((!req.session.user));

  if(!req.session.user) 
    res.render('login',{"domain_url":config.domain, "access_msg":""});
  else {
    if(req.session.user.ses_usertype == config.usertype_access.superadmin)
      res.redirect('/users');
    else
      res.redirect('/dashboard');
  }
});

/**
 * USE : FORGOT PASSWORD 
 * MODULE : LOGIN PAGE
 */

router.post('/forgotPassword', async (req, res) => {

  const result = new Promise(async (resolve, reject) => {
      try {
          var email;var db_username;
          var username = req.body.username;
          var forgot_type = req.body.forgot_type;
          var uuid = uuidv4();
          var isodate = new Date(new Date + ' UTC').toISOString();
          const users = dbUtil.get().collection(config.tbl_users);
          await users.findOne({
              $or: [{
                  'u_email': username
              }, {
                  'u_username': username
              }]
          }, function(err, result) {
              if (err) throw err;
              if (result == null || result == "") {
                  req.flash('BAD', 'Please enter valid data !!!', false);
                  res.send({
                      status: false,
                      msg: "Please enter valid data"
                  });
              } else {
                  email       = result.u_email.toLowerCase();
                  db_username = result.u_username;
                  users.updateOne({
                      $or: [{
                          'u_email': username
                      }, {
                          'u_username': username
                      }]
                  }, {
                      $set: {
                          "u_forgot_pkey": uuid,
                          "u_forgot_pdate": isodate
                      }
                  }, function(err, result) {
                      if (err) throw err;
                      if (result == null || result == "") {
                          req.flash('BAD', 'Please enter valid data !!!', false);
                          res.send({
                              status: false,
                              msg: "Please enter valid data"
                          });
                      } else {
                          // email = "kartikk7m@gmail.com"; // for temporary
                          var secret_key = uuidv1();
                          var domain = (config.domain).replace(/['"]+/g, '');
                          var logo_url = domain +"img/msb_icon_1.png";
                          var url = domain + "resetPassword?secret_key='" + secret_key + "'&product_key='" + uuid + "'&email='" + email + "'";
                          if(forgot_type == 1){
                            var compiled = ejs.compile(fs.readFileSync('./views/email_template_2.ejs', 'utf8'));
                            var template = compiled({ logo_url : logo_url, username : db_username });
                          }else{
                            var compiled = ejs.compile(fs.readFileSync('./views/email_template.ejs', 'utf8'));
                            var template = compiled({ logo_url : logo_url, url : url });
                          }
                          var transporter = nodemailer.createTransport({
                              service: 'gmail',
                              auth: {
                                  user: 'kartik.m@hoffensoft.com',
                                  pass: 'k@rtikM979'
                              }
                          });
                          var mailOptions = {
                              from: 'kartik.m@hoffensoft.com',
                              to: email,
                              subject: 'MSB - Change Password Request',
                              attachments: [{
                                    filename: 'Logo.png',
                                    path: logo_url,
                                    cid: 'logo'
                              }],
                              html: template
                              // html: '<p>Hi there,</p><p>Please <a href="'+url+'" style="text-decoration:underline">  Click </a> to change your password in MSB.</p><p>Good luck! Hope it works.</p><p>Regards,<br/> Team MSB.</p>',
                          };
                          transporter.sendMail(mailOptions, function(error, info) {
                              if (error) {
                                  console.log(error);
                                  res.status(204).send({
                                      status: false,
                                      msg: "failed",
                                      err_msg: error
                                  });
                              } else {
                                  console.log('Email sent: ' + info.response);
                                  req.flash('GOOD', 'Reset Password link sent to your mail successfully', false);
                                  res.status(200).send({
                                      status: true,
                                      msg: "success"
                                  });
                              }
                          });
                      }
                  });
              }
          });

      } catch (e) {
          console.log(e);
          // reject(e);
      }
  });
});
//RESET PASSWORD
router.get('/resetPassword', async (req, res, next)=> {
  try{
    var users           = dbUtil.get().collection(config.tbl_users);
    var email_id        = req.query.email.replace(/['"]+/g, '').toLowerCase();
    var product_key     = req.query.product_key.replace(/['"]+/g, '').toLowerCase();
    user_table          = await users.findOne({'u_email':email_id,'u_forgot_pkey':product_key});
    if(!user_table){
      req.flash('BAD','Not allowed to reset password,try again..!',false);
      res.redirect('/login');
    }
    //adding 5 mins for expiry check
    var db_datetime     = user_table.u_forgot_pdate;
    var curr_datetime   = new Date(new Date+' UTC').toISOString();
    var diff            = Math.abs(new Date(curr_datetime) - new Date(db_datetime));
    var minutes         = Math.floor((diff/1000)/60);
    if(minutes > forgot_expiry_mins){
      req.flash('BAD','Your resetting password link is expired,try again..!',false);
      res.redirect('/login');
    }
    res.render('reset_password',{"domain_url":config.domain,"secret_key":req.query.secret_key,"uuid":req.query.product_key,"email_id":req.query.email,captcha:recaptcha.render()});
  }catch (e) {
    console.log(e);
  }

});

//RESET PASSWORD AUTHENTICATE
router.post('/resetAuthenticate', async (req, res, next)=> {
  const result = new Promise(async(resolve, reject) => {
    try {
        recaptcha.verify(req, async (error, data) => {
        if(!error) {
          let user_table;
          let update_user_table;
          var users           = dbUtil.get().collection(config.tbl_users);
          var forget_pass_key = req.body.product_key.replace(/['"]+/g, '');
          var email_id        = req.body.email_id.replace(/['"]+/g, '').toLowerCase();
          var password        = req.body.password;
          var password_bcrypt = bcrypt.hashSync((password)+config.login_salt, 16);
          //email_id = "Gopalakrishnan.r@hoffensoft.com"; // for temp
          user_table          = await users.findOne({'u_email':email_id,'u_forgot_pkey':forget_pass_key});
          if(!user_table){
            req.flash('BAD','Failed to reset password,try again..!',false);
            res.send({status:false, msg:"failed to get record"});
          }else{
              //adding 5 mins for expiry check
              var db_datetime     = user_table.u_forgot_pdate;
              var curr_datetime   = new Date(new Date+' UTC').toISOString();
              var diff            = Math.abs(new Date(curr_datetime) - new Date(db_datetime));
              var minutes         = Math.floor((diff/1000)/60);
              if(minutes > forgot_expiry_mins){
                console.log('expired');
                req.flash('BAD','Your resetting password link is expired,try again..!',false);
                res.send({status:false, msg:"Your link is expired"});
              }else{
                console.log('not expired');
                update_user_table = await users.update({"u_email" : email_id},{$set: {"u_password":password, "u_password_salter" : password_bcrypt,"u_forgot_pdate":null,"u_forgot_pkey":null}});
                req.flash('GOOD','Your password changed successfully',false);
                res.send({status:true,msg:"success"});
              }
          }
         
        }
         else {
          console.log(error);
        }
     
    });    
    } catch (e) {
        console.log(e);
        // reject(e);
    }
});
  
  
});



/**
 * USE : USER LOGIN PURPOSE 
 * MODULE : LOGIN PAGE
 */
router.post('/login', function (req, res, next) {

  var username = req.body.username;
  var userpass = req.body.password;
  /**
   * usertype : SuperAdmin
   * username : msbadmin
   * userpass : admin123
   */

  if(username !== undefined && userpass !== undefined && username != '' && userpass != '' && username.length > 4 && userpass.length > 4) {

    var promise_loginer = new Promise(async(resolve, reject) => {

      try {

        var finder = new Object();
        finder["u_username"] = username;
    
        var login_result = await dbUtil.get().collection(config.tbl_users).aggregate([
          { $match : finder },
          { $lookup: { from: config.tbl_roles, localField: 'u_role', foreignField: '_id', as: 'role' } },
        ]).toArray();        

        if(login_result.length==1) {

          var logindata = login_result[0];

          if(bcrypt.compareSync(userpass+config.login_salt, logindata["u_password_salter"])) {

            let user_session_data = new Object();
            user_session_data['ses_userid'] = logindata["_id"];
            user_session_data['ses_name'] = logindata["u_firstname"]+' '+logindata["u_lastname"]; 
            user_session_data['ses_email'] = logindata["u_email"];  
            user_session_data['ses_mobile'] = logindata["u_mobile"];  
            user_session_data['ses_username'] = logindata["u_username"];  
            user_session_data['ses_usertype'] = logindata["u_usertype"];
            user_session_data['ses_role'] = logindata["u_role"];
            let u_roles = (logindata["role"].length > 0) ? logindata["role"] : new Array(); 
            user_session_data['ses_rolename'] = (u_roles.length > 0) ? u_roles[0].field : "";
            user_session_data['ses_reporting_to'] = logindata["u_reporting_to"];
            user_session_data['ses_reporting_hierarchy'] = logindata["u_reporting_hierarchy"];
           //load organizationid except for super admin
            if(logindata["u_usertype"] !== config.usertype_access.superadmin){
              user_session_data['ses_organization']=logindata["u_organizationid"];
              var org_data = await dbUtil.get().collection(config.tbl_organization).findOne({'_id':logindata["u_organizationid"]})
              user_session_data['ses_organization_logo']=org_data.u_organization_logo;
              user_session_data['ses_organization_name']=org_data.u_orgname;
            } else {
              user_session_data['ses_organization']=logindata["u_organizationid"];
              user_session_data['ses_organization_logo']= "profile.png";
              user_session_data['ses_organization_name']= "Hoffensoft Superadmin";
            }

            req.session.user = user_session_data;

            if(user_session_data.ses_usertype == config.usertype_access.superadmin)
              res.redirect(config.SA_LOGIN_SUSINDEX);
            else
              res.redirect(config.CMN_LOGIN_SUSINDEX);
          } else {
            req.flash('BAD', 'Given Password was wrong, Try again !!!', false);
            res.redirect('/login');
          }
        } else {
          req.flash('BAD', 'Given Username was not available, Try again !!!', false);
          res.redirect('/login');
        }
      } catch(e) {
        reject(e);
      }
    });    
  } else{
    // res.redirect('/login?access=errlogin');
    req.flash('BAD', 'Username & Password was required!', false);
    res.redirect('/login');
  }
});


/**
 * USE : USER LOGOUT PURPOSE 
 * MODULE : LOGOUT
 */
router.get('/logout', function (req, res) {
  req.session.destroy();
  res.redirect('/');
});

/**
 * USE : GETTING MASTER DATASOURE DATA AND LISTING FOR SELECT DROPDOWN BASED ON USERSID 
 * MODULE : ALL PAGES COMMON
 */
router.get('/cmn_masterdata', async (req, res) => {

  var session_userid = new mongo.ObjectID(req.session.user.ses_userid);

  // if(req.session.user.ses_usertype == 1){
  //   var finder = {};
  // } else if(req.session.user.ses_usertype == 2){
  //   var finder = {};
  //   finder["created_by"] = session_userid;
  // }
  // var collection = dbUtil.get().collection(config.tbl_datasource_master).find(finder).sort({ "_id" : -1 });

  var collection = await dbUtil.get().collection(config.tbl_datasource_master).aggregate( [
    {
        $match : { $or : [
        { "created_by": session_userid }, 
        { "u_reporting_hierarchy" : session_userid} 
        ]} 
    },
    {   $sort : { "_id" : -1 } }
  ]);

  collection.toArray(function(err, master_data) {
    res.send(master_data);
  });

});

/**
 * MODULE : CHECKING MALFORM ACTION
 * USE : CHECKING GIVEN DATASOURCE ID IS VALID FOR THE CURRENT USERS
 */
router.get('/malformaction', async (req, res) => {
  res.render('errors/errors',{"domain_url":config.domain, "error_code":405, "error_mainmsg":"Something went wrong or illegal action, Given data is not valid!"});
});

/**
 * USE :: MONGO QUERY RUNNER
 * @param {*} params multiple value to pass in object 
 * @param {*} callback to send result
 */
function go_fndata_buildder(params, callback){

  var action = params.action;
  if(action == "search_dategetter") {
    var data_collection = params.collection;
    data_collection.toArray(function(err, master_data) {
      if(err) {
        console.log(err);
      } else {
        each(master_data, function(master_result, next_mover){
          next_mover(master_result["Transaction Date"]);
        },function(rtn_date){
          return callback(rtn_date);
        });
      }
    });
  }
}

/**
 * USE : DATEFORMAT CONVERSATION DATE FROM ISODATE
 */
function go_fnisotodate(iso_date){

  date = new Date(iso_date);
  year = date.getFullYear();
  month = date.getMonth()+1;
  dt = date.getDate()-1;
  return (dt+'-' + month + '-'+year);
}

/**
 * FUNCTION USE : LIST LOADER FOR ALL RISK TYPE TO GET GROUP BY CUSTOMER COUNT BASED ON HIGH, LOW, MEDIUM RISKS.
 * MODULE : OVERALL CUSTOMER RISK
 */
function qry_riskfactors(params, callback) {
  var datasource_subqry = params.datasource_subqry;
  var field_namevalue = new Object();

  field_namevalue[params.fieldname] = params.fieldvalue;
  field_namevalue["Transaction Date"] = { "$gt": params.fmdt_cnter, "$lt": params.todt_cnter };
  field_namevalue["data_proprietor"] = datasource_subqry.data_proprietor;

  var agent_data = params.collection.aggregate( [
    { $match: { 
        $and: [ field_namevalue ]
    } },
    { $group: { _id: "$Name", "Trans_Count":{ $sum: 1} } },
  ]);

  var gk = 0;

  agent_data.toArray(function(err, result_riskdata) {
    
    if (err) {
      console.log(err);
    } else {
      gk = result_riskdata.length;
      return callback(gk);
    }
  });  
}



module.exports = router;
