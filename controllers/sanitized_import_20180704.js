var express = require('express');
var router = express.Router();
var config = require('../config/database');
var dbUtil = require('../config/dbUtil')
var auth = require('../middlewares/auth'), chk_loginaccess = auth.chk_loginaccess;
var session = require('express-session');
var each = require('async-each');
var db_jsondriver = require('jsondbfs');
var Excel = require('exceljs');
var slash = require('slash');
var path = require('path');
var fs = require('fs');
var mongo = require('mongodb');
var multer  = require('multer');
var dateformat = require('dateformat');
var moment = require("moment");
//var CSV = require('csv');
//var textToJson = require('txt-csv-to-json');
// const csv=require('csvtojson')
var rowNumberValue;
var __dirname = (process.platform === 'win32') ? slash(__dirname) : __dirname;
var __dirname_upload =  __dirname.replace("/controllers", '/public/img/upload');
var _ = require('lodash');
var async=require('async');

var storage = multer.diskStorage({
	destination: function(req, file, callback) {
		callback(null, __dirname_upload)
	},
	filename: function(req, file, callback) {
    console.log(file)
    if(path.extname(file.originalname) == ".csv")
      var filename_without_extension = path.basename(file.originalname, '.csv');
    else
      var filename_without_extension = file.fieldname;

    callback(null, filename_without_extension + '-' + Date.now() + path.extname(file.originalname))
	}
});

/**
 * USE : ROOT LOADER TO INSERT DOCUMENT 
 * MODULE : DATASOURCE UPLOAD
 */
router.post('/insert_xl', chk_loginaccess, function (req, res, next) {
  
  // var upload = multer({
	// 	storage: storage
  // }).single('xl_upload');
  
	// upload(req, res, function(err) {

    // var xl_datasource_name = req.body.xl_datasource_name;
    // var files = req.file;
    // var upload_newpath = files.path;
    // var upload_filename = files.originalname;


    var xl_datasource_name = req.body.xl_datasource_name;
    var files = req.files[0];
    var upload_newpath = files.path;
    var upload_filename = files.originalname;
    
    if(!upload_filename || upload_filename.match(/\.(txt)$/i) || upload_filename.match(/\.(csv)$/i)) {

      console.log("uploaded file type was matching for current scenrio");

      if(upload_filename != '' && upload_filename != undefined) {

          const now = new Date();
          var nowtimestamp = now.getTime();

          var collection = dbUtil.get().collection('MSB_Datasource_master');

          var datasource_master_count = collection.aggregate([ { $group: { _id: null, count: { $sum: 1 } } } ] );

          datasource_master_count.toArray(function(err, datasource_master_count) {

            if(datasource_master_count.length > 0){
              var datasource_name_concater = datasource_master_count[0]["count"]+1;
            } else {
              var datasource_name_concater = 1;
            }
            var mongo = require('mongodb');
            var session_userid = new mongo.ObjectID(req.session.user.ses_userid);
            var session_orgid = new mongo.ObjectID(req.session.user.ses_organization);

            var ses_reporting_hierarchy = req.session.user.ses_reporting_hierarchy;

            new Promise(async(resolve, reject) => { 

              if(ses_reporting_hierarchy.length > 0){

                var ses_reporting_hierarchy_arr = new Array();
  
                each(ses_reporting_hierarchy, function(reporting_hierarchy_result, next_mover){
                  ses_reporting_hierarchy_arr.push(new mongo.ObjectID(reporting_hierarchy_result));
                  next_mover();
                },function(){
                  resolve(ses_reporting_hierarchy_arr);
                });
  
              } else {
                var ses_reporting_hierarchy_arr = new Array();
                resolve(ses_reporting_hierarchy_arr);
              }
            }).then(function(promise_reporting_hierarchy){

              // var isodate = new Date(new Date+' UTC').toISOString();
              var isodate = moment().format();
              var datasource_obj = {  
                ds_name: (xl_datasource_name).toUpperCase(), 
                ds_status: 1, 
                ds_filelocation:upload_newpath, 
                ds_inserted_totalrow:0, 
                ds_updatedate:isodate, 
                created_by: session_userid,
                organizationid: session_orgid,
                u_reporting_hierarchy : promise_reporting_hierarchy
              };

              collection.insertOne(datasource_obj, function(err, datasource_result) {
                  if (err) 
                    console.log(err);
                  else
                  {
                    if(datasource_result["ops"].length > 0) {

                      var datasource_objectid = datasource_result["ops"][0]["_id"];

                      var sanitized_collection = dbUtil.get().collection('MSB_Sanitized_Data'); 
                      excel_import_reader(sanitized_collection, upload_newpath, datasource_objectid, function(data){
                          res.send(data);
                      });      
      
                    } else {
                      res.send({"status":"failed", "message": 'something went wrong, when master datasource inserting..'});
                    }
                    
                  }
              });

            });
            
          });
      } else {
        console.log('file name is not undefined, its a required fields');
      }
    } else {
      console.log(upload_filename + ' is not allowed');
      res.send({"status":"failed", "message": upload_filename + ' is not allowed'});
    }

  // });

});


/**
 * FUNCTION USE : ROOT LOADER TO GETTING UPLOADED DATA USING EXCELREADER (XLSX ONLY ALLOWED)  
 * MODULE : DATASOURCE UPLOAD
 * This method is used to mappingcolumn value with imported file values modified by RameshLakshman
 */
function excel_import_reader(collection, new_filepath, datasource_objectid, callback)
{
  
  var workbook = new Excel.Workbook();
  var row_insert = new Array();
  var row_not_insert = new Array();

  var wb = workbook.csv.readFile(new_filepath);
  var worksheet = workbook.getWorksheet(1);
  var sheet_name = wb.SheetNames;

  var load_sanitizezd = new Array();
  var error_rowdetails_arr = new Array();
  var total_sheetrowscount = 0;
  var getcount = new Array();
  var check;

  /*Mapping field logic :xl header mapped with database column
  Modified by RameshLakshman */

  new Promise(function(resolve, reject) {

    dbUtil.get().collection('MSB_Column_mapping').find({}).toArray(function(err, master_data) {

      check = master_data[0]['mapping_field'];

      dbUtil.get().collection('MSB_Country_Mapping').find({}).toArray(function(err, result_nationality) {
  
        if(err)
          rejects(err)
        else {
          if(result_nationality.length > 0) {
            resolve(result_nationality);
          } else {
            rejects("Dont have data in MSB_Country_Mapping");
          }
        }
      });

    });

  }).then(function(result_nationality){
    
    workbook.csv.readFile(new_filepath).then(function(worksheet) {

      var worksheet = workbook.getWorksheet(1);
  
      if(worksheet.actualRowCount !== undefined && worksheet.actualRowCount > 0){
  
        var total_sheetrowscount = worksheet.actualRowCount;
  
        worksheet.eachRow({ includeEmpty: false }, function(row, rowNumber) {
  
          new Promise(function(spliter_resolve, spliter_reject) {
            const rowvalues = row.values;
            var risk_result = rowvalues.join().split('|');
            spliter_resolve(risk_result);
          }).then(function(risk_result){
            // console.log("____________one____________");
            if(risk_result.length < 20) {
              error_rowdetails_arr.push(rowNumber);
              if(total_sheetrowscount==rowNumber) {
                if(error_rowdetails_arr.length > 0){
                  return callback({"status":"unfinished", data:error_rowdetails_arr});
                }    
              }
            } else {
              // console.log("!!!!!!!!!!!!two!!!!!!!!!!!!!!!");
              // console.log(total_sheetrowscount);
              // console.log(rowNumber);
    
              var params_sanitized = {"datasource_objectid":datasource_objectid};
    
              prepare_sanitized_data(risk_result, params_sanitized, check, result_nationality, function(sanitized_data_obj){
    
                // console.log("##############three##############");
                
                load_sanitizezd.push(sanitized_data_obj);

                // console.log(total_sheetrowscount);
                // console.log(rowNumber);
    
                if(total_sheetrowscount==rowNumber) {
    
                  // console.log("$$$$$$$$$$$$four$$$$$$$$$$$$$$");
                  
                  if(error_rowdetails_arr.length > 0){
                    return callback({"status":"unfinished", data:error_rowdetails_arr});
                  } else{
                    if(error_rowdetails_arr.length == 0){
    
                      // console.log("============================================");
                      // console.log(load_sanitizezd.length);
    
                      collection.insertMany(load_sanitizezd, function(err, res) {
                        if (err) {
                          row_not_insert.push(rowNumber);
                          console.log(err);
                        } else {

                          // console.log(res);
                          row_insert.push(rowNumber);
    
                          if(total_sheetrowscount == rowNumber && error_rowdetails_arr.length == 0){
    
                            var master_collection = dbUtil.get().collection('MSB_Datasource_master');
                            var mongo = require('mongodb');
                            var sanitized_datasource_id = new mongo.ObjectID(datasource_objectid);
                            // console.log("datasource_objectid"+datasource_objectid);
    
                            master_collection.update( { "_id" : sanitized_datasource_id }, { 
                              $set: { 
                                "ds_inserted_totalrow": load_sanitizezd.length,
                              } 
                            });
                            return callback({"status":"finished", "ttl_insert":row_insert.length, "ttl_percentage": ((row_insert.length/total_sheetrowscount)*100), "datasource_objectid" : datasource_objectid });
                          }
                        }
                      });
                    }
                  }
                }
              });
            }
          });
          
        });
      }
    });
  });

}

/**
 * FUNCTION USE : ROOT LOADER TO AFTER GOT THE UPLOADED DATA SEGREGATION WITH DATA FORMATING   1(NO) TO 22(OUTLET)
 * MODULE : DATASOURCE UPLOAD
 */
function prepare_sanitized_data(risk_result, params_sanitized, mp_check, result_nationality, callback){
  
  new Promise(function(resolve, rejects) {

    let finder_nationality = risk_result[5].toUpperCase();
    if(result_nationality.length > 0) {

      _.filter(result_nationality, function(o) { 
        if(o.alpha_two === finder_nationality){
          resolve(o.nationality);
        }
      });  
      
    } else {
      resolve(risk_result[5]);
    }

  }).then(function(nationality){

    new Promise(function(prepare_resolve, prepare_rejects) {
      
      var datasource_objectid = params_sanitized.datasource_objectid;
      var sanitized_data_obj = {};
      var risk_nationality = (nationality !== undefined && nationality != "") ? nationality.toUpperCase() : "";

      var mom_date = moment(risk_result[10],'YYYYMMDD').format('YYYY/MM/DD');
      if(isNaN(mom_date)){
        var mom_month = moment(risk_result[10],'YYYYMMDD').format('YYYY/MM/DD');
        var unixTimeZero = Date.parse(mom_date);
        var correct_dateformate= new Date(unixTimeZero);
        var final_str_date = dateformat(correct_dateformate, "UTC:m-d-yyyy h:MM:ss TT Z");
        var final_date = new Date(final_str_date);
      }else{
        var unixTimeZero = mom_date;
        var correct_dateformate= new Date(unixTimeZero);
        var final_str_date = dateformat(correct_dateformate, "UTC:m-d-yyyy h:MM:ss TT Z");
        var final_date = new Date(final_str_date);
      } 
      sanitized_data_obj[mp_check["0"]] = parseInt(risk_result[0].replace(/[^0-9]/g, ''));
      sanitized_data_obj[mp_check["1"]] = risk_result[1];
      sanitized_data_obj[mp_check["2"]] = risk_result[2];
      sanitized_data_obj[mp_check["3"]] = risk_result[3];
      sanitized_data_obj[mp_check["10"]] =  (risk_result[4] != undefined) ? final_date : "";
      sanitized_data_obj[mp_check["9"]] = risk_result[9];
      if(risk_result[11] == "Sell"){
        sanitized_data_obj[mp_check["11"]] = "WeSell";
      } else if(risk_result[11] == "Buy"){
        sanitized_data_obj[mp_check["11"]] = "WeBuy";
      } else {
        sanitized_data_obj[mp_check["11"]] = risk_result[11];
      }
      sanitized_data_obj[mp_check["12"]] = risk_result[12];
      sanitized_data_obj[mp_check["13"]] = parseFloat(risk_result[13]);
      sanitized_data_obj[mp_check["14"]] = parseFloat(risk_result[14]);
      sanitized_data_obj[mp_check["15"]] = parseFloat(risk_result[15]);
      sanitized_data_obj[mp_check["6"]] = risk_result[6];      
      sanitized_data_obj[mp_check["5"]] = risk_nationality;
      sanitized_data_obj[mp_check["4"]] = risk_result[4];
      sanitized_data_obj[mp_check["8"]] = (risk_result[8]);
      sanitized_data_obj[mp_check["16"]] = risk_result[16];
      sanitized_data_obj[mp_check["17"]] = risk_result[17];
      sanitized_data_obj[mp_check["7"]] = risk_result[7];    
      sanitized_data_obj["Mode of Payment"] = "CASH";
      sanitized_data_obj["Mode of Delivery"] = "OVER THE COUNTER";
      sanitized_data_obj["Value Risk Score"] = 0;
      sanitized_data_obj["Nationality risk Score"] = 0;
      sanitized_data_obj["Customer Type Risk Score"] = 0;
      sanitized_data_obj["Purpose Risk Score"] = 0;
      sanitized_data_obj["Source Risk score"] = 0;
      sanitized_data_obj["Occupation Risk Score"] = 0;
      sanitized_data_obj["Mode of Payment Risk Score"] = 0;
      sanitized_data_obj["Mode of Delivery Risk Score"] = 0;
      sanitized_data_obj["Total Risk Score"] = 0;
      sanitized_data_obj["datasource_objectid"] = datasource_objectid;
      sanitized_data_obj["datasource_status"] = 1;
      sanitized_data_obj["common_risk_satatus"] = {
                                                    "risk_value":0,  "risk_nationality":0, 
                                                    "risk_customertype":0,  "risk_purpose":0, 
                                                    "risk_source":0,  "risk_occupation":0, 
                                                    "risk_payment":0,  "risk_delivery":0
                                                  };
      sanitized_data_obj["Action Taken"] = "",
      sanitized_data_obj["Remarks"] = "";
      prepare_resolve(sanitized_data_obj);
    }).then(function(sanitized_data_obj){
      return callback(sanitized_data_obj);
    });
    
  });
}

/**
 * USE : AFTER DATA INSERT USE THIS API TO CALCULATE THE RISK CUMULATION
 * MODULE : DATASOURCE UPLOAD
 */
router.get('/cumlative_risk', function(req, res, next) {

  var sanitized_collection = dbUtil.get().collection('MSB_Sanitized_Data');
  var action_risk = req.query.request_action;
  var sanitized_datasource_id = new mongo.ObjectID(req.query.request_datasourceid);

  var params_sanitized = {"action_risk":action_risk,"sanitized_collection":sanitized_collection, "master_datasource_objectid":sanitized_datasource_id};

  cmn_risktype_follow(params_sanitized, function(excuted_final_sanitized_data){
    res.send(excuted_final_sanitized_data);
  });

});

/**
 * FUNCTION USE : FOR THE RISK CUMULATION PROCESS PARAMS PASSING BASED ON RISKTYPE
 * MODULE : DATASOURCE UPLOAD
 */
function cmn_risktype_follow(params, callback){

  var action_risk = params.action_risk;
  var sanitized_datasource_id = params.master_datasource_objectid;
  var sanitized_collection = params.sanitized_collection;

  if(action_risk == "RSK_VALUE") {
    var risk_params = {"collection":sanitized_collection, "action":"risk_value", "sanitized_datasource_id":sanitized_datasource_id};
  } else if(action_risk == "RSK_NATIONALITY") {
    var risk_params = {"collection":sanitized_collection, "action":"risk_nationality", "sanitized_datasource_id":sanitized_datasource_id};
  } else if(action_risk == "RSK_CUSTOMERTYPE") {
    var risk_params = {"collection":sanitized_collection, "action":"risk_customertype", "sanitized_datasource_id":sanitized_datasource_id};
  } else if(action_risk == "RSK_PURPOSE") {
    var risk_params = {"collection":sanitized_collection, "action":"risk_purpose", "sanitized_datasource_id":sanitized_datasource_id};
  } else if(action_risk == "RSK_SOURCE") {
    var risk_params = {"collection":sanitized_collection, "action":"risk_source", "sanitized_datasource_id":sanitized_datasource_id};
  } else if(action_risk == "RSK_OCCUPATION") {
    var risk_params = {"collection":sanitized_collection, "action":"risk_occupation", "sanitized_datasource_id":sanitized_datasource_id};
  } else if(action_risk == "RSK_MODEOFPAYMENT") {
    var risk_params = {"collection":sanitized_collection, "action":"risk_modeofpayment", "sanitized_datasource_id":sanitized_datasource_id};
  } else if(action_risk == "RSK_MODEOFDELIVERY") {
    var risk_params = {"collection":sanitized_collection, "action":"risk_modeofdelivery", "sanitized_datasource_id":sanitized_datasource_id};
  } else{
    res.send({"status":false, "message":"risk type doesn't match!!!"});
  }
  
  funcmn_calculate_riskfactories(risk_params, function(final_sanitized_data){
    return callback(final_sanitized_data);
  });

}

/**
 * FUNCTION USE : CALCULATING ALL KIND OF RISK FACTORIES AND UPDATING TOTALRISK, RISKSTATUS, RISK POINT IN SANTIZED TABLE
 * MODULE : DATASOURCE UPLOAD
 */
function funcmn_calculate_riskfactories(params, callback){

  var collection = params.collection;
  var action = params.action;
  var sanitized_datasource_id = params.sanitized_datasource_id;

  if(action == "risk_value") {

    var report_data = collection.find( 
        { "datasource_objectid": sanitized_datasource_id, "Value Risk Score":0, "Exchange Amount": { "$ne": "" }, "common_risk_satatus.risk_value": 0 }, 
        { "No":1, "Exchange Amount":1, "Total Risk Score":1, "common_risk_satatus":1}).limit(15000);

    report_data.toArray(function(err, report_result) {

      // return callback(report_result);

      if(report_result.length > 0) {

        var msb_value_arr = [];
        each(report_result, function(data, next_moveer){

          var msb_value_data = dbUtil.get().collection('MSB_Value').find({"VALUE END" : {$gt: data["Exchange Amount"]} },{ "VALUE RISK SCORE" : 1}).limit(1);

          msb_value_data.toArray(function(err, msb_value_result) {

            if(err){
              console.log(err);
              next_moveer();
            } else {
              function sanitized_update(msb_value_arr, data, msb_value_result, callback){

                // console.log("risk_value");
                // console.log(msb_value_result);
                // console.log(data["Exchange Amount"]);
              
                if(msb_value_result === undefined){
                  var risk_value_point = 1;
                } else {
                  var risk_value_point = (msb_value_result.length > 0 ) ? msb_value_result[0]["VALUE RISK SCORE"] : 1;
                }
                var total_risk_score = parseInt(parseInt(risk_value_point) + parseInt(data["Total Risk Score"]));

                function msd_update_collection(collection, id, risk_point, total_risk_score, callback){

                  var promise3 = new Promise(function(resolve, reject) {

                    var coll_update = collection.update( { "_id" : id }, { 
                      $set: { 
                        "Value Risk Score": parseInt(risk_point),
                        "Total Risk Score" : total_risk_score,
                        "common_risk_satatus.risk_value":1                  
                      } 
                    });
                    resolve(coll_update);
                  });

                  Promise.all([promise3]).then(function(values) {
                    return callback();
                  });
                }

                msd_update_collection(collection, data["_id"], risk_value_point, total_risk_score, function(){

                  var temp_msbvalue_arr = new Array();
                  temp_msbvalue_arr.push(data["_id"]);
                  temp_msbvalue_arr.push(data["Exchange Amount"]);
                  temp_msbvalue_arr.push(parseInt(risk_value_point));
                  temp_msbvalue_arr.push(parseInt(total_risk_score));
                  msb_value_arr.push(temp_msbvalue_arr);
                  return callback();
                });
              }
              sanitized_update(msb_value_arr, data, msb_value_result, function(){
                next_moveer();
              });
            }
          });

        },function(){
          var access_purpose = new Array();
          // var access = {status:"gorisk", data:msb_value_arr};
          var access = {status:"gorisk"};
          access_purpose.push(access);
          return callback(access_purpose);
        });
      } else {
          return callback({"status":false, "message":"sanitized record was not found!!!"});
      }
    });
  
  } else if(action == "risk_nationality") {
    
    var report_data = collection.find( 
      { "datasource_objectid": sanitized_datasource_id, "Nationality risk Score":0, "Nationality": { "$ne": "" }, "common_risk_satatus.risk_nationality": 0 }, 
      { "Nationality":1, "Total Risk Score":1, "common_risk_satatus":1}).limit(15000);
    
      report_data.toArray(function(err, report_result) {

      //return callback(report_result);

      if(report_result.length > 0) {

        var msb_nationality_arr = [];

        each(report_result, function(data, next_moveer){

          if(data["Nationality"] != ""){

            var nationality_regex = new RegExp(["^", data["Nationality"], "$"].join(""), "i");
            var msb_nationality_data = dbUtil.get().collection('MSB_Nationality').find({ "Nationality": nationality_regex },{ "Nationality Risk Score" : 1}).limit(1);

            msb_nationality_data.toArray(function(err, msb_nationality_result) {

              if(err){
                console.log(err);
                next_moveer();
              } else {
                function sanitized_update(msb_value_arr, data, msb_value_result, callback) {

                  // console.log("risk_nationality");
                  // console.log(msb_value_result);
                  // console.log(nationality_regex);
                  
                  if(msb_value_result === undefined){
                    var risk_value_point = 1;
                  } else {
                    var risk_value_point = (msb_value_result.length > 0 ) ? msb_value_result[0]["Nationality Risk Score"] : 1;
                  }
                  var total_risk_score = parseInt(parseInt(risk_value_point) + parseInt(data["Total Risk Score"]));
    
                  function msd_update_collection(collection, id, risk_point, total_risk_score, callback){
    
                    var promise3 = new Promise(function(resolve, reject) {

                      var coll_update = collection.update( { "_id" : id }, { 
                        $set: { 
                          "Nationality risk Score": parseInt(risk_point),
                          "Total Risk Score" : total_risk_score,
                          "common_risk_satatus.risk_nationality":1                  
                        } 
                      });

                      resolve(coll_update);
                    });
    
                    Promise.all([promise3]).then(function(values) {
                      return callback();
                    });
                  }
    
                  msd_update_collection(collection, data["_id"], risk_value_point, total_risk_score, function(){

                    var temp_msbvalue_arr = new Array();
                    temp_msbvalue_arr.push(data["_id"]);
                    temp_msbvalue_arr.push(data["Nationality"]);
                    temp_msbvalue_arr.push(parseInt(risk_value_point));
                    temp_msbvalue_arr.push(parseInt(total_risk_score));
                    msb_value_arr.push(temp_msbvalue_arr);
                    return callback();
                  });
                }
                sanitized_update(msb_nationality_arr, data, msb_nationality_result, function(){
                  next_moveer();
                });
              }
            });
          } else {
            next_moveer();
          }

        },function(){
          var access_purpose = new Array();
          // var access = {status:"gorisk", data:msb_nationality_arr};
          var access = {status:"gorisk"};
          access_purpose.push(access);
          return callback(access_purpose);
        });

      } else {
        return callback({"status":false, "message":"sanitized record was not found!!!"});
      }
    });
  } else if(action == "risk_customertype") {

    var report_data = collection.find( 
      { "datasource_objectid": sanitized_datasource_id, "Customer Type Risk Score":0, "Customer Type": { "$ne": "" }, "common_risk_satatus.risk_customertype": 0 }, 
      { "Customer Type":1, "Total Risk Score":1, "common_risk_satatus":1}).limit(15000);
    
    report_data.toArray(function(err, report_result) {

      // return callback(report_result);

      if(report_result.length > 0) {

        var msb_customertype_arr = [];

        each(report_result, function(data, next_moveer){

          if(data["Customer Type"] != "") {

            let finder_customertype = data["Customer Type"].toUpperCase();

            var customertype_regex = new RegExp(["^", finder_customertype, "$"].join(""), "i");
            var msb_customertype_data = dbUtil.get().collection('MSB_Customer_Type').find({ "CUSTOMER BUSINESS": customertype_regex },{ "CUSTOMER BUSINESS RISK SCORE" : 1}).limit(1);

            msb_customertype_data.toArray(function(err, msb_customertype_result) {

              if(err){
                console.log(err);
                next_moveer();
              } else {
                function sanitized_update(msb_value_arr, data, msb_value_result, callback) {

                  // console.log("risk_customertype");
                  // console.log(msb_value_result);
                  // console.log(customertype_regex);

                  if(msb_value_result === undefined){
                    var risk_value_point = 1;
                  } else {
                    var risk_value_point = (msb_value_result.length > 0 ) ? msb_value_result[0]["CUSTOMER BUSINESS RISK SCORE"] : 1;
                  }
                  var total_risk_score = parseInt(parseInt(risk_value_point) + parseInt(data["Total Risk Score"]));
    
                  function msd_update_collection(collection, id, risk_point, total_risk_score, callback){

                    var promise3 = new Promise(function(resolve, reject) {

                      var coll_update = collection.update( { "_id" : id }, { 
                        $set: { 
                          "Customer Type Risk Score": parseInt(risk_point),
                          "Total Risk Score" : total_risk_score,
                          "common_risk_satatus.risk_customertype":1                  
                        } 
                      });

                      resolve(coll_update);
                    });
    
                    Promise.all([promise3]).then(function(values) {
                      return callback();
                    });
                  }
    
                  msd_update_collection(collection, data["_id"], risk_value_point, total_risk_score, function(){

                    var temp_msbvalue_arr = new Array();
                    temp_msbvalue_arr.push(data["_id"]);
                    temp_msbvalue_arr.push(data["Customer Type"]);
                    temp_msbvalue_arr.push(parseInt(risk_value_point));
                    temp_msbvalue_arr.push(parseInt(total_risk_score));
        
                    msb_value_arr.push(temp_msbvalue_arr);
                    return callback();
                  });
                }
                sanitized_update(msb_customertype_arr, data, msb_customertype_result, function(){
                  next_moveer();
                });
              }
            });
          } else {
            next_moveer();
          }

        },function(){
          var access_purpose = new Array();
          // var access = {status:"gorisk", data:msb_customertype_arr};
          var access = {status:"gorisk"};
          access_purpose.push(access);
          return callback(access_purpose);
        });

      } else {
        return callback({"status":false, "message":"sanitized record was not found!!!"});
      }
    });

  } else if(action == "risk_purpose") {

    var report_data = collection.find( 
      {  "datasource_objectid": sanitized_datasource_id, "Purpose Risk Score":0, Purpose: { "$ne": "" }, "common_risk_satatus.risk_purpose": 0  }, 
      { "Purpose":1, "Total Risk Score":1, "common_risk_satatus":1}).limit(15000);

    report_data.toArray(function(err, report_result) {

      // return callback(report_result);

      if(report_result.length > 0) {

        var msb_purpose_arr = [];
        each(report_result, function(data, next_moveer){

          if(data["Purpose"] != "") {

            let finder_purpose = data["Purpose"].toUpperCase();
            var purpose_regex = new RegExp(["^", finder_purpose, "$"].join(""), "i");
            var msb_purpose_data = dbUtil.get().collection('MSB_Purpose').find({ "PURPOSE": purpose_regex },{ "PURPOSE RISK SCORE" : 1}).limit(1);

            msb_purpose_data.toArray(function(err, msb_purpose_result) {

              if(err){
                console.log(err);
                next_moveer();
              } else {
                function sanitized_update(msb_value_arr, data, msb_value_result, callback) {

                  // console.log("risk_purpose");
                  // console.log(msb_value_result);
                  // console.log(purpose_regex);
                  
                  if(msb_value_result === undefined){
                    var risk_value_point = 1;
                  } else {
                    var risk_value_point = (msb_value_result.length > 0 ) ? msb_value_result[0]["PURPOSE RISK SCORE"] : 1;
                  }
                  var total_risk_score = parseInt(parseInt(risk_value_point) + parseInt(data["Total Risk Score"]));
    
                  function msd_update_collection(collection, id, risk_point, total_risk_score, callback){

                    var promise3 = new Promise(function(resolve, reject) {

                      var coll_update = collection.update( { "_id" : id }, { 
                        $set: { 
                          "Purpose Risk Score": parseInt(risk_point),
                          "Total Risk Score" : total_risk_score,
                          "common_risk_satatus.risk_purpose":1                  
                        } 
                      });

                      resolve(coll_update);
                    });
    
                    Promise.all([promise3]).then(function(values) {
                      return callback();
                    });
                  }
    
                  msd_update_collection(collection, data["_id"], risk_value_point, total_risk_score, function(){

                    var temp_msbvalue_arr = new Array();
                    temp_msbvalue_arr.push(data["_id"]);
                    temp_msbvalue_arr.push(data["Purpose"]);
                    temp_msbvalue_arr.push(parseInt(risk_value_point));
                    temp_msbvalue_arr.push(parseInt(total_risk_score));
          
                    msb_value_arr.push(temp_msbvalue_arr);
                    return callback();
                  });
                }
                sanitized_update(msb_purpose_arr, data, msb_purpose_result, function(){
                  next_moveer();
                });
              }
            });
          } else {
            next_moveer();
          }

        },function(){
          var access_purpose = new Array();
          // var access = {status:"gorisk", data:msb_purpose_arr};
          var access = {status:"gorisk"};
          access_purpose.push(access);
          return callback(access_purpose);
        });

      } else {
        return callback({"status":false, "message":"sanitized record was not found!!!"});
      }
    });

  } else if(action == "risk_source") {

    var report_data = collection.find( 
      {  "datasource_objectid": sanitized_datasource_id, "Source Risk score":0, "Source": { "$ne": "" }, "common_risk_satatus.risk_source": 0  }, 
      { "Source":1, "Total Risk Score":1, "common_risk_satatus":1}).limit(15000);

    report_data.toArray(function(err, report_result) {

      // return callback(report_result);

      if(report_result.length > 0) {

        var msb_source_arr = [];
        each(report_result, function(data, next_moveer){

          if(data["Source"] != "") { 

            let finder_source = data["Source"].toUpperCase();
            //var source_regex = new RegExp(["^", data["Source"], "$"].join(""), "i");
            var source_regex = finder_source.replace(/[^a-zA-Z0-9 ]/g, "");
            var msb_source_data = dbUtil.get().collection('MSB_Source').find({ "SOURCE": source_regex },{ "SOURCE RISK SCORE" : 1}).limit(1);

            msb_source_data.toArray(function(err, msb_source_result) {

              if(err){
                console.log(err);
                next_moveer();
              } else {
                function sanitized_update(msb_value_arr, data, msb_value_result, callback) {

                  // console.log("source_regex");
                  // console.log(msb_value_result);
                  // console.log(source_regex);
                  
                  if(msb_value_result === undefined){
                    var risk_value_point = 1;
                  } else {
                    var risk_value_point = (msb_value_result.length > 0 ) ? msb_value_result[0]["SOURCE RISK SCORE"] : 1;
                  }
                  var total_risk_score = parseInt(parseInt(risk_value_point) + parseInt(data["Total Risk Score"]));
    
                  function msd_update_collection(collection, id, risk_point, total_risk_score, callback){

                    var promise3 = new Promise(function(resolve, reject) {

                      var coll_update = collection.update( { "_id" : id }, { 
                        $set: { 
                          "Source Risk score": parseInt(risk_point),
                          "Total Risk Score" : total_risk_score,
                          "common_risk_satatus.risk_source":1                  
                        } 
                      });

                      resolve(coll_update);
                    });
    
                    Promise.all([promise3]).then(function(values) {
                      return callback();
                    });
                  }
    
                  msd_update_collection(collection, data["_id"], risk_value_point, total_risk_score, function(){

                    var temp_msbvalue_arr = new Array();
                    temp_msbvalue_arr.push(data["_id"]);
                    temp_msbvalue_arr.push(data["Source"]);
                    temp_msbvalue_arr.push(parseInt(risk_value_point));
                    temp_msbvalue_arr.push(parseInt(total_risk_score));
                    
                    msb_value_arr.push(temp_msbvalue_arr);
                    return callback();
                  });
                }
                sanitized_update(msb_source_arr, data, msb_source_result, function(){
                  next_moveer();
                });
              }

            });
          } else {
            next_moveer();
          }

        },function(){
          var access_purpose = new Array();
          // var access = {status:"gorisk", data:msb_source_arr};
          var access = {status:"gorisk"};
          access_purpose.push(access);
          return callback(access_purpose);
        });

      } else {
        return callback({"status":false, "message":"sanitized record was not found!!!"});
      }
    });

  } else if(action == "risk_occupation") {

    var report_data = collection.find( 
      {  "datasource_objectid": sanitized_datasource_id, "Occupation Risk Score":0, "Occupation": { "$ne": "" }, "common_risk_satatus.risk_occupation": 0  }, 
      { "Occupation":1, "Total Risk Score":1, "common_risk_satatus":1}).limit(15000);

    report_data.toArray(function(err, report_result) {

      // return callback(report_result);

      if(report_result.length > 0) {

        var msb_occupation_arr = [];

        each(report_result, function(data, next_moveer){

          if(data["Occupation"] != "")
          {
            var occupation_regex = new RegExp(["^", data["Occupation"], "$"].join(""), "i");
            
            var msb_occupation_data = dbUtil.get().collection('MSB_Occupation').find({ "Occupation": occupation_regex },{ "Occupation Risk Score" : 1}).limit(1);

            msb_occupation_data.toArray(function(err, msb_occupation_result) {

              if(err){
                console.log(err);
                next_moveer();
              } else {
                function sanitized_update(msb_value_arr, data, msb_value_result, callback) {

                  // console.log("occupation_regex");
                  // console.log(msb_value_result);
                  // console.log(occupation_regex);
                  
                  if(msb_value_result === undefined){
                    var risk_value_point = 1;
                  } else {
                    var risk_value_point = (msb_value_result.length > 0 ) ? msb_value_result[0]["Occupation Risk Score"] : 1;
                  }
                  var total_risk_score = parseInt(parseInt(risk_value_point) + parseInt(data["Total Risk Score"]));
    
                  function msd_update_collection(collection, id, risk_point, total_risk_score, callback){

                    var promise3 = new Promise(function(resolve, reject) {

                      var coll_update = collection.update( { "_id" : id }, { 
                        $set: { 
                          "Occupation Risk Score": parseInt(risk_point),
                          "Total Risk Score" : total_risk_score,
                          "common_risk_satatus.risk_occupation":1                  
                        } 
                      });

                      resolve(coll_update);
                    });
    
                    Promise.all([promise3]).then(function(values) {
                      return callback();
                    });
                  }
    
                  msd_update_collection(collection, data["_id"], risk_value_point, total_risk_score, function(){

                    var temp_msbvalue_arr = new Array();
                    temp_msbvalue_arr.push(data["_id"]);
                    temp_msbvalue_arr.push(data["Occupation"]);
                    temp_msbvalue_arr.push(parseInt(risk_value_point));
                    temp_msbvalue_arr.push(parseInt(total_risk_score));
                      
                    msb_value_arr.push(temp_msbvalue_arr);
                    return callback();
                  });
                }
                sanitized_update(msb_occupation_arr, data, msb_occupation_result, function(){
                  next_moveer();
                });
              }
            });
          } else {
            next_moveer();
          }

        },function(){
          var access_purpose = new Array();
          // var access = {status:"gorisk", data:msb_occupation_arr};
          var access = {status:"gorisk"};
          access_purpose.push(access);
          return callback(access_purpose);
        });

      } else {
        return callback({"status":false, "message":"sanitized record was not found!!!"});
      }
    });

  } else if(action == "risk_modeofpayment") {

    var report_data = collection.find( 
      {  "datasource_objectid": sanitized_datasource_id, "Mode of Payment Risk Score":0, "Mode of Payment": { "$ne": "" }, "common_risk_satatus.risk_payment": 0  }, 
      { "Mode of Payment":1, "Total Risk Score":1, "common_risk_satatus":1}).limit(15000);

    report_data.toArray(function(err, report_result) {

      // return callback(report_result);

      if(report_result.length > 0) {

        var msb_payment_arr = [];

        each(report_result, function(data, next_moveer){

          if(data["Mode of Payment"] != "")
          {
            var payment_regex = new RegExp(["^", data["Mode of Payment"], "$"].join(""), "i");

            var msb_payment_data = dbUtil.get().collection('MSB_Mode_of_Payment').find({ "Mode of Payment": payment_regex },{ "Mode of Payment Risk Score" : 1}).limit(1);

            msb_payment_data.toArray(function(err, msb_payment_result) {

              if(err){
                console.log(err);
                next_moveer();
              } else {
                function sanitized_update(msb_value_arr, data, msb_value_result, callback) {

                  // console.log("payment_regex");
                  // console.log(msb_value_result);
                  // console.log(payment_regex);
                  
                  if(msb_value_result === undefined){
                    var risk_value_point = 1;
                  } else {
                    var risk_value_point = (msb_value_result.length > 0 ) ? msb_value_result[0]["Mode of Payment Risk Score"] : 1;
                  }
                  var total_risk_score = parseInt(parseInt(risk_value_point) + parseInt(data["Total Risk Score"]));
    
                  function msd_update_collection(collection, id, risk_point, total_risk_score, callback){

                    var promise3 = new Promise(function(resolve, reject) {

                      var coll_update = collection.update( { "_id" : id }, { 
                        $set: { 
                          "Mode of Payment Risk Score": parseInt(risk_point),
                          "Total Risk Score" : total_risk_score,
                          "common_risk_satatus.risk_payment":1                  
                        } 
                      });

                      resolve(coll_update);
                    });
    
                    Promise.all([promise3]).then(function(values) {
                      return callback();
                    });
                  }
    
                  msd_update_collection(collection, data["_id"], risk_value_point, total_risk_score, function(){

                    var temp_msbvalue_arr = new Array();
                    temp_msbvalue_arr.push(data["_id"]);
                    temp_msbvalue_arr.push(data["Mode of Payment"]);
                    temp_msbvalue_arr.push(parseInt(risk_value_point));
                    temp_msbvalue_arr.push(parseInt(total_risk_score));

                    msb_value_arr.push(temp_msbvalue_arr);
                    return callback();
                  });
                }
                sanitized_update(msb_payment_arr, data, msb_payment_result, function(){
                  next_moveer();
                });
              }
            });
          } else {
            next_moveer();
          }

        },function(){
          var access_purpose = new Array();
          // var access = {status:"gorisk", data:msb_payment_arr};
          var access = {status:"gorisk"};
          access_purpose.push(access);
          return callback(access_purpose);
        });

      } else {
        return callback({"status":false, "message":"sanitized record was not found!!!"});
      }
    });
  } else if(action == "risk_modeofdelivery") {

    var report_data = collection.find( 
      {  "datasource_objectid": sanitized_datasource_id, "Mode of Delivery Risk Score":0, "Mode of Delivery": { "$ne": "" }, "common_risk_satatus.risk_delivery": 0  }, 
      { "Mode of Delivery":1, "Total Risk Score":1, "common_risk_satatus":1}).limit(15000);

    report_data.toArray(function(err, report_result) {

      // return callback(report_result);

      if(report_result.length > 0) {

        var msb_delivery_arr = [];

        each(report_result, function(data, next_moveer){

          if(data["Mode of Delivery"] != "")
          {
            var delivery_regex = new RegExp(["^", data["Mode of Delivery"], "$"].join(""), "i");

            var msb_delivery_data = dbUtil.get().collection('MSB_Mode_of_Delivery').find({ "Mode of Delivery": delivery_regex },{ "Model of Delivery Score" : 1}).limit(1);

            msb_delivery_data.toArray(function(err, msb_delivery_result) {

              if(err){
                console.log(err);
                next_moveer();
              } else {
                function sanitized_update(msb_value_arr, data, msb_value_result, callback) {

                  // console.log("delivery_regex");
                  // console.log(msb_value_result);
                  // console.log(delivery_regex);
                  
                  if(msb_value_result === undefined){
                    var risk_value_point = 1;
                  } else {
                    var risk_value_point = (msb_value_result.length > 0 ) ? msb_value_result[0]["Model of Delivery Score"] : 1;
                  }
                  var total_risk_score = parseInt(parseInt(risk_value_point) + parseInt(data["Total Risk Score"]));
    
                  function msd_update_collection(collection, id, risk_point, total_risk_score, callback){

                    var promise3 = new Promise(function(resolve, reject) {

                      var coll_update = collection.update( { "_id" : id }, { 
                        $set: { 
                          "Mode of Delivery Risk Score": parseInt(risk_point),
                          "Total Risk Score" : total_risk_score,
                          "common_risk_satatus.risk_delivery":1                  
                        } 
                      });

                      resolve(coll_update);
                    });
    
                    Promise.all([promise3]).then(function(values) {
                      return callback();
                    });
                  }
    
                  msd_update_collection(collection, data["_id"], risk_value_point, total_risk_score, function(){

                    var temp_msbvalue_arr = new Array();
                    temp_msbvalue_arr.push(data["_id"]);
                    temp_msbvalue_arr.push(data["Mode of Delivery"]);
                    temp_msbvalue_arr.push(parseInt(risk_value_point));
                    temp_msbvalue_arr.push(parseInt(total_risk_score));
    
                    msb_value_arr.push(temp_msbvalue_arr);
                    return callback();
                  });
                }
                sanitized_update(msb_delivery_arr, data, msb_delivery_result, function(){
                  next_moveer();
                });
              }

            });
          } else {
            next_moveer();
          }

        },function(){
          var access_purpose = new Array();
          // var access = {status:"gorisk", data:msb_delivery_arr};
          var access = {status:"gorisk"};
          access_purpose.push(access);
          return callback(access_purpose);
        });

      } else {
        return callback({"status":false, "message":"sanitized record was not found!!!"});
      }
    });
 
  } else {
    return callback({"status":false, "message":"sanitized action was not found!!!"});
  }
}

/**
 * USE : ROOT LOADER FOR MASTER DATASOURCE LISTING
 * MODULE : DATASOURCE UPLOAD
 * MAIN : http://192.168.1.187:3000/sanitized_import/xlupload
 */
router.get('/masterdatasource_lister', async (req, res, next) => {

  var mongo = require('mongodb');
  var session_userid = new mongo.ObjectID(req.session.user.ses_userid);

  if(req.session.user.ses_usertype == 1){
    var finder = {};
  } else if(req.session.user.ses_usertype == 2){
    var finder = {};
    finder["created_by"] = session_userid;
  }

  var collection = await dbUtil.get().collection('MSB_Datasource_master').aggregate( [
      {
          $match : { $or : [
          { "created_by": session_userid }, 
          { "u_reporting_hierarchy" : session_userid} 
          ]} 
      },
      {   $sort : { "_id" : -1 } }
    ]);

  // var collection = dbUtil.get().collection('MSB_Datasource_master').find(finder).sort({ "_id" : -1 });

  collection.toArray(function(err, master_data) {

    var master_datasource_arr = new Array();

    if(master_data.length > 0){

      var count = 1;

      each(master_data, function(data, next_moveer){

        var temp_master_arr = [];
        
        temp_master_arr.push(count);
        temp_master_arr.push(data["_id"]);
        temp_master_arr.push(data["ds_name"]);
        temp_master_arr.push(data["ds_filelocation"]);
        temp_master_arr.push(data["ds_inserted_totalrow"]);
        temp_master_arr.push(data["ds_updatedate"]);
        
        master_datasource_arr.push(temp_master_arr);
        count++;
        next_moveer();
        
      },function(){
        return res.send(master_datasource_arr);
      });

    } else {
      return res.send(master_datasource_arr);
    }

  });

}); 

/**
 * USE : ROOT LOADER FOR MASTER DATASOURCE DELETE OPTION
 * MODULE : DATASOURCE UPLOAD
 * MAIN : http://192.168.1.187:3000/sanitized_import/xlupload
 */
router.get('/masterdatasource_remove', function(req, res, next) {

  var datasource_objid = req.query.datasource_objid || "";

  if(datasource_objid != "" && datasource_objid != null && datasource_objid != undefined && datasource_objid.length !== 0) {
    var mongo = require('mongodb');
    var sanitized_datasource_id = new mongo.ObjectID(datasource_objid);
    var datasource_subqry =  {"_id": sanitized_datasource_id};
    var sanitized_subqry =  {"datasource_objectid": sanitized_datasource_id};
 
    var collection = dbUtil.get().collection('MSB_Datasource_master').remove(datasource_subqry, function(err, remove_result){

      if(err){
        res.send({"status":false, "message":"somthing went wrong on master data remove, kindly check it."})
      } else {
        var collection = dbUtil.get().collection('MSB_Sanitized_Data').remove(sanitized_subqry, function(err, remove_result){

          if(err){
            res.send({"status":false, "message":"somthing went wrong on master data remove, kindly check it."})
          } else {
            res.send({"status":true, "message":"Deleted sucessfully"})
          }
    
        });
      }

    });
    
  } else {
    res.send({"status":false, "message":"data source id required, kindly check it."})
  }

}); 

/**
 * USE : ROOT LOADER OF CHECKING MASTER DATASOURCE NAME SHOULD BE ALREADY EXISTS OR NOT
 * MODULE : DATASOURCE UPLOAD
 * MAIN : http://192.168.1.187:3000/sanitized_import/xlupload
 */
router.get('/chk_masterdatasource_exists', function(req, res, next) {

  var xl_datasource_name = decodeURI(req.query.xl_datasource_name);

  var collection = dbUtil.get().collection('MSB_Datasource_master').find({"ds_name":(xl_datasource_name).toUpperCase()});

  collection.toArray(function(err, master_data) {

    if(master_data.length > 0){
      return res.send(false);
    } else {
      return res.send(true);
    }

  });

}); 

/**
 * USE : ROOT LOADER OF DOWNLOAD THE DATA FROM SANITIZED UPLOADED DATA
 * MODULE : DATASOURCE DOWNLOAD
 * MAIN : http://192.168.1.187:3000/sanitized_import/datasource_download?datasource_objectid=5afbda1ad055ae2a2c68fbbf
 */
router.get("/datasource_download", function (req, res) {

  var mongo = require('mongodb');
  var jsonexport = require('jsonexport');

  if(req.query.datasource_objectid !== undefined && req.query.datasource_objectid != "") {
    var xl_datasource_name = (req.query.xl_datasource_name !== undefined && req.query.xl_datasource_name != "") ? decodeURI(req.query.xl_datasource_name) : "Datasource";
    var datasource_objectid = new mongo.ObjectID(req.query.datasource_objectid);
    var master_datasource_arr = new Array();
    var collection = dbUtil.get().collection('MSB_Sanitized_Data').find({"datasource_objectid":datasource_objectid});

    collection.toArray(function(err, master_data) {

      each(master_data, function(data, next_moveer){

        var temp_master_arr = {};
        temp_master_arr["No"] = data["No"];
        temp_master_arr["Client ID"] = data["Client ID"];
        temp_master_arr["Name"] = data["Name"];
        temp_master_arr["Transaction Date"] = data["Transaction Date"];
        temp_master_arr["Receipt No"] = data["Receipt No"];
        temp_master_arr["BuySell"] = data["BuySell"];
        temp_master_arr["Currency Code"] = data["Currency Code"];
        temp_master_arr["Foreign Amount"] = data["Foreign Amount"];
        temp_master_arr["Rate"] = data["Rate"];
        temp_master_arr["Exchange Amount"] = data["Exchange Amount"];
        temp_master_arr["Address"] = data["Address"];
        temp_master_arr["Nationality"] = data["Nationality"];
        temp_master_arr["DOB"] = data["DOB"];
        temp_master_arr["Customer Type"] = data["Customer Type"];
        temp_master_arr["Purpose"] = data["Purpose"];
        temp_master_arr["Source"] = data["Source"];
        temp_master_arr["Occupation"] = data["Occupation"];
        temp_master_arr["Mode of Payment"] = data["Mode of Payment"];
        temp_master_arr["Mode of Delivery"] = data["Mode of Delivery"];
        temp_master_arr["Staff ID"] = data["Staff ID"];
        temp_master_arr["Approved By"] = data["Approved By"];
        temp_master_arr["Outlet"] = data["Outlet"];
        temp_master_arr["Value Risk Score"] = data["Value Risk Score"];
        temp_master_arr["Nationality risk Score"] = data["Nationality risk Score"];
        temp_master_arr["Customer Type Risk Score"] = data["Customer Type Risk Score"];
        temp_master_arr["Purpose Risk Score"] = data["Purpose Risk Score"];
        temp_master_arr["Source Risk score"] = data["Source Risk score"];
        temp_master_arr["Occupation Risk Score"] = data["Occupation Risk Score"];
        temp_master_arr["Mode of Payment Risk Score"] = data["Mode of Payment Risk Score"];
        temp_master_arr["Mode of Delivery Risk Score"] = data["Mode of Delivery Risk Score"];
        temp_master_arr["Total Risk Score"] = data["Total Risk Score"];

        master_datasource_arr.push(temp_master_arr);
        next_moveer();
      },function(){
        jsonexport(master_datasource_arr, function(err, csv){
          if(err) { 
            console.log(err);
          } else {
            // console.log(csv);
            res.setHeader('Content-disposition', 'attachment; filename='+(xl_datasource_name.replace(/\s+/, ""))+'.csv');
            res.set('Content-Type', 'text/csv');
            res.status(200).send(csv);
          }
        });
      });
    });
  } else {
    res.status(200).redirect("/");
  }

});



module.exports = router;
