function Overlay(show = false) {
  if (show) {
    $.LoadingOverlay('show');
  } else {
    $.LoadingOverlay('hide');
  }
}

function elementOverlay(id_element, show = false) {
  if (id_element && show) {
    // $("#" + id_element).LoadingOverlay('show');
    $("#" + id_element).LoadingOverlay("show", {
      minSize : 30,
      maxSize : 15,
    });
  } else {
    $("#" + id_element).LoadingOverlay('hide');
  }
}